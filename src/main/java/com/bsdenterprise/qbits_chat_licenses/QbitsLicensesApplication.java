package com.bsdenterprise.qbits_chat_licenses;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.redis.RedisAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication(exclude = {
		RedisAutoConfiguration.class
})
public class QbitsLicensesApplication extends SpringBootServletInitializer {

	public static void main(String... args) {
		SpringApplication.run(QbitsLicensesApplication.class, args);
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(QbitsLicensesApplication.class);
	}

}
