package com.bsdenterprise.qbits_chat_licenses.app.controller.license;

import com.bsdenterprise.qbits_chat_licenses.app.service.license.LicenseProductService;

import static org.springframework.http.ResponseEntity.ok;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import lombok.Setter;

@RestController
@RequestMapping("/licenseProduct")
public class LicenseProductController {

    @Setter(onMethod = @__(@Autowired))
    private LicenseProductService licenseProductService;

    @GetMapping
    public ResponseEntity findLicenseProducts(@RequestParam(required = false) String code,
                                              @RequestParam(required = false, defaultValue = "1") Long licenseProductTypeId) throws Exception {

        return ok(licenseProductService.findLicenseProducts(code, licenseProductTypeId));
    }

}
