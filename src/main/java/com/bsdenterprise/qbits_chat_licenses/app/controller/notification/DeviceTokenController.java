package com.bsdenterprise.qbits_chat_licenses.app.controller.notification;

import com.bsdenterprise.qbits_chat_licenses.app.dto.notification.device.DeviceTokenRequest;

import com.bsdenterprise.qbits_chat_licenses.app.service.notification.DeviceTokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static com.bsdenterprise.qbits_chat_licenses.app.util.MessageUtil.message;
import static org.springframework.http.ResponseEntity.ok;

import javax.validation.Valid;
import lombok.*;

@RestController
@RequestMapping("/device/token")
public class DeviceTokenController {

    @Setter(onMethod = @__(@Autowired))
    private DeviceTokenService deviceTokenService;

    @SneakyThrows
    @PostMapping("/store")
    public ResponseEntity storeToken(@Valid @RequestBody DeviceTokenRequest deviceTokenRequest) {
        deviceTokenService.storeToken(deviceTokenRequest);
        return ok(message("Token stored correctly"));
    }

}
