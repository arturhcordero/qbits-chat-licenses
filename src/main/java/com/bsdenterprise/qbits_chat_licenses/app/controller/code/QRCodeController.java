package com.bsdenterprise.qbits_chat_licenses.app.controller.code;

import static com.bsdenterprise.qbits_chat_licenses.app.util.MessageUtil.message;
import static org.springframework.http.ResponseEntity.ok;

import com.bsdenterprise.qbits_chat_licenses.app.dto.code.qr.request.QRCodeRequest;
import com.bsdenterprise.qbits_chat_licenses.app.service.code.QRCodeService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import lombok.Setter;
import org.springframework.web.servlet.view.RedirectView;

@RestController
@RequestMapping("/code/qr")
public class QRCodeController {

    @Setter(onMethod = @__(@Autowired))
    private QRCodeService qrCodeService;

    @PostMapping("/send")
    public ResponseEntity generateAndSendQRCode(@Valid @RequestBody QRCodeRequest qrCodeRequest) throws Exception {
        qrCodeService.generateAndSendQRCode(qrCodeRequest);
        return ok(message("QR VerificationCode sent correctly"));
    }

    @PostMapping("/get")
    public ResponseEntity getQRCode(@Valid @RequestBody QRCodeRequest qrCodeRequest) throws Exception {
        return ok(qrCodeService.getQRCode(qrCodeRequest));
    }

    @PostMapping("/consume/{code}")
    public ResponseEntity consumeQRCode(@PathVariable String code) throws Exception {
        return ok(qrCodeService.consumeQRCode(code));
    }

    @GetMapping("/redirect/{code}")
    public RedirectView redirectUrl(@PathVariable String code) {
        return new RedirectView(qrCodeService.redirectUrl(code));
    }

}
