package com.bsdenterprise.qbits_chat_licenses.app.controller.notification;

import static com.bsdenterprise.qbits_chat_licenses.app.util.MessageUtil.message;

import com.bsdenterprise.qbits_chat_licenses.app.service.notification.NotificationService;
import com.bsdenterprise.qbits_chat_licenses.app.dto.notification.request.*;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.ResponseEntity.ok;
import javax.validation.Valid;

import lombok.Setter;

@RestController
@RequestMapping("/notification")
public class NotificationController {

    @Setter(onMethod = @__(@Autowired))
    private NotificationService notificationService;

    @SneakyThrows
    @PostMapping("/gcm")
    public ResponseEntity gcmNotification(@RequestParam String jabberId,
                                          @Valid @RequestBody GCMNotificationRequest gcmNotification) {

        notificationService.sendGcmNotification(jabberId, gcmNotification);

        return ok(message("Notication sent"));
    }

    @SneakyThrows
    @PostMapping("/apns")
    public ResponseEntity apnsNotification(@RequestParam String jabberId,
                                           @Valid @RequestBody APNSNotificationRequest apnsNotification) {

        notificationService.sendApnsNotification(jabberId, apnsNotification);

        return ok(message("Notication sent"));
    }

}
