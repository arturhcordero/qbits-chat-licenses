package com.bsdenterprise.qbits_chat_licenses.app.controller.catalog;

import com.bsdenterprise.qbits_chat_licenses.app.service.catalog.RoleService;

import com.bsdenterprise.qbits_chat_licenses.app.service.license.FeatureService;
import com.bsdenterprise.qbits_chat_licenses.app.type.license.LicenseDurationType;
import com.bsdenterprise.qbits_chat_licenses.app.type.organization.CommunityType;
import com.bsdenterprise.qbits_chat_licenses.app.type.organization.OrganizationType;
import com.bsdenterprise.qbits_chat_licenses.app.type.organization.auth.UserActivationType;
import com.bsdenterprise.qbits_chat_licenses.app.type.organization.user.GenderReference;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.ResponseEntity;

import static org.springframework.http.ResponseEntity.ok;

import lombok.Setter;

@RestController
@RequestMapping("/catalog")
public class CatalogController {

    @Setter(onMethod = @__(@Autowired))
    private RoleService roleService;

    @Setter(onMethod = @__(@Autowired))
    private FeatureService featureService;

    @GetMapping("/organizationType")
    public ResponseEntity findAllOrganizationTypes() {
        return ok(OrganizationType.values());
    }

    @GetMapping("/communityType")
    public ResponseEntity findAllCommunityTypes() {
        return ok(CommunityType.values());
    }

    @GetMapping("/licenseDuration")
    public ResponseEntity findAllLicenseDurations() {
        return ok(LicenseDurationType.values());
    }

    @GetMapping("/genderReference")
    public ResponseEntity findGenderReferences() {
        return ok(GenderReference.values());
    }

    @GetMapping("/userActivation")
    public ResponseEntity findActivationMethods() {
        return ok(UserActivationType.values());
    }

    @GetMapping("/role")
    public ResponseEntity findRoles() {
        return ok(roleService.findRoles());
    }

    @GetMapping("/feature")
    public ResponseEntity findFeatures() {
        return ok(featureService.findFeatures());
    }

}
