package com.bsdenterprise.qbits_chat_licenses.app.controller.code;

import com.bsdenterprise.qbits_chat_licenses.common.config.exception.model.ValidationException;

import com.bsdenterprise.qbits_chat_licenses.app.dto.code.CodeRequest;
import com.bsdenterprise.qbits_chat_licenses.app.service.code.VerificationCodeService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.ResponseEntity;

import static com.bsdenterprise.qbits_chat_licenses.app.util.MessageUtil.message;
import static org.springframework.http.ResponseEntity.status;
import static org.springframework.http.ResponseEntity.ok;
import static org.springframework.http.HttpStatus.*;

import javax.validation.Valid;
import lombok.Setter;

@RestController
@RequestMapping("/code/verification")
public class VerificationCodeController {

    @Setter(onMethod = @__(@Autowired))
    private VerificationCodeService verificationCodeService;

    @PostMapping("/send")
    public ResponseEntity generateAndSendVerificationCode(@Valid @RequestBody CodeRequest code) throws Exception {

        verificationCodeService.generateVerificationCode(code);

        return status(CREATED).body(message("Verification code generated and send correctly"));
    }

    @PostMapping("/verify/{email}/{code}/")
    public ResponseEntity verifyCode(@PathVariable String email,
                                     @PathVariable String code) throws ValidationException {

        verificationCodeService.verifyCode(email, code);

        return ok(message("VerificationCode verified correctly"));
    }

}
