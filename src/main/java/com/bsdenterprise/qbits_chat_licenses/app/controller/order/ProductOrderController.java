package com.bsdenterprise.qbits_chat_licenses.app.controller.order;

import com.bsdenterprise.qbits_chat_licenses.app.dto.organization_order.LicenseOrderRequest;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.organization.Organization;
import com.bsdenterprise.qbits_chat_licenses.app.service.mailer.MailerService;
import com.bsdenterprise.qbits_chat_licenses.app.service.order.ProductOrderService;

import com.bsdenterprise.qbits_chat_licenses.app.dto.organization_order.OrganizationOrderRequest;

import com.bsdenterprise.qbits_chat_licenses.common.config.exception.model.ValidationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.ResponseEntity;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.ResponseEntity.*;

import lombok.Setter;

import javax.validation.Valid;

@RestController
@RequestMapping("/order")
public class ProductOrderController {

    @Setter(onMethod = @__(@Autowired))
    private ProductOrderService productOrderService;

    @Setter(onMethod = @__(@Autowired))
    private MailerService mailerService;

    @GetMapping("/organization/{organizationId}")
    public ResponseEntity findOrders(@PathVariable Long organizationId) throws ValidationException {
        return ok(productOrderService.findOrders(organizationId));
    }

    @PostMapping("/organization")
    public ResponseEntity createOrganizationOrder(@Valid @RequestBody OrganizationOrderRequest organizationOrder) throws Exception {

        try {

            productOrderService.organizationOrder(organizationOrder);

            return status(CREATED).build();

        } catch (Exception ex) {
            productOrderService.resolveUserGeneration(null);
            mailerService.removeMailQueue();
            throw ex;
        }

    }

    @PostMapping("/organization/{organizationId}/license")
    public ResponseEntity createLicenseOrder(@PathVariable Long organizationId,
                                             @Valid @RequestBody LicenseOrderRequest licenseOrder, BindingResult br) throws Exception {

        try {

            licenseOrder.setOrganizationId(organizationId);

            productOrderService.licenseOrder(licenseOrder);

            return status(CREATED).build();

        } catch (Exception ex) {
            productOrderService.resolveUserGeneration(null);
            mailerService.removeMailQueue();
            throw ex;
        }

    }

}
