package com.bsdenterprise.qbits_chat_licenses.app.controller.license;

import com.bsdenterprise.qbits_chat_licenses.common.config.exception.model.ValidationException;

import com.bsdenterprise.qbits_chat_licenses.app.dto.license.modify.UpdateLicenseRequest;
import com.bsdenterprise.qbits_chat_licenses.app.dto.license.status.RenovateLicenseRequest;
import com.bsdenterprise.qbits_chat_licenses.app.dto.license.status.CancelLicensesRequest;
import com.bsdenterprise.qbits_chat_licenses.app.service.license.LicenseService;
import com.bsdenterprise.qbits_chat_licenses.app.dto.license.assignation.*;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static com.bsdenterprise.qbits_chat_licenses.app.util.MessageUtil.message;
import static org.springframework.http.ResponseEntity.ok;

import javax.validation.Valid;
import lombok.Setter;

@RestController
@RequestMapping("/license")
public class LicenseController {

    @Setter(onMethod = @__(@Autowired))
    private LicenseService licenseService;

    @GetMapping("/me")
    public ResponseEntity myLicense() throws Exception {
        return ok(licenseService.myLicense());
    }

    @PostMapping("/assign")
    public ResponseEntity assignLicense(@Valid @RequestBody AssignLicenseRequest assignLicense) throws Exception {
        licenseService.assignLicense(assignLicense);
        return ok(message("License renovated correctly"));
    }

    @PostMapping("/update")
    public ResponseEntity updateLicense(@Valid @RequestBody UpdateLicenseRequest updateLicense) throws Exception {
        licenseService.updateLicense(updateLicense);
        return ok(message("License renovated correctly"));
    }

    @PostMapping("/unassign")
    public ResponseEntity unassignLicense(@Valid @RequestBody UnassignLicenseRequest unassignLicense) throws Exception {
        licenseService.unassignLicense(unassignLicense);
        return ok(message("License unassigned correctly"));
    }

    @PostMapping("/cancel")
    public ResponseEntity cancelLicenses(@Valid @RequestBody CancelLicensesRequest turnOffLicense) throws Exception {
        licenseService.cancelLicenses(turnOffLicense);
        return ok(message("License turned off correctly"));
    }

    @PostMapping("/renovate")
    public ResponseEntity renovateLicense(@Valid @RequestBody RenovateLicenseRequest renovateLicense) throws Exception {
        licenseService.renovateLicense(renovateLicense);
        return ok(message("License renovated correctly"));
    }

    @GetMapping("/organization/{organizationId}")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", dataType = "integer", paramType = "query",
                    value = "Results page you want to retrieve (0..N)", defaultValue = "0"),
            @ApiImplicitParam(name = "size", dataType = "integer", paramType = "query",
                    value = "Number of records per page.", defaultValue = "5"),
            @ApiImplicitParam(name = "sort", dataType = "string", paramType = "query",
                    value = "Sort.", defaultValue = "id,desc"),
    })
    public ResponseEntity findLicenses(@PathVariable Long organizationId, final Pageable pageable) throws ValidationException {
        return ok(licenseService.findLicensesInfo(organizationId, pageable));
    }

    @GetMapping("/organization/{organizationId}/group")
    public ResponseEntity findGroupedLicenses(@PathVariable Long organizationId) throws ValidationException {
        return ok(licenseService.findGroupedLicensesInfo(organizationId));
    }

    @GetMapping("/{licenseId}")
    public ResponseEntity findLicense(@PathVariable Long licenseId) throws ValidationException {
        return ok(licenseService.findLicenseInfo(licenseId));
    }

}
