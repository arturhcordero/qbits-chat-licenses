package com.bsdenterprise.qbits_chat_licenses.app.controller.organization;

import com.bsdenterprise.qbits_chat_licenses.app.dto.account.ChangePasswordRequest;
import com.bsdenterprise.qbits_chat_licenses.app.dto.account.ForgotPasswordRequest;
import com.bsdenterprise.qbits_chat_licenses.app.dto.account.RestorePasswordRequest;
import com.bsdenterprise.qbits_chat_licenses.app.dto.user.create.UserRequest;
import com.bsdenterprise.qbits_chat_licenses.app.dto.user.modify.AddRoleRequest;
import com.bsdenterprise.qbits_chat_licenses.app.service.organization.UserService;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import static com.bsdenterprise.qbits_chat_licenses.app.util.MessageUtil.message;
import static org.springframework.http.ResponseEntity.ok;

import javax.validation.Valid;
import lombok.*;

@RestController
@RequestMapping("/user")
public class UserController {

    @Setter(onMethod = @__(@Autowired))
    private UserService userService;

    @SneakyThrows
    @GetMapping("/license/{organizationId}")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", dataType = "integer", paramType = "query",
                    value = "Results page you want to retrieve (0..N)", defaultValue = "0"),
            @ApiImplicitParam(name = "size", dataType = "integer", paramType = "query",
                    value = "Number of records per page.", defaultValue = "5"),
            @ApiImplicitParam(name = "sort", dataType = "string", paramType = "query",
                    value = "Sort.", defaultValue = "id,desc"),
            @ApiImplicitParam(name = "search", dataType = "string", paramType = "query",
                    value = "Search query"),
    })
    public ResponseEntity findUsers(@PathVariable Long organizationId,
                                    @RequestParam(value = "search", required = false) String search,
                                    final Pageable pageable) {
        return ok(userService.findUsers(search, organizationId, pageable));
    }

    @SneakyThrows
    @GetMapping("/info")
    public ResponseEntity userInfo(Authentication authentication) {
        return ok(userService.userInfo(authentication.getName()));
    }

    @SneakyThrows
    @PatchMapping("/confirm/{confirmationToken}")
    public ResponseEntity confirmUser(@PathVariable String confirmationToken,
                                      @Valid @RequestBody UserRequest userRequest) {
        userService.confirmUser(confirmationToken, userRequest);
        return ok(message("User confirmed correctly"));
    }

    @SneakyThrows
    @GetMapping("/email/confirmation/{confirmationToken}")
    public ResponseEntity emailByConfirmationToken(@PathVariable String confirmationToken) {
        return ok(userService.findEmailByToken(confirmationToken));
    }

    @SneakyThrows
    @GetMapping("/password/token/{passwordConfirmationToken}")
    public ResponseEntity validateRestorePasswordToken(@PathVariable String passwordConfirmationToken) {
        userService.validateRestorePasswordToken(passwordConfirmationToken);
        return ok(message("The token is valid"));
    }

    @SneakyThrows
    @PostMapping("/addRole")
    public ResponseEntity addUserRole(@Valid @RequestBody AddRoleRequest addRole) {
        userService.addRole(addRole);
        return ok(message("User role changed correctly"));
    }

    @SneakyThrows
    @PatchMapping("/changePassword")
    public ResponseEntity changePassword(@Valid @RequestBody ChangePasswordRequest changePassword) {
        userService.changePassword(changePassword);
        return ok(message("User password changed correctly"));
    }

    @SneakyThrows
    @PostMapping("/forgotPassword")
    public ResponseEntity forgotPassword(@Valid @RequestBody ForgotPasswordRequest forgotPassword) {
        userService.forgotPassword(forgotPassword);
        return ok(message("Restore password mail sent"));
    }

    @SneakyThrows
    @PatchMapping("/restorePassword/{passwordConfirmationToken}")
    public ResponseEntity restorePassword(@PathVariable String passwordConfirmationToken,
                                          @Valid @RequestBody RestorePasswordRequest restorePassword) {
        userService.restorePassword(passwordConfirmationToken, restorePassword);
        return ok(message("User password restored correctly"));
    }

}
