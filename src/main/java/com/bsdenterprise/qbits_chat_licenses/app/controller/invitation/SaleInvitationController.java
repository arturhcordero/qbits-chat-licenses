package com.bsdenterprise.qbits_chat_licenses.app.controller.invitation;

import com.bsdenterprise.qbits_chat_licenses.app.dto.invitation.SaleInvitationRequest;
import com.bsdenterprise.qbits_chat_licenses.app.service.invitation.SaleInvitationService;

import static com.bsdenterprise.qbits_chat_licenses.app.util.MessageUtil.message;
import static org.springframework.http.ResponseEntity.ok;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import io.swagger.annotations.*;
import javax.validation.Valid;
import lombok.*;

@RestController
@RequestMapping("/sale/invitation")
public class SaleInvitationController {

    @Setter(onMethod = @__(@Autowired))
    private SaleInvitationService saleInvitationService;

    @SneakyThrows
    @GetMapping
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", dataType = "integer", paramType = "query",
                    value = "Results page you want to retrieve (0..N)", defaultValue = "0"),
            @ApiImplicitParam(name = "size", dataType = "integer", paramType = "query",
                    value = "Number of records per page.", defaultValue = "5"),
            @ApiImplicitParam(name = "sort", dataType = "string", paramType = "query",
                    value = "Sort.", defaultValue = "id,desc"),
            @ApiImplicitParam(name = "search", dataType = "string", paramType = "query",
                    value = "Search query"),
    })
    public ResponseEntity findInvitations(@RequestParam(value = "search", required = false) String search,
                                          final Pageable pageable) {
        return ok(saleInvitationService.findInvitations(search, pageable));
    }

    @SneakyThrows
    @PostMapping("/send")
    public ResponseEntity sendInvitation(@Valid @RequestBody SaleInvitationRequest saleInvitation) {
        saleInvitationService.sendInvitation(saleInvitation);
        return ok(message("Sale invitation sent"));
    }

    @SneakyThrows
    @PatchMapping("/forward/{saleInvitationId}")
    public ResponseEntity forwardInvitation(@PathVariable Long saleInvitationId) {
        saleInvitationService.forwardInvitation(saleInvitationId);
        return ok(message("Sale invitation forwarded"));
    }

    @SneakyThrows
    @DeleteMapping("/cancel/{saleInvitationId}")
    public ResponseEntity cancelInvitation(@PathVariable Long saleInvitationId) {
        saleInvitationService.cancelInvitation(saleInvitationId);
        return ok(message("Sale invitation cancelled"));
    }

    @SneakyThrows
    @PostMapping("/open/{saleInvitationUuid}")
    public ResponseEntity openInvitation(@PathVariable String saleInvitationUuid) {
        return ok(saleInvitationService.openInvitation(saleInvitationUuid));
    }

}
