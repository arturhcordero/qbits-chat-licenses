package com.bsdenterprise.qbits_chat_licenses.app.controller.organization;

import com.bsdenterprise.qbits_chat_licenses.app.dto.organization.modify.ModifyOrganizationRequest;
import com.bsdenterprise.qbits_chat_licenses.app.service.organization.OrganizationService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.ResponseEntity;

import static com.bsdenterprise.qbits_chat_licenses.app.util.MessageUtil.message;
import static org.springframework.http.ResponseEntity.ok;

import lombok.Setter;

import javax.validation.Valid;

@RestController
@RequestMapping("/organization")
@Api("Operations about organizations")
public class OrganizationController {

    @Setter(onMethod = @__(@Autowired))
    private OrganizationService organizationService;

    @GetMapping("/{organizationId}/info")
    @ApiOperation(value = "Find organization by id")
    public ResponseEntity findOrganizationInfo(@PathVariable Long organizationId) throws Exception {
        return ok(organizationService.findOrganizationInfo(organizationId));
    }

    @PatchMapping("/modify")
    public ResponseEntity modifyOrganizationInfo(Authentication auth,
                                                 @Valid @RequestBody ModifyOrganizationRequest modifyOrganization) throws Exception {

        organizationService.modifyOrganizationInfo(auth.getName(), modifyOrganization);

        return ok(message("Organization info modified correctly"));
    }

}
