package com.bsdenterprise.qbits_chat_licenses.app.controller.organization;

import com.bsdenterprise.qbits_chat_licenses.app.dto.payment.create.PaymentMethodRequest;
import com.bsdenterprise.qbits_chat_licenses.app.dto.payment.read.PaymentMethodInfo;
import com.bsdenterprise.qbits_chat_licenses.app.dto.payment.update.PaymentMethodUpdateRequest;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.payment.PaymentMethod;
import com.bsdenterprise.qbits_chat_licenses.app.type.payment.PaymentMethodFlow;

import com.bsdenterprise.qbits_chat_licenses.common.config.exception.model.ValidationException;
import com.bsdenterprise.qbits_chat_licenses.app.service.payment_method.PaymentMethodService;

import org.springframework.beans.factory.annotation.Autowired;

import static org.springframework.http.ResponseEntity.ok;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.ResponseEntity;

import static com.bsdenterprise.qbits_chat_licenses.app.util.MessageUtil.message;

import javax.validation.Valid;
import lombok.Setter;

@RestController
@RequestMapping("/paymentMethod")
public class PaymentMethodController {

    @Setter(onMethod = @__(@Autowired))
    private PaymentMethodService paymentMethodService;

    @GetMapping("/{organizationId}")
    public ResponseEntity findPaymentMethods(@PathVariable Long organizationId) throws ValidationException {
        return ok(paymentMethodService.findPaymentMethodsInfo(organizationId));
    }

    @GetMapping("/find/{paymentMethodIdentifier}")
    public ResponseEntity findPaymentMethod(@PathVariable String paymentMethodIdentifier) throws Exception {
        return ok(paymentMethodService.findPaymentMethodInfo(paymentMethodIdentifier));
    }

    @GetMapping("/default/{organizationId}")
    public ResponseEntity defaultPaymentMethod(@PathVariable Long organizationId) throws Exception {
        return ok(paymentMethodService.findDefaultPaymentMethodInfo(organizationId));
    }

    @PostMapping("/{organizationId}")
    public ResponseEntity createPaymentMethod(@PathVariable Long organizationId,
                                              @Valid @RequestBody PaymentMethodRequest paymentMethodRequest) throws Exception {

        paymentMethodRequest.setCurrentFlow(PaymentMethodFlow.CreatingPaymentMethod);
        paymentMethodRequest.setOrganizationId(organizationId);

        PaymentMethod paymentMethod = paymentMethodService
                .createPaymentMethod(paymentMethodRequest, true);

        return ok(paymentMethodService.mapperUtil()
                .map(paymentMethod, PaymentMethodInfo.class));
    }

    @PutMapping("/{paymentMethodIdentifier}")
    public ResponseEntity updatePaymentMethod(@PathVariable String paymentMethodIdentifier,
                                              @RequestBody PaymentMethodUpdateRequest paymentMethodUpdateRequest) throws Exception {

        PaymentMethod paymentMethod = paymentMethodService.updatePaymentMethod(paymentMethodIdentifier, paymentMethodUpdateRequest);

        return ok(paymentMethodService.mapperUtil()
                .map(paymentMethod, PaymentMethodInfo.class));
    }

    @PostMapping("/default/{paymentMethodIdentifier}")
    public ResponseEntity defaultPaymentMethod(@PathVariable String paymentMethodIdentifier) throws Exception {

        paymentMethodService.establishDefaultPayment(paymentMethodIdentifier);

        return ok(message("Default payment method changed correctly"));
    }

    @DeleteMapping("/{paymentMethodIdentifier}")
    public ResponseEntity removePaymentMethod(@PathVariable String paymentMethodIdentifier) throws Exception {

        paymentMethodService.removePaymentMethod(paymentMethodIdentifier);

        return ok(message("Payment method removed correctly"));
    }

}
