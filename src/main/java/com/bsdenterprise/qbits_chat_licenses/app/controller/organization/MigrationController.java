package com.bsdenterprise.qbits_chat_licenses.app.controller.organization;

import com.bsdenterprise.qbits_chat_licenses.app.dto.organization.modify.RemoveOFUserRequest;
import com.bsdenterprise.qbits_chat_licenses.app.dto.organization.read.OrganizationInfo;
import com.bsdenterprise.qbits_chat_licenses.app.dto.organization_order.OrganizationOrderMigrationRequest;

import com.bsdenterprise.qbits_chat_licenses.app.service.mailer.MailerService;
import com.bsdenterprise.qbits_chat_licenses.app.service.openfire.OpenfireService;
import com.bsdenterprise.qbits_chat_licenses.app.service.order.ProductOrderService;

import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.organization.Organization;

import com.bsdenterprise.qbits_chat_licenses.common.config.exception.model.IntegrationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.ResponseEntity;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.ResponseEntity.ok;
import static org.springframework.http.ResponseEntity.status;

import javax.validation.Valid;
import lombok.Setter;

@RestController
@RequestMapping("/migration")
public class MigrationController {

    @Setter(onMethod = @__(@Autowired))
    private ProductOrderService productOrderService;

    @Setter(onMethod = @__(@Autowired))
    private MailerService mailerService;

    @Setter(onMethod = @__(@Autowired))
    private OpenfireService openfireService;

    @PostMapping("/order/organization")
    public ResponseEntity createOrganizationMigrationOrder(@Valid @RequestBody OrganizationOrderMigrationRequest organizationOrder) throws Exception {

        try {

            Organization organization = productOrderService.organizationMigrationOrder(organizationOrder);

            mailerService.sendMailQueue();
            productOrderService.resolveUserGeneration(organization.getOwner());

            OrganizationInfo organizationInfo = productOrderService.mapperUtil()
                    .map(organization, OrganizationInfo.class);

            return status(CREATED).body(organizationInfo);

        } catch (Exception ex) {
            productOrderService.resolveUserGeneration(null);
            mailerService.removeMailQueue();
            throw ex;
        }

    }

    @PostMapping("/remove/openfireUser")
    public ResponseEntity removeUser(@RequestBody RemoveOFUserRequest removeOFUser) throws IntegrationException {
        openfireService.removeUser(removeOFUser.getUsername());
        return ok().build();
    }

}
