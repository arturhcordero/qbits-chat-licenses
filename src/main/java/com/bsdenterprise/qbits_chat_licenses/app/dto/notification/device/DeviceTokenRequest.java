package com.bsdenterprise.qbits_chat_licenses.app.dto.notification.device;

import lombok.Data;

import javax.validation.constraints.*;

@Data
public class DeviceTokenRequest {

    @NotEmpty
    private String token;

    @NotNull
    private Integer platformTypeId;

    @NotNull
    private Integer deviceTypeId;

}
