package com.bsdenterprise.qbits_chat_licenses.app.dto.user.create;

import com.bsdenterprise.qbits_chat_licenses.app.config.validator.password.match.PasswordMatch;
import com.bsdenterprise.qbits_chat_licenses.app.config.validator.password.match.common.PasswordChecker;
import com.bsdenterprise.qbits_chat_licenses.app.config.validator.password.policy.ValidPassword;
import com.bsdenterprise.qbits_chat_licenses.app.dto.user.UserInput;

import java.time.LocalDate;
import javax.validation.Valid;
import javax.validation.constraints.*;

import com.bsdenterprise.qbits_chat_licenses.app.type.organization.auth.UserActivationType;
import com.bsdenterprise.qbits_chat_licenses.app.type.organization.user.GenderReference;
import lombok.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import static com.bsdenterprise.qbits_chat_licenses.app.type.organization.auth.UserActivationType.Automatic;

@PasswordMatch
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public @Data class UserRequest extends UserInput implements PasswordChecker {

    @Valid
    @ValidPassword
    @NotEmpty(message = "{user.create.empty.password}")
    private String password;

    @Valid
    @ValidPassword
    @NotEmpty(message = "{user.create.empty.confirm-password}")
    private String confirmPassword;

    @JsonIgnore
    private boolean enabled;

    @JsonIgnore
    private String confirmationToken;

    @NotEmpty(message = "{user.create.empty.birthday}")
    private String birthdaySt;

    @JsonIgnore
    private LocalDate birthday;

    @NotNull(message = "{user.create.invalid.gender-reference}")
    private Long genderReferenceId;

    @JsonIgnore
    private GenderReference genderReference;

    @JsonIgnore
    private UserActivationType activationType;

    @Override
    public boolean checkPasswords() {
        return password.equals(confirmPassword);
    }

    public UserRequest(String email) {
        super(email);
    }

    public void setActivationType(UserActivationType activationType) {
        this.activationType = activationType;
        if (this.activationType == Automatic) {
            enabled = true;
        }
    }

}
