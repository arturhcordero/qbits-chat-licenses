package com.bsdenterprise.qbits_chat_licenses.app.dto.mailer.mail.user;

import static com.bsdenterprise.qbits_chat_licenses.common.util.Constants.DEFAULT_DATE_FORMATTER;
import com.bsdenterprise.qbits_chat_licenses.app.type.license.LicenseDurationType;

import java.time.LocalDateTime;
import lombok.*;

@Builder
public @Data class UserLicenseMail {

    private Organization organization;
    private LicenseProduct licenseProduct;
    private License license;
    private User user;

    private String confirmationUrl;

    @Data
    @NoArgsConstructor
    public static class LicenseProduct {
        private String name;
        private String description;
        private LicenseDurationType duration;
    }

    @Data
    @NoArgsConstructor
    public static class Organization {
        private String name;
        private String description;
    }

    @Data
    @NoArgsConstructor
    public static class License {
        private String expiration;
        public License(LocalDateTime expiration) {
            this.expiration = expiration.format(DEFAULT_DATE_FORMATTER);
        }
    }

    @Data
    @NoArgsConstructor
    public static class User {
        private String email;
        private String fullName;
    }

}
