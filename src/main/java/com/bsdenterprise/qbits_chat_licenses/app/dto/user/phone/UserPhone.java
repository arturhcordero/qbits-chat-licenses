package com.bsdenterprise.qbits_chat_licenses.app.dto.user.phone;

import com.bsdenterprise.qbits_chat_licenses.app.config.validator.cellphone.common.PhoneNumber;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class UserPhone implements PhoneNumber {

    @NotEmpty
    private String phone;

    @NotEmpty
    private String phoneRegion;

}
