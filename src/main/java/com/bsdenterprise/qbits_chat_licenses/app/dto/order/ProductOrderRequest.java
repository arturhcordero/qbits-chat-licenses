package com.bsdenterprise.qbits_chat_licenses.app.dto.order;

import com.bsdenterprise.qbits_chat_licenses.app.dto.order.license.OrderDetailRequest;
import com.bsdenterprise.qbits_chat_licenses.app.type.order.ProductOrderType;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.springframework.util.CollectionUtils;

import javax.validation.constraints.NotEmpty;
import java.util.LinkedList;
import java.util.List;

import static org.springframework.util.CollectionUtils.isEmpty;

public @Data class ProductOrderRequest {

    @JsonIgnore
    private ProductOrderType orderType;

    @NotEmpty
    private List<OrderDetailRequest> orderDetails;

    @JsonIgnore
    private Long paymentMethodId;

    public void addDetail(OrderDetailRequest orderDetail) {
        if (isEmpty(orderDetails))
            orderDetails = new LinkedList<>();
        orderDetail.setOrder(this);
        orderDetails.add(orderDetail);
    }

}
