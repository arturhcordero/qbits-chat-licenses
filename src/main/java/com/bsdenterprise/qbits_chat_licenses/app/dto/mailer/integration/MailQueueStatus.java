package com.bsdenterprise.qbits_chat_licenses.app.dto.mailer.integration;

import com.bsdenterprise.qbits_chat_licenses.app.type.mailer.config.GroupType;

import java.util.List;
import lombok.Data;

public @Data
class MailQueueStatus {

    private String id;
    private String senderName;
    private GroupType groupType;

    private List<MailStatus> mails;

}
