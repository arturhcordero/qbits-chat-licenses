package com.bsdenterprise.qbits_chat_licenses.app.dto.organization.modify;

import com.bsdenterprise.qbits_chat_licenses.app.type.organization.CommunityType;
import com.bsdenterprise.qbits_chat_licenses.app.type.organization.OrganizationType;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

public @Data class ModifyOrganizationRequest {

    private String name;
    private String description;
    private String rfc;

    private Long organizationTypeId;
    private Integer communityTypeId;

    @JsonIgnore
    private OrganizationType organizationType;

    @JsonIgnore
    private CommunityType communityType;

}
