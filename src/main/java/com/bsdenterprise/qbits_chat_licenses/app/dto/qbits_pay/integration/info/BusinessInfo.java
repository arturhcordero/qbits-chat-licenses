package com.bsdenterprise.qbits_chat_licenses.app.dto.qbits_pay.integration.info;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

@Data
public class BusinessInfo {

    private String affiliationNumber;
    private String currencyCode;
    private String fiid;
    private String logicalNetwork;
    private String businessName;
    private String businessCity;
    private String businessStateCode;
    private String businessCountryCode;
    private String businessTypeCode;
    private String placeId;

    @JsonIgnore
    private String saleType;

    @JsonIgnore
    private String origin;

}
