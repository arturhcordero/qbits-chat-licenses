package com.bsdenterprise.qbits_chat_licenses.app.dto.qbits_pay.integration.sale.response;

import lombok.Data;

public @Data class PaymentAuthorizationResponse {

    private String traceNumber;
    private String authorizationCode;
    private String amount;
    private String affiliationNumber;
    private String bin;

}
