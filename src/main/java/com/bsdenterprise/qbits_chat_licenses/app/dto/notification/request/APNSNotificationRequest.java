package com.bsdenterprise.qbits_chat_licenses.app.dto.notification.request;

import lombok.Data;

import java.util.Map;

public @Data class APNSNotificationRequest {

    private Aps aps;
    private Map<String, Object> extraData;

    @Data
    public static class Aps {

        private Alert alert;
        private Integer badge;
        private String sound;
        private Integer contentAvailable;
        private String category;

        public boolean isSilent() {
            return this.contentAvailable == 1;
        }

    }

    @Data
    public static class Alert {

        private String title;
        private String body;

    }

}
