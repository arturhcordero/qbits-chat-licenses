package com.bsdenterprise.qbits_chat_licenses.app.dto.payment.read;

import com.bsdenterprise.qbits_chat_licenses.app.type.payment.PaymentMethodBrand;
import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
public @Data class PaymentMethodInfo {

    private Long organizationId;
    private String alias;
    private String customerName;
    private String lastDigits;
    private String expirationDate;
    private String bank;
    private String brand;
    private Integer type;
    private boolean defaultPayment;
    private String uuid;

    public String getIcon() {
        PaymentMethodBrand paymentMethodBrand = PaymentMethodBrand
                .findPaymentMethodBrand(brand.toUpperCase());
        if (paymentMethodBrand == null)
            return PaymentMethodBrand.Visa.getIcon();
        return paymentMethodBrand.getIcon();
    }

}
