package com.bsdenterprise.qbits_chat_licenses.app.dto.mailer.config;

import lombok.Data;

@Data
public class Sender {

    private String email;
    private String name;

}
