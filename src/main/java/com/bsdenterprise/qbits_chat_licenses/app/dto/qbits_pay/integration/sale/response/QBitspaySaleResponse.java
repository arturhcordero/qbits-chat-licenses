package com.bsdenterprise.qbits_chat_licenses.app.dto.qbits_pay.integration.sale.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

public @Data class QBitspaySaleResponse {

    private String responseCode;
    private String message;
    private String prosaCode;
    private String transactionIdentifier;
    private String transactionDate;

    @JsonProperty("messageObject")
    private PaymentAuthorizationResponse authorization;

}
