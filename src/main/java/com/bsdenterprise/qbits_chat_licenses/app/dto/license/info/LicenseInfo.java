package com.bsdenterprise.qbits_chat_licenses.app.dto.license.info;

import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.license.product.Cost;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.license.product.LicenseProduct;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.organization.UserOrganization;
import com.bsdenterprise.qbits_chat_licenses.app.type.license.*;

import static com.bsdenterprise.qbits_chat_licenses.app.util.BeanUtil.getBean;
import com.bsdenterprise.qbits_chat_licenses.common.util.MapperUtil;

import java.time.LocalDateTime;
import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
public @Data class LicenseInfo {

    private Long id;
    private Integer quantity;
    private Long organizationId;
    private LocalDateTime expiration;

    private LicenseStatusType status;
    private LicenseDurationType duration;

    private UserLicenseInfo userInfo;
    private LicenseProductInfo licenseProductInfo;

    public void setUserOrganization(UserOrganization userOrganization) {

        MapperUtil mapperUtil = getBean("commonMapper", MapperUtil.class);

        if (userOrganization != null)
            userInfo = mapperUtil
                    .map(userOrganization.getUser(), UserLicenseInfo.class);
    }

    public void setLicenseProduct(LicenseProduct licenseProduct) {

        MapperUtil mapperUtil = getBean("commonMapper", MapperUtil.class);

        licenseProductInfo = mapperUtil
                .map(licenseProduct, LicenseProductInfo.class);
    }

    @Data
    @NoArgsConstructor
    private static class UserLicenseInfo {
        private Long id;
        private String fullName;
        private String email;
    }

    @Data
    @NoArgsConstructor
    private static class LicenseProductInfo {
        private Long id;
        private String code;
        private String name;
        private String description;
        private String shortDescription;
        private String abbreviation;
        private String iconUrl;
        private String color;
        private LicenseProductType type;
        private Cost cost;
    }

}
