package com.bsdenterprise.qbits_chat_licenses.app.dto.mailer.integration;

import lombok.Builder;
import lombok.Data;

import java.util.List;
import java.util.Map;

@Builder
public @Data class Mail {

    private String subject;

    // To send a previously built email
    private String body;
    private String simpleBody;

    // To construct an email by a payload
    // and a template reference
    private Map<String, Object> payload;

    private List<String> emails;

    private List<String> cc;
    private List<String> bcc;

    private String senderEmail;
    private String senderName;

    private String identifier;

}
