package com.bsdenterprise.qbits_chat_licenses.app.dto.code.qr.response;

import com.bsdenterprise.qbits_chat_licenses.app.type.info.CodeStatus;
import com.bsdenterprise.qbits_chat_licenses.app.type.info.CodeType;
import lombok.Data;

import java.time.LocalDateTime;

public @Data class QRCodeResponse {

    private String code;
    private CodeStatus status;

}
