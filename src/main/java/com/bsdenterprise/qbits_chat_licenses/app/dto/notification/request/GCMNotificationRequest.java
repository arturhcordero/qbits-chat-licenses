package com.bsdenterprise.qbits_chat_licenses.app.dto.notification.request;

import java.util.Map;
import lombok.*;

public @Data class GCMNotificationRequest {

    private Notification notification;
    private Map<String, Object> data;

    private String priority;

    @Data
    @NoArgsConstructor
    public static class Notification {
        private String title;
        private String body;
        private String sound;
    }

}
