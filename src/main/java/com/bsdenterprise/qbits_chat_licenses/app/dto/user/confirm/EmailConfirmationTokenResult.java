package com.bsdenterprise.qbits_chat_licenses.app.dto.user.confirm;

import lombok.Data;

@Data
public class EmailConfirmationTokenResult {

    private String email;

    public EmailConfirmationTokenResult(String email) {
        this.email = email;
    }

}
