package com.bsdenterprise.qbits_chat_licenses.app.dto.payment.read;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class OrderPaymentInfo {

    private String alias;
    private String transactionIdentifier;
    private String authorizationCode;
    private String responseCode;
    private String prosaCode;
    private String traceNumber;
    private String message;
    private String amount;
    private String bin;
    private LocalDateTime createdAt;

}
