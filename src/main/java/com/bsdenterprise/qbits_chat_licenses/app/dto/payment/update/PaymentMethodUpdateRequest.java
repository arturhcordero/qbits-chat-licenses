package com.bsdenterprise.qbits_chat_licenses.app.dto.payment.update;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

@Data
public class PaymentMethodUpdateRequest {

    @NotBlank
    @Length(min = 3, max = 4)
    private String securityCode;

    @NotBlank
    private String expirationDate;

}
