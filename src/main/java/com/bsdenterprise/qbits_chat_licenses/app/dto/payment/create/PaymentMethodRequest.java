package com.bsdenterprise.qbits_chat_licenses.app.dto.payment.create;

import com.bsdenterprise.qbits_chat_licenses.app.type.payment.PaymentMethodFlow;
import org.hibernate.validator.constraints.Length;
import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

public @Data class PaymentMethodRequest {

    @NotBlank
    private String customerName;

    @NotBlank
    private String cardNumber;

    @NotBlank
    @Length(min = 3, max = 4)
    private String securityCode;

    @NotBlank
    private String expirationDate;

    private boolean defaultMethod;

    private String type;

    @JsonIgnore
    private PaymentMethodFlow currentFlow = PaymentMethodFlow.CreatingPaymentMethod;

    @JsonIgnore
    private Long organizationId;

    public void setDefault() {
        setDefaultMethod(true);
    }

    public String getCardNumber() {
        return cardNumber.replace("-", "");
    }
}
