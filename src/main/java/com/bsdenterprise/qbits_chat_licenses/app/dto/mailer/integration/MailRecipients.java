package com.bsdenterprise.qbits_chat_licenses.app.dto.mailer.integration;

import java.util.*;
import lombok.*;

@NoArgsConstructor
public @Data class MailRecipients {

    private List<String> emails = new LinkedList<>();
    private List<String> cc = new LinkedList<>();
    private List<String> bcc = new LinkedList<>();

    public MailRecipients(String... emails) {
        this.emails = new LinkedList<>(Arrays.asList(emails));
    }

    public void clear() {
        this.emails.clear();
        this.cc.clear();
    }

}
