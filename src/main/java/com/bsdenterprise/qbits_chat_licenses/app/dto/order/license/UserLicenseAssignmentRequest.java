package com.bsdenterprise.qbits_chat_licenses.app.dto.order.license;

import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.organization.UserOrganization;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.organization.auth.User;
import lombok.Data;

public @Data class UserLicenseAssignmentRequest {

    private Long roleId;
    private String email;

    public UserLicenseAssignmentRequest() {

    }

    public UserLicenseAssignmentRequest(Long roleId, String email) {
        this.roleId = roleId;
        this.email = email;
    }

    public UserLicenseAssignmentRequest(UserOrganization userOrganization, User user) {
        this(userOrganization.getMainRole().getId(), user.getEmail());
    }

}
