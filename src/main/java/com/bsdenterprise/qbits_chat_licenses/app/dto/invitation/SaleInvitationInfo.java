package com.bsdenterprise.qbits_chat_licenses.app.dto.invitation;

import com.bsdenterprise.qbits_chat_licenses.app.dto.order.info.ProductOrderInfo;
import com.bsdenterprise.qbits_chat_licenses.app.type.invitation.SaleInvitationStatus;

import lombok.*;

@Data
public class SaleInvitationInfo {

    private Long id;
    private String email;
    private SaleInvitationStatus status;
    private LicenseProductInfo licenseProduct;
    private ProductOrderInfo productOrder;

    @Data
    @NoArgsConstructor
    private static class LicenseProductInfo {
        private String name;
        private String description;
        private String iconUrl;
    }

}
