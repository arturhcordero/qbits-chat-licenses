package com.bsdenterprise.qbits_chat_licenses.app.dto.organization.read;

import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.organization.Address;
import com.bsdenterprise.qbits_chat_licenses.app.type.organization.CommunityType;
import com.bsdenterprise.qbits_chat_licenses.app.type.organization.OrganizationType;

import java.io.Serializable;
import java.util.List;
import lombok.Data;

public @Data class OrganizationInfo implements Serializable {

    private Long id;
    private String name;
    private String description;
    private String rfc;
    private String uuid;

    private List<Address> addresses;

    private OrganizationType organizationType;
    private CommunityType communityType;

}
