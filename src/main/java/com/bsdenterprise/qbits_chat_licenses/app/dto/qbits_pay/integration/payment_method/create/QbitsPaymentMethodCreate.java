package com.bsdenterprise.qbits_chat_licenses.app.dto.qbits_pay.integration.payment_method.create;

import com.bsdenterprise.qbits_chat_licenses.common.util.Constants;
import org.springframework.util.StringUtils;

import static java.util.UUID.randomUUID;

import lombok.Data;

public @Data class QbitsPaymentMethodCreate {

    private String customerName;
    private String cardNumber;
    private String expirationDate;
    private String cvv;

    private String jidUuid;
    private String alias;
    private String agreement;

    private final String pin = Constants.DEFAULT_PIN_PAYMENT_METHOD;

    public String getAlias() {
        return String.format("%s", randomUUID()).substring(0, 4);
    }

    public String getExpirationDate() {

        if (!StringUtils.isEmpty(expirationDate))
            return expirationDate.replaceAll("/", "");

        return expirationDate;
    }

}
