package com.bsdenterprise.qbits_chat_licenses.app.dto.license.product;

import lombok.Data;

@Data
public class FeatureInfo {

    private Long id;
    private String code;
    private String description;

}
