package com.bsdenterprise.qbits_chat_licenses.app.dto.license.assignation;

import javax.validation.constraints.*;

import lombok.Data;

public @Data class AssignLicenseRequest {

    @NotNull
    private Long licenseId;

    @NotEmpty
    private String email;

    @NotNull
    private Long roleId;

    private String message;

}
