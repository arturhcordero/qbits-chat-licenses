package com.bsdenterprise.qbits_chat_licenses.app.dto.invitation;

import javax.validation.constraints.*;
import lombok.Data;

@Data
public class SaleInvitationRequest {

    @NotEmpty
    private String email;

    @NotNull
    private Long licenseProductId;

}
