package com.bsdenterprise.qbits_chat_licenses.app.dto.order.info;

import com.bsdenterprise.qbits_chat_licenses.app.dto.order.detail.info.OrderDetailInfo;
import com.bsdenterprise.qbits_chat_licenses.app.dto.organization.read.OrganizationInfo;
import com.bsdenterprise.qbits_chat_licenses.app.dto.payment.read.*;
import com.bsdenterprise.qbits_chat_licenses.app.type.order.*;

import java.math.BigDecimal;
import java.util.List;
import java.time.*;

import lombok.Data;

@Data
public class ProductOrderInfo {

    private Long id;
    private LocalDateTime createdAt;
    private BigDecimal total;
    private ProductOrderStatus status;
    private ProductOrderType type;
    private OrderPaymentInfo orderPayment;

    private OrganizationInfo organization;
    private PaymentMethodInfo paymentMethod;

    private List<OrderDetailInfo> orderDetails;

}
