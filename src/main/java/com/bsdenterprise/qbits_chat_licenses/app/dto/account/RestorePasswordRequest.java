package com.bsdenterprise.qbits_chat_licenses.app.dto.account;

import com.bsdenterprise.qbits_chat_licenses.app.config.validator.password.match.PasswordMatch;
import com.bsdenterprise.qbits_chat_licenses.app.config.validator.password.match.common.PasswordChecker;
import com.bsdenterprise.qbits_chat_licenses.app.config.validator.password.policy.ValidPassword;

import javax.validation.Valid;
import javax.validation.constraints.*;

import lombok.*;

@PasswordMatch
@NoArgsConstructor
public @Data class RestorePasswordRequest implements PasswordChecker {

    @Valid
    @ValidPassword
    @NotEmpty(message = "{user.create.empty.password}")
    private String password;

    @Valid
    @ValidPassword
    @NotEmpty(message = "{user.create.empty.confirm-password}")
    private String confirmPassword;

    @Override
    public boolean checkPasswords() {
        return password.equals(confirmPassword);
    }

}

