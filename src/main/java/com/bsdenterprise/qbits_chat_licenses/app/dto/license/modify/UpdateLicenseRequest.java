package com.bsdenterprise.qbits_chat_licenses.app.dto.license.modify;

import javax.validation.constraints.NotNull;
import lombok.Data;

public @Data class UpdateLicenseRequest {

    @NotNull
    private Long licenseId;

    @NotNull
    private Long productId;

}
