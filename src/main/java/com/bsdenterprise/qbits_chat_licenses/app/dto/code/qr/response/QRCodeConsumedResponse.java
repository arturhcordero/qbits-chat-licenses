package com.bsdenterprise.qbits_chat_licenses.app.dto.code.qr.response;

import lombok.*;

@Builder
public @Data class QRCodeConsumedResponse {

    private Integer configuration;
    private String jabberId;

}
