package com.bsdenterprise.qbits_chat_licenses.app.dto.user;

import com.bsdenterprise.qbits_chat_licenses.app.config.validator.cellphone.Phone;
import com.bsdenterprise.qbits_chat_licenses.app.dto.user.phone.UserPhone;

import javax.validation.Valid;
import javax.validation.constraints.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.springframework.util.StringUtils;

import static org.apache.commons.lang3.ObjectUtils.firstNonNull;
import static org.springframework.util.StringUtils.isEmpty;

@Data
@NoArgsConstructor
public class UserInput {

    @Email
    @NotEmpty(message = "{user.create.empty.email}")
    private String email;

    @NotEmpty(message = "{user.create.empty.name}")
    private String name;

    @NotEmpty(message = "{user.create.empty.last-name}")
    private String lastName;

    private String secondLastName;
    private String nickname;

    @Valid
    @Phone
    @NotNull(message = "{user.create.invalid.phone-number}")
    private UserPhone cellPhone;

    @JsonIgnore
    private String phone;

    @JsonIgnore
    private String phoneRegion;

    public UserInput(String email) {
        this.email = email;
    }

    public String getEmail() {
        return StringUtils.trimAllWhitespace(email);
    }

    public String getFullName() {

        String fullName = String.join(" ",
                firstNonNull(name, ""),
                firstNonNull(lastName, ""),
                firstNonNull(secondLastName, ""))
                .trim();

        if (isEmpty(fullName))
            return null;
        return fullName;
    }

}
