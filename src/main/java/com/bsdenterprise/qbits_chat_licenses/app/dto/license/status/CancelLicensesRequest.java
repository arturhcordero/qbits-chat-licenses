package com.bsdenterprise.qbits_chat_licenses.app.dto.license.status;

import javax.validation.constraints.NotNull;

import lombok.Data;

public @Data class CancelLicensesRequest {

    @NotNull
    private Long licenseId;

    private Integer quantity;

}
