package com.bsdenterprise.qbits_chat_licenses.app.dto.qbits_pay.integration.sale.create;

import com.bsdenterprise.qbits_chat_licenses.common.util.Constants;
import lombok.Data;

public @Data class QbitsPaymentMethodRequest {

    private String alias;
    private String jidUuid;
    private String pin = Constants.DEFAULT_PIN_PAYMENT_METHOD;

}
