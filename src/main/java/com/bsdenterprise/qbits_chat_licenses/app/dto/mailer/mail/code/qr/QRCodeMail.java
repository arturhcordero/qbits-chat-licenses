package com.bsdenterprise.qbits_chat_licenses.app.dto.mailer.mail.code.qr;

import lombok.*;

@Data
public class QRCodeMail {

    private User user;
    private User toUser;
    private String code;
    private String redirectUrl;

    @Data
    @NoArgsConstructor
    public static class User {
        private String fullName;
        private String email;
    }

}
