package com.bsdenterprise.qbits_chat_licenses.app.dto.mailer.config;

import lombok.Data;

public @Data class TemplateReference {

    private String key;
    private String subject;
    private String toBucket;
    private String fileNameReference;

}
