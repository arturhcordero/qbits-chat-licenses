package com.bsdenterprise.qbits_chat_licenses.app.dto.organization.create;

import com.bsdenterprise.qbits_chat_licenses.app.dto.address.AddressItemRequest;
import com.bsdenterprise.qbits_chat_licenses.app.dto.user.create.UserRequest;

import java.util.List;

import javax.validation.constraints.*;

import com.bsdenterprise.qbits_chat_licenses.app.type.organization.CommunityType;
import com.bsdenterprise.qbits_chat_licenses.app.type.organization.OrganizationType;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

public @Data class OrganizationRequest {

    private String name;

    private String description;
    private String rfc;

    private List<AddressItemRequest> addresses;

    @NotNull
    private Long organizationTypeId;

    @JsonIgnore
    private OrganizationType organizationType;

    @NotNull
    private Integer communityTypeId;

    @JsonIgnore
    private CommunityType communityType;

    @NotNull
    private UserRequest user;

}
