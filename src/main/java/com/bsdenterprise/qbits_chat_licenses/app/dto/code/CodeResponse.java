package com.bsdenterprise.qbits_chat_licenses.app.dto.code;

import com.bsdenterprise.qbits_chat_licenses.app.type.info.*;
import lombok.Data;

import java.time.LocalDateTime;

public @Data class CodeResponse {

    private String email;
    private String content;
    private CodeType type;
    private CodeStatus status;
    private Long userId;
    private LocalDateTime expiration;

}
