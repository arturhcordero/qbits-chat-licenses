package com.bsdenterprise.qbits_chat_licenses.app.dto.qbits_pay.integration.sale.create;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Builder
public @Data class QBitspaySaleRequest {

    @JsonProperty("card")
    private QbitsPaymentMethodRequest paymentMethod;

    @JsonProperty("sale")
    private QBitsSaleRequest sale;

}
