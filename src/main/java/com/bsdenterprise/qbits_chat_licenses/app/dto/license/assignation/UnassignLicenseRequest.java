package com.bsdenterprise.qbits_chat_licenses.app.dto.license.assignation;

import lombok.*;

import javax.validation.constraints.NotNull;

@NoArgsConstructor
public @Data class UnassignLicenseRequest {

    @NotNull
    private Long licenseId;

    public UnassignLicenseRequest(@NotNull Long licenseId) {
        this.licenseId = licenseId;
    }

}
