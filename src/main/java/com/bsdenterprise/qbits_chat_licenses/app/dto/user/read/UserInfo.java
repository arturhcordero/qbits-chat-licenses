package com.bsdenterprise.qbits_chat_licenses.app.dto.user.read;

import com.bsdenterprise.qbits_chat_licenses.app.dto.catalog.role.RoleInfo;
import com.bsdenterprise.qbits_chat_licenses.app.type.organization.user.GenderReference;

import java.time.LocalDateTime;
import java.time.LocalDate;

import lombok.Data;

public @Data class UserInfo {

    private Long id;
    private String jid;
    private String email;
    private boolean enabled;
    private String name;
    private String lastName;
    private String secondLastName;
    private String fullName;
    private LocalDate birthday;
    private GenderReference genderReference;
    private String nickname;
    private String phone;
    private String phoneRegion;
    private RoleInfo mainRole;
    private LocalDateTime memberSince;

}
