package com.bsdenterprise.qbits_chat_licenses.app.dto.qbits_pay.integration.payment_method.create;

import lombok.Data;

public @Data class QbitsPaymentMethodCreated {

    private String uuid;
    private String customerName;
    private String jidUuid;
    private String alias;
    private String expirationDate;
    private String agreement;
    private String bank;
    private String brand;
    private Integer type;

    private String message;

}
