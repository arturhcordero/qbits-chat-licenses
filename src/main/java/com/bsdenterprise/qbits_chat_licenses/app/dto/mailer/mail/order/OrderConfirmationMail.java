package com.bsdenterprise.qbits_chat_licenses.app.dto.mailer.mail.order;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.license.product.Cost;
import com.bsdenterprise.qbits_chat_licenses.app.type.license.LicenseDurationType;
import com.bsdenterprise.qbits_chat_licenses.app.type.license.LicenseProductType;
import com.bsdenterprise.qbits_chat_licenses.app.type.order.ProductOrderType;
import com.bsdenterprise.qbits_chat_licenses.app.type.organization.auth.RoleType;
import lombok.*;
import org.springframework.util.StringUtils;

import static com.bsdenterprise.qbits_chat_licenses.common.util.Constants.*;

@NoArgsConstructor
public @Data class OrderConfirmationMail {

    private Long id;
    private User user;
    private ProductOrderType type;
    private BigDecimal total;
    private List<OrderDetail> orderDetails;
    private OrderPayment orderPayment;
    private PaymentMethod paymentMethod;

    public String getOwnerLicenseExpiration() {
        return orderDetails.stream()
                .filter(od -> {
                    final UserLicenseAssignment assignment = od.getAssignment();
                    if (assignment != null)
                        return assignment.getEmail().equals(user.getEmail());
                    return false;
                })
                .map(od -> od.getLicense().getExpiration())
                .findFirst().orElse(null);
    }

    public BigDecimal getTax() {
        BigDecimal tax = BigDecimal.ZERO;
        for (OrderDetail od: orderDetails) {
            tax = tax.add(od.getTax());
        }
        return tax;
    }

    public BigDecimal getSubtotal() {
        BigDecimal subtotal = BigDecimal.ZERO;
        for (OrderDetail od: orderDetails){
            subtotal = subtotal.add(od.getPrice());
        }
        return subtotal;
    }

    public String getTypeDescription() {

        boolean multiple = orderDetails.size() > 1 ||
                orderDetails.iterator().next().getQuantity() > 1;

        return type.getDescription().get(multiple ? "plural" : "singular");
    }

    @Data
    @NoArgsConstructor
    public static class User {
        private String fullName;
        private String email;
        private String phone;
        private String phoneRegion;
    }

    @Data
    @NoArgsConstructor
    public static class OrderDetail {

        private Integer quantity;
        private BigDecimal price;
        private BigDecimal tax;
        private BigDecimal discount;
        private BigDecimal subtotal;
        private LicenseProduct licenseProduct;
        private License license;
        private UserLicenseAssignment assignment;
    }

    @Data
    @NoArgsConstructor
    public static class License {
        private LocalDateTime expiration;
        public String getExpiration() {
            return expiration.format(DEFAULT_DATE_FORMATTER);
        }
    }

    @Data
    @NoArgsConstructor
    public static class LicenseProduct {

        private String name;
        private String description;
        private String iconUrl;
        private LicenseProductType type;
        private BigDecimal price;

        public void setCost(Cost cost) {
            price = cost.getPrice(LicenseDurationType.Monthly);
        }

    }

    @Data
    @NoArgsConstructor
    public static class UserLicenseAssignment {
        private String email;
        private RoleType role;
    }

    @Data
    @NoArgsConstructor
    public static class OrderPayment {
        private String alias;
        private String traceNumber;
        private String authorizationCode;
    }

    @Data
    @NoArgsConstructor
    public static class PaymentMethod {
        private String lastDigits;
        private String brand;

        public String getBrand() {
            if (StringUtils.isEmpty(brand) || brand.equals(UNDEFINED))
                return "VISA";
            return brand;
        }
    }

}
