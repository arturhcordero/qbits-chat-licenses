package com.bsdenterprise.qbits_chat_licenses.app.dto.organization_order;

import com.bsdenterprise.qbits_chat_licenses.app.dto.order.ProductOrderRequest;
import com.bsdenterprise.qbits_chat_licenses.app.dto.organization.create.OrganizationRequest;

import javax.validation.constraints.NotNull;

import com.bsdenterprise.qbits_chat_licenses.app.dto.payment.create.PaymentMethodRequest;
import lombok.Data;

public @Data class OrganizationOrderRequest {

    @NotNull
    private OrganizationRequest organization;

    @NotNull
    private ProductOrderRequest order;

    @NotNull
    private PaymentMethodRequest paymentMethod;

    private String saleInvitationUuid;
    private boolean ignoringSaleInvitation;

}
