package com.bsdenterprise.qbits_chat_licenses.app.dto.code;

import javax.validation.constraints.*;
import lombok.Data;

public @Data class CodeRequest {

    @NotEmpty
    private String email;

    private Integer configuration;

}
