package com.bsdenterprise.qbits_chat_licenses.app.dto.payment.migration;

import com.bsdenterprise.qbits_chat_licenses.app.type.payment.PaymentMethodFlow;
import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

public @Data class PaymentMethodMigrationRequest {

    @NotEmpty
    private String jidUuid;

    @NotEmpty
    private String alias;

    @NotEmpty
    private String customerName;

    private String bank;
    private String brand;
    private Integer type;

    private boolean defaultMethod;

    @JsonIgnore
    private PaymentMethodFlow currentFlow = PaymentMethodFlow.CreatingPaymentMethod;

    @JsonIgnore
    private Long organizationId;

}

