package com.bsdenterprise.qbits_chat_licenses.app.dto.organization.create;

import javax.validation.constraints.*;

import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.organization.auth.User;
import lombok.Data;

public @Data class OrganizationCreatedResponse {

    @NotNull
    private Long id;

    @NotNull
    private User owner;

    @NotBlank
    private String name;

}
