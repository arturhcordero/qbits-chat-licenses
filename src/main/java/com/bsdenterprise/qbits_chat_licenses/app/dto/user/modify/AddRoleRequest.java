package com.bsdenterprise.qbits_chat_licenses.app.dto.user.modify;

import javax.validation.constraints.*;

import lombok.Data;

public @Data class AddRoleRequest {

    @NotNull
    private Long roleId;

    @NotEmpty
    private String email;

}
