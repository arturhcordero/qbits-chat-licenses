package com.bsdenterprise.qbits_chat_licenses.app.dto.mailer.integration;

import lombok.Data;

import java.util.List;

public @Data class MailStatus {

    private String identifier;
    private String subject;
    private List<String> emails;

    private boolean sent;

    public MailStatus setSent(boolean sent) {
        this.sent = sent; return this;
    }

}
