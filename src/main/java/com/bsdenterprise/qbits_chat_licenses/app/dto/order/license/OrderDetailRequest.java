package com.bsdenterprise.qbits_chat_licenses.app.dto.order.license;

import com.bsdenterprise.qbits_chat_licenses.app.dto.order.ProductOrderRequest;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.validation.constraints.NotNull;

public @Data class OrderDetailRequest {

    @NotNull
    private Integer quantity;

    @NotNull
    private Long productId;

    @NotNull
    private Long durationId;

    @JsonIgnore
    private Long licenseId;

    private UserLicenseAssignmentRequest assignment;

    @JsonIgnore
    private ProductOrderRequest order;

    @JsonIgnore
    public boolean isLicenseEstablished() {
        return licenseId != null;
    }

}
