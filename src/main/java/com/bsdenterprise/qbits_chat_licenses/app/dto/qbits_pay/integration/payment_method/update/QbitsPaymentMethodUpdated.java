package com.bsdenterprise.qbits_chat_licenses.app.dto.qbits_pay.integration.payment_method.update;

import lombok.Data;

public @Data class QbitsPaymentMethodUpdated {

    private String masterId;
    private String uuid;
    private String jidUuid;
    private String alias;
    private String customerName;
    private String brand;
    private String bank;
    private String last4;
    private String cardStatusCatId;
    private String softDelete;
    private String lastUse;
    private String createdAt;
    private String modifiedAt;
    private String aboutToExpire;
    private String message;
    private Integer type;

    public boolean isActive(){
        return cardStatusCatId.equals("1");
    }
    public boolean isExpired(){
        return cardStatusCatId.equals("2");
    }

}
