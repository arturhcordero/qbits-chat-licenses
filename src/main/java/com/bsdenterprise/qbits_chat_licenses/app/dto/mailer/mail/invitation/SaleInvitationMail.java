package com.bsdenterprise.qbits_chat_licenses.app.dto.mailer.mail.invitation;

import com.bsdenterprise.qbits_chat_licenses.app.type.license.LicenseProductType;
import lombok.*;

@Data
@Builder
public class SaleInvitationMail {

    private User user;
    private LicenseProduct licenseProduct;
    private String saleInvitationUrl;

    @Data
    @NoArgsConstructor
    public static class User {
        private String fullName;
        private String email;
        private String phone;
        private String phoneRegion;
    }

    @Data
    @NoArgsConstructor
    public static class LicenseProduct {
        private String name;
        private String description;
        private String iconUrl;
        private LicenseProductType type;
    }

}
