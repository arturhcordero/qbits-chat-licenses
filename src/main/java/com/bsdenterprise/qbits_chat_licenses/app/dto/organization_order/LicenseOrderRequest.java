package com.bsdenterprise.qbits_chat_licenses.app.dto.organization_order;

import com.bsdenterprise.qbits_chat_licenses.app.dto.order.ProductOrderRequest;

import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.validation.constraints.*;

import lombok.Data;

public @Data class LicenseOrderRequest {

    @JsonIgnore
    private Long organizationId;

    @NotNull
    private ProductOrderRequest order;

    @JsonIgnore
    private String paymentMethodIdentifier;

}
