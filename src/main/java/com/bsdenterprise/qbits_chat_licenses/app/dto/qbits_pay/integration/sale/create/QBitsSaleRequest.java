package com.bsdenterprise.qbits_chat_licenses.app.dto.qbits_pay.integration.sale.create;

import com.bsdenterprise.qbits_chat_licenses.app.dto.qbits_pay.integration.info.BusinessInfo;

import lombok.Data;

public @Data class QBitsSaleRequest {

    private String transactionIdentifier;
    private String total;
    private BusinessInfo businessInfo;
    private String origin;

    public String getSaleType() {

        if (businessInfo != null)
            return businessInfo.getSaleType();

        return null;
    }

    public String getOrigin() {

        if (businessInfo != null)
            return businessInfo.getOrigin();

        return null;

    }

}
