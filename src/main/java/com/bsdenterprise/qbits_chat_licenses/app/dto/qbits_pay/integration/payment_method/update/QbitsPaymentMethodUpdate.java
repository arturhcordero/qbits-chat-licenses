package com.bsdenterprise.qbits_chat_licenses.app.dto.qbits_pay.integration.payment_method.update;

import com.bsdenterprise.qbits_chat_licenses.common.util.Constants;
import lombok.Data;
import org.springframework.util.StringUtils;

import static java.util.UUID.randomUUID;

public @Data class QbitsPaymentMethodUpdate {

    private String expirationDate;
    private String cvv;

    private String jidUuid;
    private String alias;
    private String agreement;

    private final String pin = Constants.DEFAULT_PIN_PAYMENT_METHOD;

}
