package com.bsdenterprise.qbits_chat_licenses.app.dto.mailer.mail.user;

import lombok.*;

@Builder
public @Data class PasswordChangeConfirmationMail {

    private String fullName;

}
