package com.bsdenterprise.qbits_chat_licenses.app.dto.catalog.role;

import lombok.Data;

import java.io.Serializable;

public @Data class RoleInfo implements Serializable {

    private Long id;
    private String code;
    private String name;
    private String description;

}
