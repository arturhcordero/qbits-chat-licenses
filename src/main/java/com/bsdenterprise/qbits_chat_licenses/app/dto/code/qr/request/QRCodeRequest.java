package com.bsdenterprise.qbits_chat_licenses.app.dto.code.qr.request;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

public @Data class QRCodeRequest {

    private String email;
    private Integer configuration;

}
