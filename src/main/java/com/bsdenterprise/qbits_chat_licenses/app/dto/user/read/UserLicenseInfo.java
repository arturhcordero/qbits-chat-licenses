package com.bsdenterprise.qbits_chat_licenses.app.dto.user.read;

import com.bsdenterprise.qbits_chat_licenses.app.dto.license.info.LicenseInfo;

import lombok.Data;

@Data
public class UserLicenseInfo {

    private UserInfo user;
    private LicenseInfo license;

}
