package com.bsdenterprise.qbits_chat_licenses.app.dto.mailer.config;

import com.bsdenterprise.qbits_chat_licenses.app.type.mailer.config.MailTemplate;

import java.util.Map;
import lombok.Data;

public @Data class Group {

    private String bucket;
    private String path;

    private Sender sender;
    private Map<MailTemplate, TemplateReference> templates;

}
