package com.bsdenterprise.qbits_chat_licenses.app.dto.account;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class ChangePasswordRequest {

    @NotEmpty
    private String email;

    @NotEmpty
    private String currentPassword;

    @NotEmpty
    private String newPassword;

    @NotEmpty
    private String repeatNewPassword;

}
