package com.bsdenterprise.qbits_chat_licenses.app.dto.address;

import lombok.Data;

@Data
public class AddressItemRequest {

    private String address;
    private String state;
    private String city;
    private String zipCode;

}
