package com.bsdenterprise.qbits_chat_licenses.app.dto.license.product;

import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.license.product.Cost;
import com.bsdenterprise.qbits_chat_licenses.app.type.license.LicenseProductType;
import lombok.Data;

import java.util.List;

@Data
public class LicenseProductInfo {

    private Long id;
    private String code;
    private String name;
    private String description;
    private String shortDescription;
    private String abbreviation;
    private String recordingUrl;
    private String iconUrl;
    private String color;
    private boolean defaultProduct;

    private LicenseProductType type = LicenseProductType.SingleLicense;

    private Cost cost;
    private Long costId;

    public List<FeatureInfo> features;
    
}
