package com.bsdenterprise.qbits_chat_licenses.app.dto.account;

import javax.validation.constraints.NotEmpty;
import lombok.Data;

@Data
public class ForgotPasswordRequest {

    @NotEmpty
    private String email;

}
