package com.bsdenterprise.qbits_chat_licenses.app.dto.license.status;

import lombok.Data;

import javax.validation.constraints.*;

public @Data class RenovateLicenseRequest {

    @NotNull
    private Long licenseId;

}
