package com.bsdenterprise.qbits_chat_licenses.app.dto.organization_order;

import com.bsdenterprise.qbits_chat_licenses.app.dto.order.ProductOrderRequest;
import com.bsdenterprise.qbits_chat_licenses.app.dto.organization.create.OrganizationRequest;
import com.bsdenterprise.qbits_chat_licenses.app.dto.payment.create.PaymentMethodRequest;
import com.bsdenterprise.qbits_chat_licenses.app.dto.payment.migration.PaymentMethodMigrationRequest;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

public @Data class OrganizationOrderMigrationRequest {

    @NotNull
    private OrganizationRequest organization;

    @NotNull
    private ProductOrderRequest order;

    @NotNull
    private List<PaymentMethodMigrationRequest> paymentMethods;

}
