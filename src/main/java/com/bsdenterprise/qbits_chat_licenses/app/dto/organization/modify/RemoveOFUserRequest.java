package com.bsdenterprise.qbits_chat_licenses.app.dto.organization.modify;

import lombok.Data;

@Data
public class RemoveOFUserRequest {

    private String username;

}
