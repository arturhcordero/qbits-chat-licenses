package com.bsdenterprise.qbits_chat_licenses.app.dto.mailer.mail.user;

import lombok.Builder;
import lombok.Data;

@Builder
public @Data class ForgotPasswordMail {

    private String restorePasswordUrl;

}
