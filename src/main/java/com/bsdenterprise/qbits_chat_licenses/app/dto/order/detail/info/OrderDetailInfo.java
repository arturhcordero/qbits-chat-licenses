package com.bsdenterprise.qbits_chat_licenses.app.dto.order.detail.info;

import com.bsdenterprise.qbits_chat_licenses.app.dto.license.product.LicenseProductInfo;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

import lombok.Data;

@Data
public class OrderDetailInfo {

    private Long id;
    private LocalDateTime createdAt;
    private Integer quantity;
    private BigDecimal price;
    private BigDecimal tax;
    private BigDecimal discount;
    private BigDecimal subtotal;
    private Long licenseProductId;
    private LicenseProductInfo licenseProduct;

    private LocalDate periodStartDate;
    private LocalDate periodEndDate;

}
