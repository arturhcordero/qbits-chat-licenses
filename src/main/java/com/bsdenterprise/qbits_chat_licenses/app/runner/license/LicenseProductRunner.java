package com.bsdenterprise.qbits_chat_licenses.app.runner.license;

import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.license.product.*;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.repository.license.FeatureRepository;
import com.bsdenterprise.qbits_chat_licenses.app.type.license.LicenseDurationType;

import com.bsdenterprise.qbits_chat_licenses.app.persistence.repository.license.LicenseProductRepository;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.repository.product.CostRepository;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.repository.product.TaxRepository;

import org.springframework.core.annotation.Order;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.boot.CommandLineRunner;
import com.google.common.collect.ImmutableMap;

import lombok.extern.slf4j.Slf4j;
import lombok.*;

import java.math.*;
import java.util.*;

@Slf4j
@Component
@Order(1)
public class LicenseProductRunner implements CommandLineRunner {

    @Setter(onMethod = @__(@Autowired))
    private LicenseProductRepository licenseProductRepository;

    @Setter(onMethod = @__(@Autowired))
    private TaxRepository taxRepository;

    @Setter(onMethod = @__(@Autowired))
    private CostRepository costRepository;

    @Setter(onMethod = @__(@Autowired))
    private FeatureRepository featureRepository;

    @AllArgsConstructor @Data
    class LicenseProductData {
        private BigDecimal cost;
        private List<String> codes;
    }

    @Override
    public void run(String... args) throws Exception {

        List<Feature> features = Arrays.asList(
                new Feature("M_IND","Mensajes individuales"),
                new Feature("GR_ENC","Grupos encriptados"),
                new Feature("PRIV","Privacidad"),
                new Feature("A_DESTRUCT","Autodestrucción"),
                new Feature("CONT_TONE","Tono por contacto"),
                new Feature("PIN_BLOCK","Bloqueo por PIN"),
                new Feature("READ_GO","Read&Go para conversaciones y multimedia"),
                new Feature("WATER_MARK","Marca de agua en Read&Go"),
                new Feature("USR_D","Disociar usuarios")
        );

        createFeatures(features);

        createLicenseProducts(features,
                ImmutableMap.<LicenseProduct, LicenseProductData>builder()
                    .put(new LicenseProduct(
                            "SHIELD",
                            "SHIELD",
                            "Mensajes individuales y en grupos encriptados, añadir usuarios por QR",
                            "SHIELD",
                            "SH",
                            "https://s3.amazonaws.com/qbits-chat/gallery/Shield.mp4",
                            "https://s3.amazonaws.com/qbits-chat/gallery/icons/ico_shield.png",
                            "#15AA06",
                            true,
                            1
                    ), new LicenseProductData(BigDecimal.valueOf(50),
                            Arrays.asList(
                                    "M_IND", "GR_ENC"
                            )))
                    .put(new LicenseProduct(
                            "TANK",
                            "TANK",
                            "Privacidad, autodestrucción, tono por contacto, bloqueo por PIN",
                            "TANK",
                            "TK",
                            "https://s3.amazonaws.com/qbits-chat/gallery/Tank.mp4",
                            "https://s3.amazonaws.com/qbits-chat/gallery/icons/ico_tank.png",
                            "#E42507",
                            false,
                            2
                    ), new LicenseProductData(BigDecimal.valueOf(400),
                            Arrays.asList(
                                    "M_IND", "GR_ENC",
                                    "PRIV", "A_DESTRUCT", "CONT_TONE", "PIN_BLOCK"
                            )))
                    .put(new LicenseProduct(
                            "BUNKER",
                            "BUNKER",
                            "Read&Go, para conversaciones y multimedia",
                            "BUNKER",
                            "BK",
                            "https://s3.amazonaws.com/qbits-chat/gallery/Bunker.mp4",
                            "https://s3.amazonaws.com/qbits-chat/gallery/icons/ico_bunker.png",
                            "#2496F0",
                            false,
                            3
                    ), new LicenseProductData(BigDecimal.valueOf(750),
                            Arrays.asList(
                                    "M_IND", "GR_ENC",
                                    "PRIV", "A_DESTRUCT", "CONT_TONE", "PIN_BLOCK",
                                    "READ_GO", "GR_ENC"
                            )))
                    .put(new LicenseProduct(
                            "PENTAGON",
                            "PENTAGON",
                            "Mensajes individuales y en grupos encriptados, añadir usuarios por QR",
                            "PENTAGON",
                            "PG",
                            "https://s3.amazonaws.com/qbits-chat/gallery/Pentagon.mp4",
                            "https://s3.amazonaws.com/qbits-chat/gallery/icons/ico_pentagon.png",
                            "#FFB900",
                            false,
                            4
                    ), new LicenseProductData(BigDecimal.valueOf(1500),
                            Arrays.asList(
                                    "M_IND", "GR_ENC",
                                    "PRIV", "A_DESTRUCT", "CONT_TONE", "PIN_BLOCK",
                                    "READ_GO", "GR_ENC",
                                    "WATER_MARK", "USR_D"
                            )))
                .build()
        );

    }

    @Transactional
    public void createLicenseProducts(List<Feature> features, ImmutableMap<LicenseProduct, LicenseProductData> licenseProducts) {

        Tax tax = createMainTax();

        licenseProducts.forEach((licenseProduct, data) -> {

            if (!licenseProductRepository.existsByCode(licenseProduct.getCode())) {

                Cost cost = costRepository.save(new Cost(calculateCost(data.getCost()), tax));

                licenseProduct.setCost(cost);

                List<LicenseProductFeature> licenseProductFeatures = retrieveFeatures(
                        licenseProduct, features, data.getCodes()
                );

                licenseProduct.setProductFeatures(licenseProductFeatures);

                licenseProductRepository.save(licenseProduct);
            }

        });

    }

    private BigDecimal calculateCost(BigDecimal totalCost) {

        BigDecimal yearMonths = BigDecimal.valueOf(LicenseDurationType.YearMonths);
        BigDecimal yearDays = BigDecimal.valueOf(LicenseDurationType.YearDays);

        MathContext mathContext = new MathContext(14);

        return totalCost
                .multiply(yearMonths, mathContext)
                .divide(yearDays, mathContext);
    }

    private Tax createMainTax() {

        final String code = "MEXICAN_IVA";

        Optional<Tax> optionalTax = taxRepository.findByCode(code);

        if (optionalTax.isPresent())
            return optionalTax.get();

        Tax tax = new Tax();
        tax.setCode("MEXICAN_IVA");
        tax.setDescription("Mexican Tax (IVA)");
        tax.setValue(BigDecimal.valueOf(16));

        return taxRepository.save(tax);
    }

    @Transactional
    public void createFeatures(List<Feature> features) {

        features.forEach(feature -> {
            if (!featureRepository.existsByCode(feature.getCode()))
                featureRepository.save(feature);
        });

    }

    private List<LicenseProductFeature> retrieveFeatures(LicenseProduct licenseProduct,
                                                         List<Feature> features, List<String> codes) {

        List<LicenseProductFeature> featuresList = new LinkedList<>();

        for (String code : codes) {

            Optional<Feature> optionalFeature = features.stream()
                    .filter(feature -> feature.getCode().equals(code))
                    .findFirst();

            optionalFeature.ifPresent(feature ->
                    featuresList.add(new LicenseProductFeature(licenseProduct, feature)));

        }

        return featuresList;
    }

}
