package com.bsdenterprise.qbits_chat_licenses.app.runner.order;

import com.bsdenterprise.qbits_chat_licenses.app.controller.order.ProductOrderController;
import com.bsdenterprise.qbits_chat_licenses.app.dto.order.ProductOrderRequest;
import com.bsdenterprise.qbits_chat_licenses.app.dto.order.license.OrderDetailRequest;
import com.bsdenterprise.qbits_chat_licenses.app.dto.order.license.UserLicenseAssignmentRequest;
import com.bsdenterprise.qbits_chat_licenses.app.dto.organization.create.OrganizationRequest;
import com.bsdenterprise.qbits_chat_licenses.app.dto.organization_order.OrganizationOrderRequest;
import com.bsdenterprise.qbits_chat_licenses.app.dto.payment.create.PaymentMethodRequest;
import com.bsdenterprise.qbits_chat_licenses.app.dto.user.create.UserRequest;
import com.bsdenterprise.qbits_chat_licenses.app.dto.user.phone.UserPhone;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.license.product.*;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.repository.license.FeatureRepository;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.repository.license.LicenseProductRepository;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.repository.product.CostRepository;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.repository.product.TaxRepository;
import com.bsdenterprise.qbits_chat_licenses.app.service.organization.UserService;
import com.bsdenterprise.qbits_chat_licenses.app.type.license.LicenseDurationType;
import com.bsdenterprise.qbits_chat_licenses.app.type.organization.CommunityType;
import com.bsdenterprise.qbits_chat_licenses.app.type.organization.OrganizationType;
import com.bsdenterprise.qbits_chat_licenses.app.type.organization.auth.RoleType;
import com.bsdenterprise.qbits_chat_licenses.app.type.organization.auth.UserActivationType;
import com.bsdenterprise.qbits_chat_licenses.app.type.organization.user.GenderReference;
import com.google.common.collect.ImmutableMap;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Slf4j
@Component
@Order(3)
public class ProductOrderRunner implements CommandLineRunner {

    @Setter(onMethod = @__(@Autowired))
    private ProductOrderController productOrderController;

    @Setter(onMethod = @__(@Autowired))
    private UserService userService;

    @Override
    public void run(String... args) throws Exception {

        final String email = "arturh.sw@gmail.com";

        if (!userService.findUserByEmail(email).isPresent()) {

            UserLicenseAssignmentRequest userLicenseAssignment = new UserLicenseAssignmentRequest();
            userLicenseAssignment.setEmail(email);
            userLicenseAssignment.setRoleId(RoleType.Owner.getIdentifier());

            OrderDetailRequest orderDetail = new OrderDetailRequest();
            orderDetail.setAssignment(userLicenseAssignment);
            orderDetail.setProductId(1L);
            orderDetail.setDurationId(LicenseDurationType.Monthly.getIdentifier());
            orderDetail.setQuantity(1);

            List<OrderDetailRequest> orderDetails = new LinkedList<>();
            orderDetails.add(orderDetail);

            ProductOrderRequest productOrder = new ProductOrderRequest();
            productOrder.setOrderDetails(orderDetails);

            UserPhone userPhone = new UserPhone();
            userPhone.setPhoneRegion("MX");
            userPhone.setPhone("812751055");

            UserRequest user = new UserRequest();
            user.setEmail(email);
            user.setActivationType(UserActivationType.Automatic);
            user.setName("Arturo");
            user.setLastName("Cordero");
            user.setSecondLastName("Muñiz");
            user.setBirthdaySt("22/11/1996");
            user.setPassword("jaqart_56923");
            user.setConfirmPassword("jaqart_56923");
            user.setGenderReferenceId(GenderReference.Male.getIdentifier());
            user.setCellPhone(userPhone);

            OrganizationRequest organization = new OrganizationRequest();
            organization.setCommunityTypeId(CommunityType.Open.getIdentifier());
            organization.setOrganizationTypeId(OrganizationType.Physical.getIdentifier());
            organization.setName(user.getFullName());
            organization.setDescription(user.getFullName());
            organization.setRfc("XEXX010101000");
            organization.setUser(user);

            PaymentMethodRequest paymentMethod = new PaymentMethodRequest();
            paymentMethod.setCustomerName("Arturo Cordero Muñiz");
            paymentMethod.setCardNumber("4152313244290727");
            paymentMethod.setExpirationDate("08/21");
            paymentMethod.setSecurityCode("698");

            OrganizationOrderRequest organizationOrder = new OrganizationOrderRequest();
            organizationOrder.setOrder(productOrder);
            organizationOrder.setOrganization(organization);
            organizationOrder.setPaymentMethod(paymentMethod);
            organizationOrder.setIgnoringSaleInvitation(true);

            productOrderController.createOrganizationOrder(organizationOrder);

        }

    }

}
