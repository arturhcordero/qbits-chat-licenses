package com.bsdenterprise.qbits_chat_licenses.app.runner.catalog;

import static com.bsdenterprise.qbits_chat_licenses.app.config.access.PermissionType.*;
import static com.bsdenterprise.qbits_chat_licenses.app.type.organization.auth.RoleType.*;
import static com.google.common.collect.ImmutableList.of;
import static java.util.Arrays.*;

import com.bsdenterprise.qbits_chat_licenses.app.config.access.PermissionType;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.organization.auth.RolePermission;
import com.bsdenterprise.qbits_chat_licenses.app.type.organization.auth.RoleType;

import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.organization.auth.Permission;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.organization.auth.Role;

import com.bsdenterprise.qbits_chat_licenses.app.persistence.repository.catalog.PermissionRepository;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.repository.catalog.RolePermissionRepository;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.repository.catalog.RoleRepository;

import com.bsdenterprise.qbits_chat_licenses.common.util.MapperUtil;

import com.google.common.collect.ImmutableMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.*;

import lombok.Setter;

@Component
@Order(2)
public class RoleRunner implements CommandLineRunner {

    @Setter(onMethod = @__(@Autowired))
    private RoleRepository roleRepository;

    @Setter(onMethod = @__(@Autowired))
    private PermissionRepository permissionRepository;

    @Setter(onMethod = @__(@Autowired))
    private RolePermissionRepository rolePermissionRepository;

    @Setter(onMethod = @__({@Autowired, @Qualifier("commonMapper")}))
    private MapperUtil mapperUtil;

    @Override
    public void run(String... args) throws Exception {

        createRoles(
                ImmutableMap.<RoleType, List<PermissionType>>builder()
                        .put(Owner, asList(
                                ManageUsersLicenses,
                                UpdateLicense,
                                ManagePaymentMethods,
                                ReadBillingInformation,
                                ChangePassword,
                                CancellMembership,
                                SendSaleInvitation
                        ))
                        .put(Admin, asList(
                                ManageUsersLicenses,
                                UpdateLicense,
                                ManagePaymentMethods,
                                ReadBillingInformation,
                                ChangePassword,
                                CancellMembership,
                                SendSaleInvitation
                        ))
                        .put(User, asList(
                                ChangePassword,
                                CancellMembership,
                                SendSaleInvitation
                        ))
                        .put(Salesman, of())
                        .put(Validator, of())
                        .build()
        );

    }

    private void createRoles(ImmutableMap<RoleType, List<PermissionType>> roles) {

        Map<PermissionType, Permission> savedPermissions = new HashMap<>();

        roles.forEach((roleType, permissionsList) -> {

            if (!roleRepository.existsByCode(roleType.getCode())) {

                Role role = roleRepository.save(mapperUtil.map(roleType, Role.class));

                permissionsList.forEach(p -> {

                    if (savedPermissions.containsKey(p) ||
                            !permissionRepository.existsByName(p.name())) {

                        if (!savedPermissions.containsKey(p))
                            savedPermissions.put(p, permissionRepository.save(new Permission(p.name())));

                        rolePermissionRepository.save(new RolePermission(role, savedPermissions.get(p)));
                    }

                });

            }

        });

    }

}
