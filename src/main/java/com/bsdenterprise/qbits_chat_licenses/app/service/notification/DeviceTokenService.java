package com.bsdenterprise.qbits_chat_licenses.app.service.notification;

import static com.bsdenterprise.qbits_chat_licenses.common.config.message.MessageCode.UNEXPECTED_ERROR;
import static com.bsdenterprise.qbits_chat_licenses.app.type.notification.PlatformType.findPlatformType;
import static com.bsdenterprise.qbits_chat_licenses.app.type.notification.DeviceType.findDeviceType;

import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.model.DeleteEndpointRequest;
import com.bsdenterprise.qbits_chat_licenses.app.config.aws.SNSConfig;
import com.bsdenterprise.qbits_chat_licenses.app.dto.notification.device.DeviceTokenRequest;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.organization.auth.User;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.repository.notification.DeviceTokenRepository;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.notification.DeviceToken;

import com.bsdenterprise.qbits_chat_licenses.app.service.organization.UserService;
import com.bsdenterprise.qbits_chat_licenses.app.type.notification.DeviceType;
import com.bsdenterprise.qbits_chat_licenses.app.type.notification.PlatformType;

import com.bsdenterprise.qbits_chat_licenses.common.config.exception.model.ValidationException;
import com.bsdenterprise.qbits_chat_licenses.common.service.BaseService;

import com.amazonaws.services.sns.model.CreatePlatformEndpointRequest;
import com.amazonaws.services.sns.model.CreatePlatformEndpointResult;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.Authentication;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import lombok.extern.slf4j.Slf4j;

import lombok.*;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Slf4j
@Service
public class DeviceTokenService extends BaseService<DeviceTokenRepository, DeviceToken> {

    @Setter(onMethod = @__(@Autowired))
    private AmazonSNS snsClient;

    @Setter(onMethod = @__(@Autowired))
    private SNSConfig snsConfig;

    @Setter(onMethod = @__(@Autowired))
    private UserService userService;

    @SneakyThrows
    @Transactional
    public void storeToken(DeviceTokenRequest deviceTokenRequest) {

        User user = userService.findUserByEmail(authentication().getName())
                .orElseThrow(() -> new ValidationException(UNEXPECTED_ERROR));

        final String token = deviceTokenRequest.getToken();

        DeviceType deviceType = findDeviceType(deviceTokenRequest.getDeviceTypeId())
                .orElseThrow(() -> new ValidationException(UNEXPECTED_ERROR));

        PlatformType platformType = findPlatformType(deviceTokenRequest.getPlatformTypeId())
                .orElseThrow(() -> new ValidationException(UNEXPECTED_ERROR));

        String platformApplicationArn = snsConfig.getPlatformApplications().get(platformType);

        Optional<DeviceToken> optionalDeviceToken = repository.findByUserId(user.getId());

        if (optionalDeviceToken.isPresent()) {
            DeviceToken deviceToken = optionalDeviceToken.get();
            if (!deviceToken.getToken().equals(token)) {
                String platformEndpointArn = updatePlatformEndpoint(platformApplicationArn,
                        deviceToken.getPlatformEndpointArn(), token);
                repository.updatePlatformEndpoint(platformEndpointArn, deviceToken.getId());
            }
        } else {

            DeviceToken deviceToken = new DeviceToken(token, deviceType, platformType);
            deviceToken.setPlatformEndpointArn(createPlatformEndpoint(platformApplicationArn, token));
            deviceToken.setUser(user);

            save(deviceToken);
        }

    }

    private String updatePlatformEndpoint(String platformApplicationArn,
                                          String platformEndpointArn, String token) {

        snsClient.deleteEndpoint(new DeleteEndpointRequest().withEndpointArn(platformEndpointArn));

        CreatePlatformEndpointResult platformEndpointResult =
                snsClient.createPlatformEndpoint(
                        new CreatePlatformEndpointRequest()
                                .withPlatformApplicationArn(platformApplicationArn)
                                .withToken(token));

        return platformEndpointResult.getEndpointArn();
    }

    private String createPlatformEndpoint(String platformApplicationArn, String token) {

        CreatePlatformEndpointResult platformEndpointResult =
                snsClient.createPlatformEndpoint(
                        new CreatePlatformEndpointRequest()
                                .withPlatformApplicationArn(platformApplicationArn)
                                .withToken(token));

        return platformEndpointResult.getEndpointArn();
    }

    private Authentication authentication() {
        return SecurityContextHolder.getContext().getAuthentication();
    }

}
