package com.bsdenterprise.qbits_chat_licenses.app.service.license;

import com.bsdenterprise.qbits_chat_licenses.app.dto.license.product.FeatureInfo;
import com.bsdenterprise.qbits_chat_licenses.app.dto.license.product.LicenseProductInfo;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.license.product.Feature;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.license.product.LicenseProductFeature;
import com.bsdenterprise.qbits_chat_licenses.app.type.license.LicenseProductType;
import com.bsdenterprise.qbits_chat_licenses.common.config.exception.model.ValidationException;
import com.bsdenterprise.qbits_chat_licenses.common.service.BaseService;

import com.bsdenterprise.qbits_chat_licenses.app.persistence.repository.license.LicenseProductRepository;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.license.product.LicenseProduct;

import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.stream.Collectors;

import static com.bsdenterprise.qbits_chat_licenses.app.util.TypeValidatorUtil.validateLicenseProductType;

@Slf4j
@Service
@CacheConfig(cacheNames = "licenseProducts")
public class LicenseProductService extends
        BaseService<LicenseProductRepository, LicenseProduct> {

    public LicenseProduct findLicenseProduct(Long licenseProductId) {
        return findById(licenseProductId);
    }

    @Cacheable
    public List<LicenseProductInfo> findLicenseProducts(String code, Long licenseProductTypeId) throws ValidationException {

        LicenseProductType type = validateLicenseProductType(licenseProductTypeId, false);

        if (type == null)
            type = LicenseProductType.SingleLicense;

        List<LicenseProduct> licenseProducts = repository.findLicenseProducts(code, type.name());

        return licenseProducts.stream().map(lp -> {

            List<Feature> features = lp.getProductFeatures().stream()
                    .map(LicenseProductFeature::getFeature)
                    .collect(Collectors.toList());

            LicenseProductInfo licenseProductInfo = mapperUtil().map(lp, LicenseProductInfo.class);

            licenseProductInfo.setFeatures(mapperUtil().map(features, FeatureInfo.class));

            return licenseProductInfo;

        }).collect(Collectors.toList());

    }

}
