package com.bsdenterprise.qbits_chat_licenses.app.service.order.order_detail;

import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.order.OrderDetail;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.repository.order.order_detail.OrderDetailRepository;
import com.bsdenterprise.qbits_chat_licenses.common.service.BaseService;

import org.springframework.stereotype.Service;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class OrderDetailService extends BaseService<OrderDetailRepository, OrderDetail> {

    public void setOrderDetailLicense(Long licenseId, Long orderDetailId) {
        repository.setOrderDetailLicense(licenseId, orderDetailId);
    }

    public OrderDetail findLastOrderDetail(Long licenseId) {
        return repository.findLastOrderDetail(licenseId);
    }

}
