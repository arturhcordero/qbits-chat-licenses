package com.bsdenterprise.qbits_chat_licenses.app.service.code;

import static com.bsdenterprise.qbits_chat_licenses.common.config.message.MessageCode.UNEXPECTED_ERROR;
import static com.bsdenterprise.qbits_chat_licenses.app.util.IdentifierUtil.*;

import static com.bsdenterprise.qbits_chat_licenses.app.type.mailer.config.MailTemplate.*;
import static com.bsdenterprise.qbits_chat_licenses.app.type.mailer.config.GroupType.*;
import static com.bsdenterprise.qbits_chat_licenses.app.type.info.CodeStatus.*;
import static com.bsdenterprise.qbits_chat_licenses.common.util.Constants.EMAIL_VERIFICATION_CODE_LENGTH;

import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.code.VerificationCode;
import com.bsdenterprise.qbits_chat_licenses.app.service.organization.UserService;
import com.bsdenterprise.qbits_chat_licenses.common.config.exception.model.ValidationException;

import com.bsdenterprise.qbits_chat_licenses.app.dto.code.CodeRequest;
import com.bsdenterprise.qbits_chat_licenses.app.dto.mailer.integration.MailRecipients;

import com.bsdenterprise.qbits_chat_licenses.common.service.BaseService;
import com.bsdenterprise.qbits_chat_licenses.app.service.mailer.MailerService;

import com.bsdenterprise.qbits_chat_licenses.app.persistence.repository.code.VerificationCodeRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import lombok.Setter;

@Slf4j
@Service
public class VerificationCodeService extends BaseService<VerificationCodeRepository, VerificationCode> {

    @Setter(onMethod = @__(@Autowired))
    private MailerService mailerService;

    @Setter(onMethod = @__(@Autowired))
    private UserService userService;

    public void generateVerificationCode(CodeRequest codeRequest) throws Exception {

        final String email = codeRequest.getEmail();
        final String code = searchAvailableVerificationCode(email);

        VerificationCode verificationCode = new VerificationCode();
        verificationCode.setEmail(email);
        verificationCode.setCode(code);
        verificationCode.setStatus(Created);

        save(verificationCode);
        enqueueVerificationMail(verificationCode);

        mailerService.sendMailQueue();
    }

    private String searchAvailableVerificationCode(String email) {

        String code = generateRandomCode(EMAIL_VERIFICATION_CODE_LENGTH);

        while (repository.exists(email, code))
            code = generateRandomCode(EMAIL_VERIFICATION_CODE_LENGTH);

        return code;
    }

    public void verifyCode(String email, String code) throws ValidationException {
        if (!repository.canVerifyCode(email, code))
            throw new ValidationException(UNEXPECTED_ERROR);
        repository.markCodeAsUsed(code);
    }

    private void enqueueVerificationMail(VerificationCode verificationCode) throws Exception {
        mailerService.enqueueMail(QBITS_CHAT, EMAIL_VERIFICATION_CODE,
                new MailRecipients(verificationCode.getEmail()), verificationCode);
    }

    private Authentication authentication() {
        return SecurityContextHolder.getContext().getAuthentication();
    }

}
