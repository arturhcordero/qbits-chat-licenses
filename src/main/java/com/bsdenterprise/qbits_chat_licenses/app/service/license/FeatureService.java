package com.bsdenterprise.qbits_chat_licenses.app.service.license;

import com.bsdenterprise.qbits_chat_licenses.app.dto.license.product.FeatureInfo;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.license.product.Feature;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.repository.license.FeatureRepository;
import com.bsdenterprise.qbits_chat_licenses.common.service.BaseService;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@CacheConfig(cacheNames = "features")
public class FeatureService extends BaseService<FeatureRepository,Feature>{

    @Cacheable
    public List<FeatureInfo> findFeatures(){
        return findAll(FeatureInfo.class);
    }

}
