package com.bsdenterprise.qbits_chat_licenses.app.service.license;

import com.bsdenterprise.qbits_chat_licenses.app.dto.license.assignation.UnassignLicenseRequest;
import com.bsdenterprise.qbits_chat_licenses.app.dto.license.info.LicenseInfo;
import com.bsdenterprise.qbits_chat_licenses.app.dto.license.modify.UpdateLicenseRequest;
import com.bsdenterprise.qbits_chat_licenses.app.dto.license.status.RenovateLicenseRequest;
import com.bsdenterprise.qbits_chat_licenses.app.dto.license.status.CancelLicensesRequest;
import com.bsdenterprise.qbits_chat_licenses.app.dto.notification.request.APNSNotificationRequest;
import com.bsdenterprise.qbits_chat_licenses.app.dto.notification.request.APNSNotificationRequest.Alert;
import com.bsdenterprise.qbits_chat_licenses.app.dto.notification.request.APNSNotificationRequest.Aps;
import com.bsdenterprise.qbits_chat_licenses.app.dto.notification.request.GCMNotificationRequest;
import com.bsdenterprise.qbits_chat_licenses.app.dto.notification.request.GCMNotificationRequest.Notification;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.license.product.LicenseProduct;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.notification.DeviceToken;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.order.OrderDetail;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.order.ProductOrder;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.organization.auth.Role;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.organization.auth.UserOrganizationRole;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.repository.license.UserLicenseAssignmentRepository;
import com.bsdenterprise.qbits_chat_licenses.app.service.notification.NotificationService;
import com.bsdenterprise.qbits_chat_licenses.app.service.order.ProductOrderService;
import com.bsdenterprise.qbits_chat_licenses.app.service.order.order_detail.OrderDetailService;
import com.bsdenterprise.qbits_chat_licenses.app.service.organization.UserOrganizationService;
import com.bsdenterprise.qbits_chat_licenses.app.type.license.LicenseActionFlow;
import com.bsdenterprise.qbits_chat_licenses.app.type.license.LicenseDurationType;
import com.bsdenterprise.qbits_chat_licenses.app.type.license.LicenseStatusType;
import com.bsdenterprise.qbits_chat_licenses.common.config.exception.model.ValidationException;
import com.bsdenterprise.qbits_chat_licenses.common.config.message.MessageCode;
import com.bsdenterprise.qbits_chat_licenses.common.service.BaseService;

import com.bsdenterprise.qbits_chat_licenses.app.persistence.repository.license.LicenseRepository;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.license.License;

import static com.bsdenterprise.qbits_chat_licenses.app.type.license.LicenseActionFlow.*;
import static com.bsdenterprise.qbits_chat_licenses.app.type.organization.auth.RoleType.Owner;
import static com.bsdenterprise.qbits_chat_licenses.app.util.TypeValidatorUtil.validateRole;

import static com.bsdenterprise.qbits_chat_licenses.app.type.license.LicenseStatusType.*;
import static com.bsdenterprise.qbits_chat_licenses.app.type.mailer.config.MailTemplate.*;
import static com.bsdenterprise.qbits_chat_licenses.app.type.mailer.config.GroupType.QBITS_CHAT;

import com.bsdenterprise.qbits_chat_licenses.app.dto.license.assignation.AssignLicenseRequest;
import com.bsdenterprise.qbits_chat_licenses.app.dto.mailer.mail.user.UserLicenseMail;
import com.bsdenterprise.qbits_chat_licenses.app.dto.mailer.integration.MailRecipients;
import com.bsdenterprise.qbits_chat_licenses.app.dto.user.create.UserRequest;

import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.license.UserLicenseAssignment;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.organization.auth.User;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.organization.*;

import com.bsdenterprise.qbits_chat_licenses.app.service.mailer.MailerService;
import com.bsdenterprise.qbits_chat_licenses.app.service.organization.OrganizationService;
import com.bsdenterprise.qbits_chat_licenses.app.service.organization.UserService;

import com.bsdenterprise.qbits_chat_licenses.app.type.organization.auth.RoleType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.core.env.Environment;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.Authentication;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import lombok.extern.slf4j.Slf4j;
import lombok.Setter;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
public class LicenseService extends BaseService<LicenseRepository, License> {

    @Setter(onMethod = @__(@Autowired))
    private UserService userService;

    @Setter(onMethod = @__(@Autowired))
    private OrganizationService organizationService;

    @Setter(onMethod = @__(@Autowired))
    private UserOrganizationService userOrganizationService;

    @Setter(onMethod = @__(@Autowired))
    private ProductOrderService productOrderService;

    @Setter(onMethod = @__(@Autowired))
    private OrderDetailService orderDetailService;

    @Setter(onMethod = @__(@Autowired))
    private MailerService mailerService;

    @Setter(onMethod = @__(@Autowired))
    private NotificationService notificationService;

    @Setter(onMethod = @__(@Autowired))
    private UserLicenseAssignmentRepository userLicenseAssignmentRepository;

    @Setter(onMethod = @__(@Autowired))
    private Environment env;

    public LicenseInfo myLicense() throws ValidationException {

        User user = userService.findUserByEmail(authentication().getName())
                .orElseThrow(() -> new ValidationException(MessageCode.UNEXPECTED_ERROR));

        return mapperUtil().map(user.getUserOrganization().getLicense(), LicenseInfo.class);
    }

    @Transactional
    @SuppressWarnings("Duplicates")
    public void assignLicense(AssignLicenseRequest assignLicense) throws Exception {

        final String email = assignLicense.getEmail();
        final RoleType role = validateRole(assignLicense.getRoleId());

        License license = findLicense(assignLicense.getLicenseId());

        if (!license.getOrganization().getOwnerEmail().equals(authentication().getName()))
            throw new ValidationException(MessageCode.UNEXPECTED_ERROR);

        if (license.getStatus() == Inactive)
            throw new ValidationException(MessageCode.UNEXPECTED_ERROR);

        Optional<User> userFound = userService.findUserByEmail(email);

        if (userFound.isPresent()) {

            User user = userFound.get();
            UserOrganization userOrganization = user.getUserOrganization();

            if (user.hasLicense())
                throw new ValidationException(MessageCode.UNEXPECTED_ERROR);

            license.setUserOrganization(userOrganization);
            license.setStatus(Active);
            save(license);

            userOrganization.establishMainRole(getReference(Role.class, role.getIdentifier()));
            userOrganization.setLicense(license);

            userOrganizationService.updateUserOrganization(userOrganization);

            LicenseActionFlow flow = LicenseAssignment;

            if (!user.isEnabled())
                flow = LicenseAssignmentNonExistentUser;

            notifyLicenseAction(flow, license, userOrganization);

        } else {

            UserLicenseAssignment assignment = new UserLicenseAssignment();
            assignment.setEmail(email);
            assignment.setRole(role);

            UserOrganization userOrganization = createLicenseUser(assignment, license.getOrganization(), license);
            license.setStatus(Active);
            save(license);

            assignment.setLicenseId(license.getId());
            userLicenseAssignmentRepository.save(assignment);

            notifyLicenseAction(LicenseAssignmentNonExistentUser, license, userOrganization);
        }

        mailerService.sendMailQueue();
    }

    @Transactional
    @SuppressWarnings("Duplicates")
    public void updateLicense(UpdateLicenseRequest updateLicense) throws Exception {

        final License license = findLicense(updateLicense.getLicenseId());
        final User owner = license.getOrganization().getOwner();

        if (license.getStatus() == Inactive)
            throw new ValidationException(MessageCode.UNEXPECTED_ERROR);

        if (!license.getOrganization().getOwnerEmail().equals(authentication().getName()))
            throw new ValidationException(MessageCode.UNEXPECTED_ERROR);

        ProductOrder licenseOrder = productOrderService.licenseUpdateOrder(license, updateLicense);

        processOrderLicenses(licenseOrder);

        productOrderService.payOrder(licenseOrder, licenseOrder.getPaymentMethod(), owner);

    }

    public void processOrderLicenses(ProductOrder productOrder) {

        productOrder.getOrderDetails().forEach(orderDetail -> {

            try {

                final Organization organization = productOrder.getOrganization();
                final UserLicenseAssignment assignment = orderDetail.getAssignment();

                switch (productOrder.getType()) {
                    case UpdateLicense: {

                        licenseUpdate(orderDetail);

                        break;
                    }
                    case LicenseRenovation: {

                        if (!orderDetail.isLicenseEstablished())
                            throw new ValidationException(MessageCode.UNEXPECTED_ERROR);

                        licenseRenovation(orderDetail);

                        break;
                    }
                    case CreateOrganization: {

                        final License license = setupLicense(orderDetail, organization);

                        UserOrganization userOrganization;
                        User user = productOrder.getUser();

                        // The owner
                        if (user.getEmail().equalsIgnoreCase(assignment.getEmail())) {

                            userOrganization = productOrder.getUser().getUserOrganization();

                            userOrganization.setLicense(license);
                            userOrganizationService.updateUserOrganization(userOrganization);

                            license.setUserOrganization(userOrganization);

                        } else {
                            // Non owner user
                            userOrganization = createLicenseUser(assignment, organization, license);
                        }

                        save(license);

                        orderDetailService.setOrderDetailLicense(license.getId(), orderDetail.getId());
                        orderDetail.setLicense(license);

                        notifyLicenseAction(LicenseAssignmentNonExistentUser, license, userOrganization);

                        break;
                    }
                    case AddingLicenses: {

                        for (int i = 0; i < orderDetail.getQuantity(); i++) {

                            final License license = setupLicense(orderDetail, organization);
                            save(license);

                            if (assignment != null) {
                                UserOrganization userOrganization = createLicenseUser(assignment, organization, license);
                                orderDetailService.setOrderDetailLicense(license.getId(), orderDetail.getId());
                                notifyLicenseAction(LicenseAssignmentNonExistentUser, license, userOrganization);
                            }

                        }

                    }
                }

            } catch (Exception ex) {
                log.error("Error processing detail: ", ex);
            }

        });

    }

    private License setupLicense(OrderDetail orderDetail, Organization organization) {

        License license = new License();

        license.setDuration(orderDetail.getDuration());
        license.setExpiration(calculateLicenseExpiration(orderDetail));
        license.setLicenseProduct(orderDetail.getLicenseProduct());
        license.setStatus(LicenseStatusType.Active);
        license.setOrganization(organization);

        return license;
    }

    private void licenseUpdate(OrderDetail orderDetail) throws Exception {

        License license = orderDetail.getLicense();
        license.setLicenseProduct(orderDetail.getLicenseProduct());

        /*
        LicenseDurationType duration = license.getDuration();
        LicenseProduct licenseProduct = license.getLicenseProduct();
        */

        save(license);
    }

    @Transactional
    protected void licenseRenovation(OrderDetail orderDetail) {

        log.info("Renovating order detail: {}", orderDetail.getId());

        final ProductOrder productOrder = orderDetail.getProductOrder();

        LicenseProduct licenseProduct = orderDetail.getLicenseProduct();
        Integer quantity = orderDetail.getQuantity();

        Page<License> licensesPage = repository.findAll((root, query, cb) ->
                cb.and(
                        cb.and(
                                cb.and(
                                        cb.equal(root.get("organizationId"), productOrder.getOrganizationId()),
                                        cb.equal(root.get("status"), Active)
                                ), cb.equal(root.get("licenseProductId"), licenseProduct.getId())
                        ),
                        cb.equal(root.get("deleted"), false)
                ), PageRequest.of(0, quantity));

        LocalDateTime now = LocalDateTime.now();

        licensesPage.forEach(license -> {
            try {

                LocalDateTime expiration = license.getExpiration();
                long monthsDifference = now.until(expiration.toLocalDate(), ChronoUnit.MONTHS);

                ChronoUnit chronoUnit = license.getDuration().getUnit();

                if (monthsDifference > 1) {
                    expiration = now.plus(1, chronoUnit);
                } else {
                    expiration.plus(1, chronoUnit);
                }

                repository.updateExpiration(license.getExpiration(), license.getId());

                notifyLicenseRenovation(license);

            } catch (Exception e) {
                e.printStackTrace();
            }
        });

    }

    private void notifyLicenseRenovation(License license) {

        final UserOrganization userOrganization = license.getUserOrganization();
        final User user = userOrganization.getUser();
        final DeviceToken deviceToken = user.getDeviceToken();

        final String title = "Su licencia ha sido renovada";
        final String body = "Siga disfrutando de Q-Bits Chat";

        Map<String, Object> extraData = new HashMap<>();
        extraData.put("licenseActionFlow", LicenseRenovation.getIdentifier());

        String jabberId = user.getJid();

        switch (deviceToken.getType()) {
            case iOS: {

                notificationService.sendApnsNotification(jabberId,
                        buildApnsNotification(title, body, extraData));

                break;
            }
            case Android: {

                notificationService.sendGcmNotification(jabberId,
                        buildGcmNotification(title, body, extraData));

                break;
            }
        }

    }

    private APNSNotificationRequest buildApnsNotification(String title, String body, Map<String, Object> extraData) {

        APNSNotificationRequest apnsNotification = new APNSNotificationRequest();

        Alert alert = new Alert();
        alert.setTitle(title);
        alert.setBody(body);

        Aps aps = new Aps();
        aps.setAlert(alert);
        aps.setSound("default");

        apnsNotification.setAps(aps);
        apnsNotification.setExtraData(extraData);

        return apnsNotification;
    }

    private GCMNotificationRequest buildGcmNotification(String title, String body, Map<String, Object> extraData) {

        GCMNotificationRequest gcmNotification = new GCMNotificationRequest();

        Notification notification = new Notification();
        notification.setTitle(title);
        notification.setBody(body);
        notification.setSound("default");

        gcmNotification.setNotification(notification);
        gcmNotification.setData(extraData);
        gcmNotification.setPriority("high");

        return gcmNotification;
    }

    private void notifyLicenseAction(LicenseActionFlow flow, License license, UserOrganization userOrganization) throws Exception {

        if (userOrganization == null)
            throw new ValidationException(MessageCode.UNEXPECTED_ERROR);

        userOrganization.setLicense(license);

        Optional<UserOrganizationRole> ownerRole = userOrganization.getUserOrganizationRoles().stream()
                .filter(uor -> uor.getRole().getCode().equals(Owner.getCode()))
                .findFirst();

        if (!ownerRole.isPresent())
            userLicenseActionMail(flow, userOrganization);
    }

    private void userLicenseActionMail(LicenseActionFlow flow, UserOrganization userOrganization) {

        final User user = userOrganization.getUser();
        final UserLicenseMail.UserLicenseMailBuilder mailBuilder = userLicenseMailBuilder(userOrganization);

        switch (flow) {

            case LicenseAssignment:

                mailerService.enqueueMail(QBITS_CHAT, USER_LICENSE_ASSIGNMENT,
                        new MailRecipients(user.getEmail()), mailBuilder.build());

                break;

            case LicenseAssignmentNonExistentUser:

                String confirmationUrl = String.format("%s?confirmationToken=%s",
                        env.getProperty("app.chat.user-flow.confirmation-url"), user.getConfirmationToken());

                mailBuilder.confirmationUrl(confirmationUrl);

                mailerService.enqueueMail(QBITS_CHAT, USER_LICENSE_ASSIGNMENT_ACTIVATION,
                        new MailRecipients(user.getEmail()), mailBuilder.build());

                break;

            case LicenseUnassignment:

                mailerService.enqueueMail(QBITS_CHAT, USER_LICENSE_UNASSIGNMENT,
                        new MailRecipients(user.getEmail()), mailBuilder.build());

                break;

        }

    }

    private UserLicenseMail.UserLicenseMailBuilder userLicenseMailBuilder(UserOrganization userOrganization) {

        License userLicense = userOrganization.getLicense();
        User user = userOrganization.getUser();

        UserLicenseMail.LicenseProduct licenseProduct = mapperUtil()
                .map(userLicense.getLicenseProduct(), UserLicenseMail.LicenseProduct.class);

        UserLicenseMail.Organization organization = mapperUtil()
                .map(userOrganization.getOrganization(), UserLicenseMail.Organization.class);

        UserLicenseMail.License license = new UserLicenseMail.License(userLicense.getExpiration());

        UserLicenseMail.User userInfo = mapperUtil()
                .map(user, UserLicenseMail.User.class);

        return UserLicenseMail.builder()
                .user(userInfo)
                .organization(organization)
                .licenseProduct(licenseProduct)
                .license(license);
    }

    @Transactional
    @SuppressWarnings("Duplicates")
    public void unassignLicense(UnassignLicenseRequest unassignLicense) throws Exception {

        final License license = findLicense(unassignLicense.getLicenseId());

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (!license.getOrganization().getOwnerEmail().equals(authentication.getName()))
            throw new ValidationException(MessageCode.UNEXPECTED_ERROR);

        this.unassignLicense(license);
    }

    @Transactional
    @SuppressWarnings("Duplicates")
    public void unassignLicense(License license) throws Exception {

        final UserOrganization userOrganization = license.getUserOrganization();

        if (userOrganization != null) {

            license.setUserOrganization(null);

            notifyLicenseAction(LicenseUnassignment, license, userOrganization);
            userOrganizationService.removeUserOrganizationLicense(userOrganization);

            save(license);

            mailerService.sendMailQueue();
        }

    }

    @Transactional
    public void cancelLicenses(CancelLicensesRequest cancelLicensesRequest) throws Exception {

        final Integer quantity = cancelLicensesRequest.getQuantity();
        final License license = findLicense(cancelLicensesRequest.getLicenseId());
        final Organization organization = organizationService.findOrganization(license.getOrganizationId());

        if (!organization.getOwnerEmail().equalsIgnoreCase(authentication().getName()))
            throw new ValidationException(MessageCode.UNEXPECTED_ERROR);

        Page<License> licensesPage = repository.findAll((root, query, cb) ->
                cb.and(
                        cb.and(
                                cb.and(
                                        cb.equal(root.get("organizationId"), organization.getId()),
                                        cb.equal(root.get("status"), Active)
                                ), cb.equal(root.get("licenseProductId"), license.getLicenseProductId())
                        ),
                        cb.and(
                                cb.equal(root.get("deleted"), false),
                                cb.isNull(root.get("userOrganizationId"))
                        )
                ), PageRequest.of(0, quantity));

        licensesPage.forEach(currentLicense -> {
            try {
                unassignLicense(currentLicense);
                super.delete(currentLicense.getId(), false);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    public void renovateLicense(RenovateLicenseRequest renovateLicense) throws Exception {

        License license = findLicense(renovateLicense.getLicenseId());

        LocalDateTime now = LocalDateTime.now();
        LocalDateTime expiration = license.getExpiration();

        if (!now.isAfter(expiration)) // Not expired
            throw new ValidationException(MessageCode.UNEXPECTED_ERROR);

        productOrderService.licenseRenovationOrder(license, renovateLicense);

        ChronoUnit chronoUnit = ChronoUnit.MONTHS;

        if (license.getDuration() == LicenseDurationType.Yearly)
            chronoUnit = ChronoUnit.YEARS;

        long unitsOfTimeBetween = chronoUnit.between(expiration, now);

        license.setExpiration(now.plus(unitsOfTimeBetween, chronoUnit));

        save(license);
    }

    private UserOrganization createLicenseUser(UserLicenseAssignment assignment, Organization organization, License license) throws Exception {

        User createdUser = userService.createDefaultUser(new UserRequest(assignment.getEmail()));

        UserOrganization userOrganization = organizationService
                .addUserOrganization(createdUser, organization, assignment.getRole());

        license.setUserOrganization(userOrganization);

        return userOrganization;
    }

    public Page<LicenseInfo> findLicensesInfo(Long organizationId, Pageable pageable) throws ValidationException {

        if (!organizationService.findOrganization(organizationId).getOwnerEmail()
                .equalsIgnoreCase(authentication().getName()))
            throw new ValidationException(MessageCode.UNEXPECTED_ERROR);

        return repository.findByOrganizationIdAndDeletedFalse(organizationId, pageable)
                .map(license -> mapperUtil().map(license, LicenseInfo.class));
    }

    public List<LicenseInfo> findGroupedLicensesInfo(Long organizationId) throws ValidationException {

        if (!organizationService.findOrganization(organizationId).getOwnerEmail()
                .equalsIgnoreCase(authentication().getName()))
            throw new ValidationException(MessageCode.UNEXPECTED_ERROR);

        return mapperUtil().map(repository.findExpiredGroupedByProduct(organizationId), LicenseInfo.class);
    }

    public LicenseInfo findLicenseInfo(Long licenseId) throws ValidationException {

        License license = findLicense(licenseId);

        if (!license.getOrganization().getOwnerEmail()
                .equalsIgnoreCase(authentication().getName()))
            throw new ValidationException(MessageCode.UNEXPECTED_ERROR);

        return mapperUtil().map(license, LicenseInfo.class);
    }

    private License findLicense(Long licenseId) throws ValidationException {

        Optional<License> license = repository.findById(licenseId);

        return license.orElseThrow(() -> new ValidationException(MessageCode.UNEXPECTED_ERROR));
    }

    private LocalDateTime calculateLicenseExpiration(OrderDetail orderDetail) {

        LicenseDurationType duration = orderDetail.getDuration();

        return LocalDateTime.now()
                .plus(1, duration == LicenseDurationType.Monthly ?
                        ChronoUnit.MONTHS : ChronoUnit.YEARS);
    }

    private Authentication authentication() {
        return SecurityContextHolder.getContext().getAuthentication();
    }

}
