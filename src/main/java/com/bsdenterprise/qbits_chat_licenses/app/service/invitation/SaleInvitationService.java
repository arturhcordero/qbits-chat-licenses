package com.bsdenterprise.qbits_chat_licenses.app.service.invitation;

import static com.bsdenterprise.qbits_chat_licenses.common.config.message.MessageCode.UNEXPECTED_ERROR;
import static com.bsdenterprise.qbits_chat_licenses.app.type.mailer.config.MailTemplate.SALE_INVITATION;
import static com.bsdenterprise.qbits_chat_licenses.app.type.mailer.config.GroupType.QBITS_CHAT;
import static com.bsdenterprise.qbits_chat_licenses.app.type.license.LicenseStatusType.*;

import static com.bsdenterprise.qbits_chat_licenses.app.type.invitation.SaleInvitationStatus.*;

import com.bsdenterprise.qbits_chat_licenses.app.dto.invitation.SaleInvitationInfo;
import com.bsdenterprise.qbits_chat_licenses.app.dto.mailer.mail.invitation.SaleInvitationMail;
import com.bsdenterprise.qbits_chat_licenses.app.dto.mailer.integration.MailRecipients;
import com.bsdenterprise.qbits_chat_licenses.app.dto.invitation.SaleInvitationRequest;

import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.invitation.SaleInvitation;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.license.License;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.license.product.LicenseProduct;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.organization.auth.User;

import com.bsdenterprise.qbits_chat_licenses.app.service.license.LicenseProductService;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.repository.invitation.SaleInvitationRepository;

import com.bsdenterprise.qbits_chat_licenses.app.service.mailer.MailerService;
import com.bsdenterprise.qbits_chat_licenses.app.service.organization.UserService;
import com.bsdenterprise.qbits_chat_licenses.app.type.license.LicenseStatusType;
import com.bsdenterprise.qbits_chat_licenses.common.config.exception.model.ValidationException;
import com.bsdenterprise.qbits_chat_licenses.common.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.Authentication;

import static org.springframework.util.StringUtils.isEmpty;

import lombok.extern.slf4j.Slf4j;
import lombok.*;

import javax.persistence.criteria.Predicate;
import java.util.Optional;

@Slf4j
@Service
public class SaleInvitationService extends BaseService<SaleInvitationRepository, SaleInvitation> {

    @Setter(onMethod = @__(@Autowired))
    private LicenseProductService licenseProductService;

    @Setter(onMethod = @__(@Autowired))
    private MailerService mailerService;

    @Setter(onMethod = @__(@Autowired))
    private UserService userService;

    @Setter(onMethod = @__(@Autowired))
    private Environment env;

    public Optional<SaleInvitation> findInvitation(String saleInvitationUuid) {
        return repository.findByUuid(saleInvitationUuid);
    }

    public void saveInvitation(SaleInvitation saleInvitation) throws Exception {
        save(saleInvitation);
    }

    @Transactional(readOnly = true)
    public Page<SaleInvitationInfo> findInvitations(final String search, final Pageable pageable) throws ValidationException {

        User authenticatedUser = userService.findUserByEmail(authentication().getName())
                .orElseThrow(() -> new ValidationException(UNEXPECTED_ERROR));

        return repository.findAll((root, query, cb) -> {

            Predicate predicate = cb.and(
                    cb.equal(root.get("userId"), authenticatedUser.getId()),
                    cb.equal(root.get("deleted"), false)
            );

            if (!isEmpty(search))
                predicate = cb.and(predicate, cb.like(
                        root.get("email"), "%" + search + "%")
                );

            return predicate;

        }, pageable).map(si -> mapperUtil().map(si, SaleInvitationInfo.class));
    }

    @Transactional
    public void sendInvitation(SaleInvitationRequest saleInvitationRequest) throws Exception {

        final String email = saleInvitationRequest.getEmail();
        final Long licenseProductId = saleInvitationRequest.getLicenseProductId();

        validateUserExistance(email);

        User authenticatedUser = userService.findUserByEmail(authentication().getName())
                .orElseThrow(() -> new ValidationException(UNEXPECTED_ERROR));

        LicenseProduct licenseProduct =
                licenseProductService.findLicenseProduct(licenseProductId);

        Optional<SaleInvitation> optionalSaleInvitation =
                repository.findInvitation(email, authenticatedUser.getId());

        if (optionalSaleInvitation.isPresent()) {

            SaleInvitation saleInvitation = optionalSaleInvitation.get();
            repository.forwardSaleInvitation(saleInvitation.getId());

            sendMailInvitation(saleInvitation);

        } else {

            SaleInvitation saleInvitation = new SaleInvitation();

            saleInvitation.setEmail(email);
            saleInvitation.setLicenseProduct(licenseProduct);
            saleInvitation.setUser(authenticatedUser);
            saleInvitation.setStatus(Sent);

            save(saleInvitation);

            sendMailInvitation(saleInvitation);
        }

        mailerService.sendMailQueue();
    }

    private void validateUserExistance(String email) throws ValidationException {

        Optional<User> user = userService.findUserByEmail(email);

        if (user.isPresent()) {
            License license = user.get().getUserOrganization().getLicense();
            if (license.getStatus() == Active)
                throw new ValidationException(UNEXPECTED_ERROR);
        }

    }

    @Transactional
    public void forwardInvitation(Long saleInvitationId) throws Exception {

        SaleInvitation saleInvitation = findById(saleInvitationId);
        sendMailInvitation(saleInvitation);

        repository.forwardSaleInvitation(saleInvitation.getId());

        mailerService.sendMailQueue();
    }

    @Transactional
    public void cancelInvitation(Long saleInvitationId) throws Exception {

        if (existsById(saleInvitationId)) {
            repository.cancelSaleInvitation(saleInvitationId);
            delete(saleInvitationId, false);
        }
        else throw new ValidationException(UNEXPECTED_ERROR);

    }

    @Transactional
    public SaleInvitationInfo openInvitation(String saleInvitationUuid) throws ValidationException {

        SaleInvitation saleInvitation = findInvitation(saleInvitationUuid)
                .orElseThrow(() -> new ValidationException(UNEXPECTED_ERROR));

        if (saleInvitation.getStatus() == Used)
            throw new ValidationException(UNEXPECTED_ERROR);

        SaleInvitationInfo saleInvitationInfo = mapperUtil()
                .map(saleInvitation, SaleInvitationInfo.class);

        repository.openSaleInvitation(saleInvitation.getId());

        return saleInvitationInfo;
    }

    private void sendMailInvitation(SaleInvitation saleInvitation) {

        SaleInvitationMail.User user = mapperUtil()
                .map(saleInvitation.getUser(), SaleInvitationMail.User.class);

        SaleInvitationMail.LicenseProduct licenseProduct = mapperUtil()
                .map(saleInvitation.getLicenseProduct(), SaleInvitationMail.LicenseProduct.class);

        String saleInvitationUrl = String.format("%s?invitationCode=%s",
                env.getProperty("app.chat.invitation-flow.sale-invitation-url"), saleInvitation.getUuid());

        SaleInvitationMail saleInvitationMail =
                SaleInvitationMail.builder()
                        .user(user)
                        .licenseProduct(licenseProduct)
                        .saleInvitationUrl(saleInvitationUrl)
                        .build();

        mailerService.enqueueMail(QBITS_CHAT, SALE_INVITATION,
                new MailRecipients(saleInvitation.getEmail()), saleInvitationMail);
    }

    private Authentication authentication() {
        return SecurityContextHolder.getContext().getAuthentication();
    }

}
