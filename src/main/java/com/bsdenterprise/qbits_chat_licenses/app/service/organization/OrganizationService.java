package com.bsdenterprise.qbits_chat_licenses.app.service.organization;

import static com.bsdenterprise.qbits_chat_licenses.app.type.organization.OrganizationType.*;
import static com.bsdenterprise.qbits_chat_licenses.app.util.TypeValidatorUtil.validateCommunityType;
import static com.bsdenterprise.qbits_chat_licenses.app.util.TypeValidatorUtil.validateOrganizationType;

import com.bsdenterprise.qbits_chat_licenses.app.dto.organization.modify.ModifyOrganizationRequest;
import com.bsdenterprise.qbits_chat_licenses.app.dto.organization.read.OrganizationInfo;

import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.organization.UserOrganization;
import com.bsdenterprise.qbits_chat_licenses.app.type.organization.auth.RoleType;
import static com.bsdenterprise.qbits_chat_licenses.app.type.organization.auth.RoleType.*;
import static com.bsdenterprise.qbits_chat_licenses.common.config.message.MessageCode.UNEXPECTED_ERROR;

import com.bsdenterprise.qbits_chat_licenses.app.type.organization.auth.UserActivationType;

import com.bsdenterprise.qbits_chat_licenses.common.config.exception.model.ValidationException;
import com.bsdenterprise.qbits_chat_licenses.common.service.BaseService;

import com.bsdenterprise.qbits_chat_licenses.app.persistence.repository.organization.OrganizationRepository;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.organization.Organization;

import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.organization.auth.User;

import com.bsdenterprise.qbits_chat_licenses.app.dto.user.create.UserRequest;
import com.bsdenterprise.qbits_chat_licenses.app.dto.organization.create.OrganizationRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CachePut;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;

import lombok.extern.slf4j.Slf4j;
import lombok.Setter;
import org.springframework.util.CollectionUtils;

@Slf4j
@Service
@CacheConfig(cacheNames = "organizations")
public class OrganizationService extends BaseService<OrganizationRepository, Organization> {

    @Setter(onMethod = @__(@Autowired))
    private UserService userService;

    @Setter(onMethod = @__(@Autowired))
    private UserOrganizationService userOrganizationService;

    //@Cacheable(key = "#organizationId")
    public OrganizationInfo findOrganizationInfo(Long organizationId) throws Exception {

        User user = userService.findUserByEmail(authentication().getName())
                .orElseThrow(() -> new ValidationException(UNEXPECTED_ERROR));

        Organization organization = user.getUserOrganization().getOrganization();

        if (!organization.getId().equals(organizationId))
            throw new ValidationException(UNEXPECTED_ERROR);

        return mapperUtil().map(organization, OrganizationInfo.class);
    }

    @CachePut(key = "#result.id")
    public OrganizationInfo modifyOrganizationInfo(String username, ModifyOrganizationRequest modifyOrganization) throws Exception {

        User user = userService.findUserByEmail(username)
                .orElseThrow(() -> new ValidationException(UNEXPECTED_ERROR));

        Organization organization = user.getUserOrganization().getOrganization();

        if (!organization.getOwnerEmail().equals(user.getEmail()))
            throw new ValidationException(UNEXPECTED_ERROR);

        modifyOrganization.setOrganizationType(validateOrganizationType(modifyOrganization.getOrganizationTypeId(), false));
        modifyOrganization.setCommunityType(validateCommunityType(modifyOrganization.getCommunityTypeId(), false));

        objectUtil.merge(modifyOrganization, organization);

        return mapperUtil().map(save(organization), OrganizationInfo.class);
    }

    public Organization findOrganization(Long organizationId) {
        return findById(organizationId);
    }

    public Boolean existsOrganization(Long organizationId) {
        return repository.existsById(organizationId);
    }

    public Organization createOrganization(OrganizationRequest organizationInput) throws Exception {

        organizationInput.setOrganizationType(validateOrganizationType(organizationInput.getOrganizationTypeId()));
        organizationInput.setCommunityType(validateCommunityType(organizationInput.getCommunityTypeId()));

        if (organizationInput.getOrganizationType() == Moral) {
            if (CollectionUtils.isEmpty(organizationInput.getAddresses()))
                throw new ValidationException(UNEXPECTED_ERROR);
        }

        Organization organization = save(organizationInput, Organization.class);

        final @NotNull UserRequest userInput = organizationInput.getUser();
        userInput.setActivationType(UserActivationType.Automatic);

        User user = userService.createUser(userInput);
        UserOrganization userOrganization = addUserOrganization(user, organization, Owner);

        organization.getUserOrganizations().add(userOrganization);
        user.getUserOrganizations().add(userOrganization);

        return organization;
    }

    public UserOrganization addUserOrganization(User user, Organization organization, RoleType role) throws Exception {
        return userOrganizationService.relateUserOrganization(user, organization, role);
    }

    public Authentication authentication() {
        return SecurityContextHolder.getContext().getAuthentication();
    }

}
