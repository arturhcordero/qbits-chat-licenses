package com.bsdenterprise.qbits_chat_licenses.app.service.catalog;

import com.bsdenterprise.qbits_chat_licenses.app.dto.catalog.role.RoleInfo;
import com.bsdenterprise.qbits_chat_licenses.common.service.BaseService;

import com.bsdenterprise.qbits_chat_licenses.app.persistence.repository.catalog.RoleRepository;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.organization.auth.Role;

import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

import java.util.List;

@Slf4j
@Service
@CacheConfig(cacheNames = "roles")
public class RoleService extends BaseService<RoleRepository, Role> {

    @Cacheable
    public List<RoleInfo> findRoles() {
        return findAll(RoleInfo.class);
    }

    public Role findByCode(String code){
        return repository.findByCode(code);
    }

}
