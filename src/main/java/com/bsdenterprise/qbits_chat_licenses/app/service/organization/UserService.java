package com.bsdenterprise.qbits_chat_licenses.app.service.organization;

import static com.bsdenterprise.qbits_chat_licenses.app.type.mailer.config.GroupType.QBITS_CHAT;
import static com.bsdenterprise.qbits_chat_licenses.app.type.mailer.config.MailTemplate.CHANGE_PASSWORD_CONFIRMATION;
import static com.bsdenterprise.qbits_chat_licenses.app.type.mailer.config.MailTemplate.FORGOT_PASSWORD;
import static com.bsdenterprise.qbits_chat_licenses.common.util.Constants.BIRTHDAY_FORMATTER;

import static com.bsdenterprise.qbits_chat_licenses.app.util.TypeValidatorUtil.validateGender;
import static com.bsdenterprise.qbits_chat_licenses.app.util.TypeValidatorUtil.validateRole;
import static com.bsdenterprise.qbits_chat_licenses.common.config.message.MessageCode.*;

import com.bsdenterprise.qbits_chat_licenses.app.dto.account.ChangePasswordRequest;
import com.bsdenterprise.qbits_chat_licenses.app.dto.account.ForgotPasswordRequest;
import com.bsdenterprise.qbits_chat_licenses.app.dto.catalog.role.RoleInfo;
import com.bsdenterprise.qbits_chat_licenses.app.dto.license.info.LicenseInfo;
import com.bsdenterprise.qbits_chat_licenses.app.dto.mailer.integration.MailRecipients;
import com.bsdenterprise.qbits_chat_licenses.app.dto.mailer.mail.user.ForgotPasswordMail;
import com.bsdenterprise.qbits_chat_licenses.app.dto.mailer.mail.user.PasswordChangeConfirmationMail;
import com.bsdenterprise.qbits_chat_licenses.app.dto.user.confirm.EmailConfirmationTokenResult;
import com.bsdenterprise.qbits_chat_licenses.app.dto.user.modify.AddRoleRequest;
import com.bsdenterprise.qbits_chat_licenses.app.dto.account.RestorePasswordRequest;
import com.bsdenterprise.qbits_chat_licenses.app.dto.user.read.UserInfo;
import com.bsdenterprise.qbits_chat_licenses.app.dto.user.read.UserLicenseInfo;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.license.License;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.organization.UserOrganization;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.organization.auth.UserOrganizationRole;
import com.bsdenterprise.qbits_chat_licenses.app.service.mailer.MailerService;
import com.bsdenterprise.qbits_chat_licenses.app.service.openfire.OpenfireService;
import com.bsdenterprise.qbits_chat_licenses.app.type.organization.auth.RoleType;

import com.bsdenterprise.qbits_chat_licenses.app.type.organization.auth.UserActivationType;
import com.bsdenterprise.qbits_chat_licenses.common.config.exception.model.IntegrationException;
import com.bsdenterprise.qbits_chat_licenses.common.service.BaseService;
import com.bsdenterprise.qbits_chat_licenses.common.config.exception.model.ValidationException;

import com.bsdenterprise.qbits_chat_licenses.app.dto.user.create.UserRequest;

import com.bsdenterprise.qbits_chat_licenses.app.persistence.repository.user.UserRepository;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.organization.auth.User;

import lombok.SneakyThrows;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import static java.util.UUID.*;

import lombok.extern.slf4j.Slf4j;
import lombok.Setter;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

@Slf4j
@Service
@CacheConfig(cacheNames = "users")
public class UserService extends BaseService<UserRepository, User> {

    @Setter(onMethod = @__(@Autowired))
    private MailerService mailerService;

    @Setter(onMethod = @__(@Autowired))
    private OpenfireService openfireService;

    @Setter(onMethod = @__(@Autowired))
    private UserOrganizationService userOrganizationService;

    @Setter(onMethod = @__(@Autowired))
    private OrganizationService organizationService;

    @Setter(onMethod = @__(@Autowired))
    private PasswordEncoder passwordEncoder;

    @Setter(onMethod = @__(@Autowired))
    private Environment env;

    public EmailConfirmationTokenResult findEmailByToken(String token) throws ValidationException {
        return new EmailConfirmationTokenResult(
                repository.findEmailByToken(token)
                        .orElseThrow(() -> new ValidationException(UNEXPECTED_ERROR))
        );
    }

    public void validateRestorePasswordToken(String token) throws ValidationException {
        if (!repository.existsByForgotPasswordToken(token))
            throw new ValidationException(UNEXPECTED_ERROR);
    }

    public Page<UserLicenseInfo> findUsers(String search, Long organizationId, Pageable pageable) throws ValidationException {

        final String authenticatedUser = authentication().getName();

        if (!organizationService.findOrganization(organizationId).getOwnerEmail()
                .equalsIgnoreCase(authenticatedUser))
            throw new ValidationException(UNEXPECTED_ERROR);

        Page<UserOrganization> userOrganizationPage = userOrganizationService
                .findUsers(search, organizationId, authenticatedUser, pageable);

        return userOrganizationPage.map(userOrganization -> {

            UserLicenseInfo userLicense = new UserLicenseInfo();

            License license = userOrganization.getLicense();

            UserInfo userInfo = mapperUtil().map(userOrganization.getUser(), UserInfo.class);
            userInfo.setMainRole(mapperUtil().map(userOrganization.getMainRole(), RoleInfo.class));
            userInfo.setMemberSince(userOrganization.getCreatedAt());

            userLicense.setUser(userInfo);

            if (license != null)
                userLicense.setLicense(mapperUtil().map(license, LicenseInfo.class));

            return userLicense;

        });
    }

    public Optional<User> findUserByJabberId(String jabberId) {
        return repository.findByJid(jabberId);
    }

    public Optional<User> findUserByEmail(String email) {
        return repository.findByEmail(email);
    }

    @Transactional
    public void changePassword(ChangePasswordRequest changePassword) throws Exception {

        String newPassword = changePassword.getNewPassword();

        final String originalPassword = newPassword;
        final String email = changePassword.getEmail();
        final String repeatNewPassword = changePassword.getRepeatNewPassword();

        if (!newPassword.equals(repeatNewPassword))
            throw new ValidationException(UNEXPECTED_ERROR);

        User user = findUserByEmail(email)
                .orElseThrow(() -> new ValidationException(UNEXPECTED_ERROR));

        if (!authentication().getName().equals(user.getEmail()))
            throw new ValidationException(UNEXPECTED_ERROR);

        if (!passwordEncoder.matches(changePassword.getCurrentPassword(), user.getPassword()))
            throw new ValidationException(UNEXPECTED_ERROR);

        newPassword = passwordEncoder.encode(newPassword);

        PasswordChangeConfirmationMail passwordConfirmationMail =
                PasswordChangeConfirmationMail.builder()
                        .fullName(user.getFullName())
                        .build();

        mailerService.enqueueMail(QBITS_CHAT, CHANGE_PASSWORD_CONFIRMATION,
                new MailRecipients(email), passwordConfirmationMail);

        repository.changePassword(newPassword, user.getId());

        openfireService.changePassword(user.getUsername(), originalPassword);

        mailerService.sendMailQueue();
    }

    @SneakyThrows
    @Transactional
    public void forgotPassword(ForgotPasswordRequest forgotPassword) {

        final String email = forgotPassword.getEmail();

        if (!userExists(email))
            throw new ValidationException(UNEXPECTED_ERROR);

        String passwordConfirmationToken = randomUUID().toString();
        repository.assignPasswordConfirmationToken(passwordConfirmationToken, email);

        String confirmationUrl = String.format("%s?forgotPasswordToken=%s",
                env.getProperty("app.chat.user-flow.restore-password-url"), passwordConfirmationToken);

        ForgotPasswordMail forgotPasswordMail = ForgotPasswordMail.builder()
                .restorePasswordUrl(confirmationUrl).build();

        mailerService.enqueueMail(QBITS_CHAT, FORGOT_PASSWORD,new MailRecipients(email), forgotPasswordMail);

        mailerService.sendMailQueue();
    }

    @SneakyThrows
    @Transactional
    public void restorePassword(final String token, final RestorePasswordRequest forgotPassword) {

        validateRestorePasswordToken(token);

        User user = repository.findSimpleUser(token);
        final String password = forgotPassword.getPassword();

        repository.changePassword(passwordEncoder.encode(password), token);

        openfireService.changePassword(user.getUsername(), password);

        repository.assignPasswordConfirmationToken(null, user.getEmail());

        PasswordChangeConfirmationMail passwordConfirmationMail =
                PasswordChangeConfirmationMail.builder()
                        .fullName(user.getFullName())
                        .build();

        mailerService.enqueueMail(QBITS_CHAT, CHANGE_PASSWORD_CONFIRMATION,
                new MailRecipients(user.getEmail()), passwordConfirmationMail);

        mailerService.sendMailQueue();
    }

    public User createUser(UserRequest userRequest) throws Exception {

        final String email = userRequest.getEmail();
        final String originalPassword = userRequest.getPassword();

        // Verify that user does not exists
        if (userExists(email)) throwUserAlreadyExists(email);

        addUserAttributes(userRequest);
        userRequest.setConfirmationToken(randomUUID().toString());

        // Create the user
        User createdUser = save(userRequest, User.class);

        createdUser.setOriginalPassword(originalPassword);
        openfireService.setUserGeneration(createdUser);

        return createdUser;
    }

    public User createDefaultUser(UserRequest userRequest) throws Exception {

        final String email = userRequest.getEmail();

        // Verify that user does not exists
        if (userExists(email))
            throwUserAlreadyExists(email);

        userRequest.setConfirmationToken(randomUUID().toString());
        userRequest.setActivationType(UserActivationType.Default);

        // Create the user
        return super.save(userRequest, User.class);
    }

    public void resolveUserGeneration(User user) throws IntegrationException {

        if (user == null) {
            openfireService.resolveUserGeneration(true);
            return;
        }

        repository.assignJabberId(openfireService.resolveUserGeneration(false), user.getId());
    }

    public UserInfo userInfo(String username) throws ValidationException {

        User user = findUserByEmail(username)
                .orElseThrow(() -> new ValidationException(UNEXPECTED_ERROR));

        return mapperUtil().map(user, UserInfo.class);
    }

    @Transactional
    public void confirmUser(final String confirmationToken, final UserRequest userRequest) throws Exception {

        User user = repository.findByConfirmationTokenAndEnabledFalse(confirmationToken)
                .orElseThrow(() -> new ValidationException(UNEXPECTED_ERROR));

        user.setOriginalPassword(userRequest.getPassword());
        openfireService.setUserGeneration(user);

        addUserAttributes(userRequest);

        objectUtil.merge(userRequest, user);

        user.setEnabled(true);
        save(user);

        resolveUserGeneration(user);
    }

    public boolean userExists(String email) {
        return repository.existsByEmail(email);
    }

    public void addRole(AddRoleRequest addRole) throws Exception {

        RoleType role = validateRole(addRole.getRoleId());

        User user = findUserByEmail(addRole.getEmail())
                .orElseThrow(() -> new ValidationException(USER_INVALID_EMAIL));

        UserOrganization userOrganization = user.getUserOrganization();

        if (userOrganization.getOrganization().getOwnerEmail()
                .equalsIgnoreCase(authentication().getName()))
            throw new ValidationException(USER_ACCESS_DENIED);

        List<UserOrganizationRole> userOrganizationRoles = userOrganization.getUserOrganizationRoles();

        Optional<UserOrganizationRole> roleFound = userOrganizationRoles.stream()
                .filter(or -> or.getRole().getId().equals(role.getIdentifier()))
                .findFirst();

        if (!roleFound.isPresent())
            userOrganizationService.addRole(userOrganization, role);

    }

    private void addUserAttributes(UserRequest userRequest) throws ValidationException {

        // Verify if the gender reference exists and set it to the request
        userRequest.setGenderReference(validateGender(userRequest.getGenderReferenceId()));

        // Parse birthday and set it to the create mail
        if (!StringUtils.isEmpty(userRequest.getBirthdaySt()))
            userRequest.setBirthday(parseBirthday(userRequest.getBirthdaySt()));

        // Encode the password with passwordEncoder
        userRequest.setPassword(passwordEncoder.encode(userRequest.getPassword()));

        // Set cellphone detail to userCreate mail
        if (!ObjectUtils.isEmpty(userRequest.getCellPhone()))
            objectUtil.merge(userRequest.getCellPhone(), userRequest);

    }

    private LocalDate parseBirthday(String birthday) throws ValidationException {

        LocalDate parsedBirthday;

        try {
            parsedBirthday = LocalDate.parse(birthday, BIRTHDAY_FORMATTER);
        } catch (Exception ex) {
            throw new ValidationException(USER_INVALID_BIRTHDAY, birthday);
        }

        return parsedBirthday;
    }

    private void throwUserAlreadyExists(String email) throws ValidationException {
        throw new ValidationException(USER_ALREADY_EXISTS, email);
    }

    private Authentication authentication() {
        return SecurityContextHolder.getContext().getAuthentication();
    }

}
