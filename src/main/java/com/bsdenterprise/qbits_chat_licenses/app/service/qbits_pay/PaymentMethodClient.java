package com.bsdenterprise.qbits_chat_licenses.app.service.qbits_pay;

import com.bsdenterprise.qbits_chat_licenses.app.dto.qbits_pay.integration.payment_method.update.QbitsPaymentMethodUpdate;
import com.bsdenterprise.qbits_chat_licenses.app.dto.qbits_pay.integration.payment_method.update.QbitsPaymentMethodUpdated;
import com.bsdenterprise.qbits_chat_licenses.app.service.qbits_pay.common.QBitspayRestClient;
import com.bsdenterprise.qbits_chat_licenses.common.config.exception.model.IntegrationException;

import com.bsdenterprise.qbits_chat_licenses.app.dto.qbits_pay.integration.payment_method.create.QbitsPaymentMethodCreated;
import com.bsdenterprise.qbits_chat_licenses.app.dto.qbits_pay.integration.payment_method.create.QbitsPaymentMethodCreate;

import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class PaymentMethodClient extends QBitspayRestClient {

    public QbitsPaymentMethodCreated createPaymentMethod(QbitsPaymentMethodCreate paymentMethod) throws Exception {

        try {

            final String path = "/v1/card";

            return super.post(path, paymentMethod, QbitsPaymentMethodCreated.class).getBody();

        } catch (IntegrationException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new IntegrationException(ex.getMessage());
        }

    }

    public QbitsPaymentMethodUpdated updatePaymentMethod(QbitsPaymentMethodUpdate paymentMethod) throws Exception {

        try {

            final String jidUuid = paymentMethod.getJidUuid();
            final String path = String.format("/v1/card/jid/%s/", jidUuid);

            return super.put(path, paymentMethod, QbitsPaymentMethodUpdated.class).getBody();

        } catch (IntegrationException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new IntegrationException(ex.getMessage());
        }

    }

}
