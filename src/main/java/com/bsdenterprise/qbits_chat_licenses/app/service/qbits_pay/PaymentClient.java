package com.bsdenterprise.qbits_chat_licenses.app.service.qbits_pay;

import com.bsdenterprise.qbits_chat_licenses.app.service.qbits_pay.common.QBitspayRestClient;
import com.bsdenterprise.qbits_chat_licenses.common.config.exception.model.IntegrationException;

import com.bsdenterprise.qbits_chat_licenses.app.dto.qbits_pay.integration.sale.create.QBitspaySaleRequest;
import com.bsdenterprise.qbits_chat_licenses.app.dto.qbits_pay.integration.sale.response.QBitspaySaleResponse;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class PaymentClient extends QBitspayRestClient {

    public QBitspaySaleResponse payOrganizationOrder(QBitspaySaleRequest qBitspaySale) throws Exception {

        try{

            final String path = "/v2/sale.json";

            ResponseEntity<QBitspaySaleResponse> response = super.post(path, qBitspaySale, QBitspaySaleResponse.class);

            return response.getBody();

        } catch (IntegrationException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new IntegrationException(ex.getMessage());
        }

    }

}
