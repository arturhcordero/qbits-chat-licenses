package com.bsdenterprise.qbits_chat_licenses.app.service.order;

import com.bsdenterprise.qbits_chat_licenses.app.config.exception.model.UnsuccessfulPaymentException;
import com.bsdenterprise.qbits_chat_licenses.app.dto.license.modify.UpdateLicenseRequest;
import com.bsdenterprise.qbits_chat_licenses.app.dto.license.status.RenovateLicenseRequest;
import com.bsdenterprise.qbits_chat_licenses.app.dto.order.info.ProductOrderInfo;
import com.bsdenterprise.qbits_chat_licenses.app.dto.organization_order.LicenseOrderRequest;
import com.bsdenterprise.qbits_chat_licenses.app.dto.organization_order.OrganizationOrderMigrationRequest;
import com.bsdenterprise.qbits_chat_licenses.app.dto.payment.migration.PaymentMethodMigrationRequest;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.invitation.SaleInvitation;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.organization.Organization;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.organization.auth.Role;
import com.bsdenterprise.qbits_chat_licenses.app.service.invitation.SaleInvitationService;
import com.bsdenterprise.qbits_chat_licenses.app.service.order.order_detail.OrderDetailService;
import com.bsdenterprise.qbits_chat_licenses.app.type.order.ProductOrderType;
import com.bsdenterprise.qbits_chat_licenses.common.config.exception.model.IntegrationException;
import com.bsdenterprise.qbits_chat_licenses.common.config.exception.model.ValidationException;
import com.bsdenterprise.qbits_chat_licenses.common.service.BaseService;

import com.bsdenterprise.qbits_chat_licenses.app.service.license.LicenseService;
import com.bsdenterprise.qbits_chat_licenses.app.service.organization.UserService;

import static com.bsdenterprise.qbits_chat_licenses.app.type.invitation.SaleInvitationStatus.Used;
import static com.bsdenterprise.qbits_chat_licenses.app.type.order.ProductOrderType.*;
import static com.bsdenterprise.qbits_chat_licenses.app.util.TypeValidatorUtil.validateLicenseDuration;
import static com.bsdenterprise.qbits_chat_licenses.app.util.TypeValidatorUtil.validateRole;

import com.bsdenterprise.qbits_chat_licenses.app.service.mailer.MailerService;
import com.bsdenterprise.qbits_chat_licenses.app.service.payment.PaymentService;
import com.bsdenterprise.qbits_chat_licenses.app.service.organization.OrganizationService;
import com.bsdenterprise.qbits_chat_licenses.app.service.license.LicenseProductService;
import com.bsdenterprise.qbits_chat_licenses.app.service.payment_method.PaymentMethodService;

import com.bsdenterprise.qbits_chat_licenses.app.persistence.repository.order.ProductOrderRepository;

import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.payment.PaymentMethod;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.order.payment.OrderPayment;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.order.ProductOrder;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.order.OrderDetail;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.license.UserLicenseAssignment;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.license.product.LicenseProduct;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.license.product.Cost;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.license.License;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.organization.UserOrganization;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.organization.auth.User;

import com.bsdenterprise.qbits_chat_licenses.app.dto.mailer.mail.order.OrderConfirmationMail;
import com.bsdenterprise.qbits_chat_licenses.app.dto.mailer.integration.MailRecipients;
import com.bsdenterprise.qbits_chat_licenses.app.dto.organization_order.OrganizationOrderRequest;
import com.bsdenterprise.qbits_chat_licenses.app.dto.organization.create.OrganizationRequest;
import com.bsdenterprise.qbits_chat_licenses.app.dto.order.ProductOrderRequest;
import com.bsdenterprise.qbits_chat_licenses.app.dto.order.license.OrderDetailRequest;
import com.bsdenterprise.qbits_chat_licenses.app.dto.order.license.UserLicenseAssignmentRequest;
import com.bsdenterprise.qbits_chat_licenses.app.dto.payment.create.PaymentMethodRequest;

import com.bsdenterprise.qbits_chat_licenses.app.type.mailer.config.GroupType;
import com.bsdenterprise.qbits_chat_licenses.app.type.mailer.config.MailTemplate;

import static com.bsdenterprise.qbits_chat_licenses.app.type.order.ProductOrderStatus.*;
import static com.bsdenterprise.qbits_chat_licenses.common.config.message.MessageCode.PAYMENT_UNSUCCESSFUL;
import static com.bsdenterprise.qbits_chat_licenses.common.config.message.MessageCode.UNEXPECTED_ERROR;
import static com.bsdenterprise.qbits_chat_licenses.common.config.message.MessageProperties.getMessage;
import static java.math.BigDecimal.ZERO;
import static java.math.RoundingMode.HALF_UP;
import static org.springframework.util.StringUtils.isEmpty;

import com.bsdenterprise.qbits_chat_licenses.app.type.organization.auth.RoleType;
import com.bsdenterprise.qbits_chat_licenses.app.type.license.LicenseDurationType;
import com.bsdenterprise.qbits_chat_licenses.app.type.payment.PaymentMethodFlow;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import javax.xml.ws.WebServiceException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.YearMonth;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

import lombok.extern.slf4j.Slf4j;
import lombok.Setter;

@Slf4j
@Service
public class ProductOrderService extends BaseService<ProductOrderRepository, ProductOrder> {

    @Setter(onMethod = @__(@Autowired))
    private OrganizationService organizationService;

    @Setter(onMethod = @__(@Autowired))
    private PaymentMethodService paymentMethodService;

    @Setter(onMethod = @__(@Autowired))
    private PaymentService paymentService;

    @Setter(onMethod = @__(@Autowired))
    private LicenseProductService licenseProductService;

    @Setter(onMethod = @__(@Autowired))
    private OrderDetailService orderDetailService;

    @Setter(onMethod = @__(@Autowired))
    private SaleInvitationService saleInvitationService;

    @Setter(onMethod = @__(@Autowired))
    private MailerService mailerService;

    @Setter(onMethod = @__(@Autowired))
    private UserService userService;

    @Setter(onMethod = @__(@Autowired))
    private LicenseService licenseService;

    public List<ProductOrderInfo> findOrders(Long organizationId) throws ValidationException {

        User user = userService.findUserByEmail(authentication().getName())
                .orElseThrow(() -> new ValidationException(UNEXPECTED_ERROR));

        List<ProductOrder> organizationOrders = repository.findOrganizationOrders(organizationId);

        return mapperUtil("productOrderMapper").map(organizationOrders, ProductOrderInfo.class);
    }

    @SuppressWarnings("Duplicates")
    @Transactional(
            rollbackFor = { Throwable.class },
            noRollbackFor = { WebServiceException.class })
    public void organizationOrder(OrganizationOrderRequest organizationOrder) throws Exception {

        @NotNull OrganizationRequest organizationInput = organizationOrder.getOrganization();
        @NotNull PaymentMethodRequest paymentMethodInput = organizationOrder.getPaymentMethod();
        @NotNull ProductOrderRequest orderInput = organizationOrder.getOrder();
        final boolean ignoringSaleInvitation = organizationOrder.isIgnoringSaleInvitation();

        SaleInvitation saleInvitation = checkInvitacion(organizationOrder.getSaleInvitationUuid(), ignoringSaleInvitation);

        Organization createdOrganization = organizationService.createOrganization(organizationInput);

        paymentMethodInput.setCurrentFlow(PaymentMethodFlow.CreatingOrganizationOrder);
        paymentMethodInput.setOrganizationId(createdOrganization.getId());
        paymentMethodInput.setDefault();

        PaymentMethod paymentMethod = paymentMethodService
                .createPaymentMethod(paymentMethodInput, false);

        orderInput.setOrderType(CreateOrganization);
        orderInput.setPaymentMethodId(paymentMethod.getId());

        orderInput.getOrderDetails().forEach(od -> od.setOrder(orderInput));

        ProductOrder createdProductOrder = createOrganizationOrder(createdOrganization, orderInput);

        useInvitation(saleInvitation, createdProductOrder);

        // Default organization order
        licenseService.processOrderLicenses(createdProductOrder);

        OrderPayment orderPayment = paymentService.payOrganizationOrder(createdProductOrder, paymentMethod);

        createdProductOrder.setOrderPayment(orderPayment);
        createdProductOrder.setUser(createdOrganization.getOwner());

        enqueueOrderNotification(createdProductOrder);
        resolveUserGeneration(createdOrganization.getOwner());

        mailerService.sendMailQueue();
    }

    private SaleInvitation checkInvitacion(String saleInvitationUuid, boolean ignoringSaleInvitation) throws Exception {

        if (ignoringSaleInvitation)
            return null;

        if (isEmpty(saleInvitationUuid))
            throw new ValidationException(UNEXPECTED_ERROR);

        return saleInvitationService.findInvitation(saleInvitationUuid)
                .orElseThrow(() -> new ValidationException(UNEXPECTED_ERROR));
    }

    private void useInvitation(SaleInvitation saleInvitation, ProductOrder productOrder) throws Exception {

        if (saleInvitation != null) {
            saleInvitation.setStatus(Used);
            saleInvitation.setProductOrder(productOrder);
            saleInvitationService.saveInvitation(saleInvitation);
        }

    }

    @SuppressWarnings("Duplicates")
    @Transactional(
            rollbackFor = { Throwable.class },
            noRollbackFor = { WebServiceException.class })
    public Organization organizationMigrationOrder(OrganizationOrderMigrationRequest organizationOrder) throws Exception {

        @NotNull OrganizationRequest organizationInput = organizationOrder.getOrganization();
        @NotNull List<PaymentMethodMigrationRequest> paymentMethods = organizationOrder.getPaymentMethods();
        @NotNull ProductOrderRequest orderInput = organizationOrder.getOrder();

        Organization createdOrganization = organizationService.createOrganization(organizationInput);

        AtomicReference<PaymentMethod> defaultPaymentMethod = new AtomicReference<>();

        paymentMethods.forEach(paymentMethodInput -> {

            try {

                if (paymentMethodInput.isDefaultMethod())
                    paymentMethodInput.setCurrentFlow(PaymentMethodFlow.CreatingOrganizationOrder);

                paymentMethodInput.setOrganizationId(createdOrganization.getId());

                PaymentMethod paymentMethod = paymentMethodService
                        .migratePaymentMethod(paymentMethodInput);

                if (paymentMethodInput.isDefaultMethod())
                    defaultPaymentMethod.set(paymentMethod);

                log.info("Payment method created with id: {}", paymentMethod.getId());

            } catch (Exception ex) {
                log.error("Error creation payment method. ", ex);
            }

        });

        orderInput.setOrderType(CreateOrganization);
        orderInput.setPaymentMethodId(defaultPaymentMethod.get().getId());

        ProductOrder createdProductOrder = createOrganizationOrder(createdOrganization, orderInput);
        // Migration order
        licenseService.processOrderLicenses(createdProductOrder);

        OrderPayment orderPayment = paymentService.payOrganizationOrder(createdProductOrder, defaultPaymentMethod.get());

        createdProductOrder.setOrderPayment(orderPayment);
        createdProductOrder.setUser(createdOrganization.getOwner());

        enqueueOrderNotification(createdProductOrder);

        return createdOrganization;
    }

    public void resolveUserGeneration(User owner) throws IntegrationException {
        userService.resolveUserGeneration(owner);
    }

    @SuppressWarnings("Duplicates")
    @Transactional(rollbackFor = { Throwable.class })
    public void licenseOrder(LicenseOrderRequest licenseOrder) throws Exception {

        @NotNull ProductOrderRequest orderInput = licenseOrder.getOrder();

        PaymentMethod paymentMethod = paymentMethodService
                .findDefaultPaymentMethod(licenseOrder.getOrganizationId());

        Organization organization = validateOrganizationRequestPayment(licenseOrder.getOrganizationId(), paymentMethod);

        orderInput.setOrderType(AddingLicenses);
        orderInput.setPaymentMethodId(paymentMethod.getId());
        orderInput.getOrderDetails().forEach(od -> od.setOrder(orderInput));

        ProductOrder createdProductOrder = createOrganizationOrder(organization, orderInput);
        licenseService.processOrderLicenses(createdProductOrder);

        OrderPayment orderPayment = paymentService.payOrganizationOrder(createdProductOrder, paymentMethod);

        createdProductOrder.setOrderPayment(orderPayment);
        createdProductOrder.setUser(organization.getOwner());

        enqueueOrderNotification(createdProductOrder);

        mailerService.sendMailQueue();
    }

    @SuppressWarnings("Duplicates")
    public ProductOrder licenseRenovationOrder(License license, RenovateLicenseRequest renovateLicense) throws Exception {

        final PaymentMethod paymentMethod = paymentMethodService
                .findDefaultPaymentMethod(license.getOrganizationId());

        ProductOrderRequest orderInput = new ProductOrderRequest();
        orderInput.setOrderType(LicenseRenovation);
        orderInput.setPaymentMethodId(paymentMethod.getId());

        return createLicenseOrder(license, license.getLicenseProduct(), orderInput, paymentMethod);
    }

    @SuppressWarnings("Duplicates")
    public ProductOrder licenseUpdateOrder(License license, UpdateLicenseRequest updateLicense) throws Exception {

        final LicenseProduct licenseProduct = licenseProductService
                .findLicenseProduct(updateLicense.getProductId());

        final PaymentMethod paymentMethod = paymentMethodService
                .findDefaultPaymentMethod(license.getOrganizationId());

        ProductOrderRequest orderInput = new ProductOrderRequest();
        orderInput.setOrderType(UpdateLicense);
        orderInput.setPaymentMethodId(paymentMethod.getId());

        return createLicenseOrder(license, licenseProduct, orderInput, paymentMethod);
    }

    private ProductOrder createLicenseOrder(License license, LicenseProduct licenseProduct,
                                            ProductOrderRequest orderRequest, PaymentMethod paymentMethod) throws Exception {

        final UserOrganization userOrganization = license.getUserOrganization();
        final Role role = userOrganization.getMainRole();
        final User user = userOrganization.getUser();

        UserLicenseAssignmentRequest assignmentRequest =
                new UserLicenseAssignmentRequest(role.getId(), user.getEmail());

        OrderDetailRequest orderDetailRequest = new OrderDetailRequest();
        orderDetailRequest.setLicenseId(license.getId());
        orderDetailRequest.setDurationId(license.getDuration().getIdentifier());
        orderDetailRequest.setProductId(licenseProduct.getId());
        orderDetailRequest.setAssignment(assignmentRequest);
        orderDetailRequest.setQuantity(1);

        orderRequest.addDetail(orderDetailRequest);

        return createOrganizationOrder(license.getOrganization(), orderRequest);
    }

    public void payOrder(ProductOrder productOrder, PaymentMethod paymentMethod, User owner) throws Exception {

        if (productOrder.getTotal().compareTo(ZERO) > 0) {

            OrderPayment orderPayment = paymentService.payOrganizationOrder(productOrder, paymentMethod);

            if (!orderPayment.isApproved())
                throw new UnsuccessfulPaymentException(getMessage(PAYMENT_UNSUCCESSFUL.getKey()));

            productOrder.setOrderPayment(orderPayment);
        }

        productOrder.setUser(owner);

        enqueueOrderNotification(productOrder);
    }

    private Organization validateOrganizationRequestPayment(Long organizationId, PaymentMethod paymentMethod) throws ValidationException {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (!paymentMethod.getOrganizationId().equals(organizationId))
            throw new ValidationException(UNEXPECTED_ERROR);

        Organization organization = organizationService.findOrganization(organizationId);

        if (!organization.getOwnerEmail()
                .equalsIgnoreCase(authentication.getName()))
            throw new ValidationException(UNEXPECTED_ERROR);

        return organization;
    }

    public ProductOrder createOrganizationOrder(Organization organization, ProductOrderRequest orderInput) throws Exception {

        ProductOrder productOrder = new ProductOrder();

        productOrder.setPaymentMethod(getReference(PaymentMethod.class, orderInput.getPaymentMethodId()));
        productOrder.setUser(organization.getOwner());
        productOrder.setOrganization(organization);
        productOrder.setType(orderInput.getOrderType());
        productOrder.setStatus(Created);

        List<OrderDetailRequest> invalidOrderDetails = new LinkedList<>();

        orderInput.getOrderDetails().forEach(orderDetailRequest -> {

            try {

                OrderDetail orderDetail = buildOrderDetail(orderDetailRequest);

                productOrder.addDetail(orderDetail);

            } catch (Exception ex) {
                invalidOrderDetails.add(orderDetailRequest);
            }

        });

        if (productOrder.isEmpty())
            throw new ValidationException(UNEXPECTED_ERROR);

        establishPeriods(productOrder);

        save(productOrder);

        return productOrder;
    }

    private void establishPeriods(ProductOrder productOrder) {

        productOrder.getOrderDetails().forEach(orderDetail -> {

            final LicenseDurationType licenseDuration = orderDetail.getDuration();
            final LocalDate startDate = LocalDate.now();

            final License license = orderDetail.getLicense();
            LocalDate endDate = startDate.plus(1, licenseDuration.getUnit());

            if (license != null)
                endDate = license.getExpiration().toLocalDate();

            orderDetail.setPeriodStartDate(startDate);
            orderDetail.setPeriodEndDate(endDate);

        });

    }

    private OrderDetail buildOrderDetail(OrderDetailRequest orderDetailRequest) throws Exception {

        LicenseDurationType duration = validateLicenseDuration(orderDetailRequest.getDurationId());
        UserLicenseAssignment assignment = assignment(orderDetailRequest.getAssignment());

        ProductOrderRequest order = orderDetailRequest.getOrder();

        validateOrderDetailAssignment(order, assignment);

        LicenseProduct targetLicenseProduct = licenseProductService.findLicenseProduct(orderDetailRequest.getProductId());

        OrderDetail orderDetail = new OrderDetail();

        orderDetail.setQuantity(orderDetailRequest.getQuantity());
        orderDetail.setDuration(duration);
        orderDetail.setAssignment(assignment);
        orderDetail.setLicenseProductId(orderDetailRequest.getProductId());
        orderDetail.setLicenseProduct(targetLicenseProduct);

        if (orderDetailRequest.isLicenseEstablished()) {
            License licenseReference = entityManager.getReference(License.class, orderDetailRequest.getLicenseId());
            assignment.setLicenseId(licenseReference.getId());
            orderDetail.setLicense(licenseReference);
        }

        calculateDetailCost(order, orderDetail, targetLicenseProduct, duration);

        return orderDetail;
    }

    private void calculateDetailCost(ProductOrderRequest order, OrderDetail orderDetail,
                                     LicenseProduct targetLicenseProduct, LicenseDurationType duration) {

        final Cost targetCost = targetLicenseProduct.getCost();
        final BigDecimal price = targetCost.getPrice(duration);
        final BigDecimal tax = targetCost.getTax(duration);

        switch (order.getOrderType()) {
            case UpdateLicense: {
                License license = orderDetail.getLicense();
                if (license.isExpired()) {
                    orderDetail.setPrice(price);
                    orderDetail.setTax(tax);
                } else {
                    // Greather License
                    final LicenseProduct currentLicenseProduct = license.getLicenseProduct();
                    final Cost currentCost = currentLicenseProduct.getCost();
                    if (targetLicenseProduct.isGreatherThan(currentLicenseProduct)) {

                        int currentMonthDays = YearMonth.now().lengthOfMonth();
                        long daysDifference = license.expirationDaysDifference();

                        if (daysDifference < 0) daysDifference = 0;

                        BigDecimal subtractPrice = ZERO;
                        BigDecimal subtractTax = ZERO;

                        if (daysDifference > 0) {

                            if (daysDifference == currentMonthDays) {

                                OrderDetail lastDetail = orderDetailService.findLastOrderDetail(license.getId());

                                if (!lastDetail.getSubtotal()
                                        .equals(ZERO.setScale(2, HALF_UP))) {
                                    subtractPrice = currentCost.getPrice(duration);
                                    subtractTax = currentCost.getTax(duration);
                                }

                            } else {
                                subtractPrice = currentCost.getPrice(duration, daysDifference);
                                subtractTax = currentCost.getTax(duration, daysDifference);
                            }
                        }

                        orderDetail.setPrice(targetCost.getPrice(duration).subtract(subtractPrice));
                        orderDetail.setTax(targetCost.getTax(duration).subtract(subtractTax));
                    } else {
                        orderDetail.setPrice(ZERO);
                        orderDetail.setTax(ZERO);
                    }
                }
                break;
            }
            case CreateOrganization: {
                orderDetail.setPrice(price);
                orderDetail.setTax(tax);
                break;
            }
            case LicenseRenovation: {
                orderDetail.setPrice(price);
                orderDetail.setTax(tax);
                break;
            }
            case AddingLicenses: {
                orderDetail.setPrice(price);
                orderDetail.setTax(tax);
                break;
            }
        }

    }

    private void validateOrderDetailAssignment(ProductOrderRequest productOrder,
                                               UserLicenseAssignment assignment) throws ValidationException {

        final ProductOrderType orderType = productOrder.getOrderType();

        switch (orderType) {
            case CreateOrganization: {
                final String email = assignment.getEmail();
                if (userService.userExists(email)) {
                    Optional<User> user = userService.findUserByEmail(email);
                    if (user.isPresent() && user.get().hasLicense())
                        throw new ValidationException(getMessage("organization.order.invalid.assignment"));
                } break;
            }
            case UpdateLicense: {
                // nothing
            }
            case AddingLicenses: {
                // nothing
            }
            case LicenseRenovation: {
                // nothing
            }
        }

    }

    private UserLicenseAssignment assignment(UserLicenseAssignmentRequest assignmentRequest)
            throws ValidationException {

        if (assignmentRequest == null) return null;

        RoleType role = validateRole(assignmentRequest.getRoleId());

        UserLicenseAssignment assignment = new UserLicenseAssignment();
        assignment.setEmail(assignmentRequest.getEmail());
        assignment.setRole(role);

        return assignment;
    }

    private void enqueueOrderNotification(ProductOrder productOrder) {

        MailTemplate template = MailTemplate.ORGANIZATION_ORDER_CONFIRMATION;
        GroupType group = GroupType.QBITS_CHAT;

        OrderConfirmationMail orderMail = mapperUtil
                .map(productOrder, OrderConfirmationMail.class);

        MailRecipients mailRecipients = new MailRecipients(productOrder.getUser().getEmail());

        mailerService.enqueueMail(group, template, orderMail.getTypeDescription(), mailRecipients, orderMail);
    }

    private Authentication authentication() {
        return SecurityContextHolder.getContext().getAuthentication();
    }

}
