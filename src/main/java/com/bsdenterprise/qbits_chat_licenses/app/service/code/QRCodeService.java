package com.bsdenterprise.qbits_chat_licenses.app.service.code;

import static com.bsdenterprise.qbits_chat_licenses.common.util.Constants.QR_CODE_LENGTH;
import static com.bsdenterprise.qbits_chat_licenses.app.type.mailer.config.GroupType.QBITS_CHAT;
import static com.bsdenterprise.qbits_chat_licenses.app.type.mailer.config.MailTemplate.SHARE_QR;
import static com.bsdenterprise.qbits_chat_licenses.app.util.IdentifierUtil.generateAlphanumericCode;
import static com.bsdenterprise.qbits_chat_licenses.common.config.message.MessageCode.UNEXPECTED_ERROR;
import static com.bsdenterprise.qbits_chat_licenses.app.type.info.CodeStatus.Created;

import com.bsdenterprise.qbits_chat_licenses.app.dto.code.qr.response.QRCodeConsumedResponse;
import com.bsdenterprise.qbits_chat_licenses.app.dto.code.qr.request.QRCodeRequest;
import com.bsdenterprise.qbits_chat_licenses.app.dto.code.qr.response.QRCodeResponse;
import com.bsdenterprise.qbits_chat_licenses.app.dto.mailer.integration.MailRecipients;
import com.bsdenterprise.qbits_chat_licenses.app.dto.mailer.mail.code.qr.QRCodeMail;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.code.QRCode;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.organization.auth.User;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.repository.code.QRCodeRepository;
import com.bsdenterprise.qbits_chat_licenses.app.service.mailer.MailerService;
import com.bsdenterprise.qbits_chat_licenses.app.service.organization.UserService;
import com.bsdenterprise.qbits_chat_licenses.common.config.exception.model.ValidationException;
import com.bsdenterprise.qbits_chat_licenses.common.service.BaseService;

import org.springframework.core.env.Environment;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import static java.util.Objects.isNull;
import static org.springframework.util.StringUtils.isEmpty;

import java.util.Optional;

import lombok.extern.slf4j.Slf4j;
import lombok.Setter;
import org.springframework.util.StringUtils;

@Slf4j
@Service
public class QRCodeService extends BaseService<QRCodeRepository, QRCode> {

    @Setter(onMethod = @__(@Autowired))
    private MailerService mailerService;

    @Setter(onMethod = @__(@Autowired))
    private UserService userService;

    @Setter(onMethod = @__(@Autowired))
    private Environment env;

    private QRCode generateQRCode(QRCodeRequest qrCodeRequest) throws Exception {

        final String email = qrCodeRequest.getEmail();
        final Integer configuration = qrCodeRequest.getConfiguration();

        if (isNull(configuration))
            throw new ValidationException(UNEXPECTED_ERROR);

        User user = userService.findUserByEmail(authentication().getName())
                .orElseThrow(() -> new ValidationException(UNEXPECTED_ERROR));

        User toUser = null;
        Optional<QRCode> optionalQRCode = Optional.empty();

        if (!isEmpty(email)) {

            toUser = userService.findUserByEmail(email)
                    .orElseThrow(() -> new ValidationException(UNEXPECTED_ERROR));

            optionalQRCode = repository.findQRCode(user.getId(), email);

        } else {
            optionalQRCode = repository.findQRCode(user.getId());
        }

        if (optionalQRCode.isPresent())
            return optionalQRCode.get();

        else {

            QRCode code = new QRCode();

            code.setUser(user);
            code.setToUser(toUser);
            code.setEmail(email);
            code.setCode(searchAvailableQRCode(email));
            code.setStatus(Created);
            code.setConfiguration(configuration);

            return save(code);
        }
    }

    @Transactional
    public QRCodeResponse getQRCode(QRCodeRequest qrCodeRequest) throws Exception {
        return mapperUtil().map(generateQRCode(qrCodeRequest), QRCodeResponse.class);
    }

    @Transactional
    public void generateAndSendQRCode(QRCodeRequest qrCodeRequest) throws Exception {
        enqueueQRMail(generateQRCode(qrCodeRequest));
        mailerService.sendMailQueue();
    }

    @Transactional
    public QRCodeConsumedResponse consumeQRCode(String code) throws Exception {

        User user = userService.findUserByEmail(authentication().getName())
                .orElseThrow(() -> new ValidationException(UNEXPECTED_ERROR));

        ValidationException codeNotFound = new ValidationException(UNEXPECTED_ERROR);

        QRCode qrCode = repository.findQRCode(code).orElseThrow(() -> codeNotFound);

        if (qrCode.getToUserId() != null) {

            if (!repository.canVerifyCode(authentication().getName(), code))
                throw codeNotFound;

            if (!qrCode.getToUser().getEmail().equals(user.getEmail()))
                throw new ValidationException(UNEXPECTED_ERROR);

            repository.markCodeAsUsed(qrCode.getId());
        }

        return QRCodeConsumedResponse.builder()
                .configuration(qrCode.getConfiguration())
                .jabberId(qrCode.getUser().getJid())
                .build();
    }

    public String redirectUrl(String code) {
        return String.format(env.getProperty("app.chat.qr-flow.redirect-url", ""), code);
    }

    private String searchAvailableQRCode(String email) {

        String code = generateAlphanumericCode(QR_CODE_LENGTH).toUpperCase();

        while (repository.exists(email, code))
            code = generateAlphanumericCode(QR_CODE_LENGTH).toUpperCase();

        return code;
    }

    private void enqueueQRMail(QRCode qrCode) throws Exception {

        final String redirectUrl = env.getProperty("app.chat.qr-flow.redirect-server-url", "");

        QRCodeMail qrCodeMail = mapperUtil().map(qrCode, QRCodeMail.class);
        qrCodeMail.setRedirectUrl(String.format(redirectUrl, qrCode.getCode()));

        mailerService.enqueueMail(QBITS_CHAT, SHARE_QR, new MailRecipients(qrCode.getEmail()), qrCodeMail);
    }

    private Authentication authentication() {
        return SecurityContextHolder.getContext().getAuthentication();
    }

}
