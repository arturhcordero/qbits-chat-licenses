package com.bsdenterprise.qbits_chat_licenses.app.service.notification;

import static com.bsdenterprise.qbits_chat_licenses.common.config.message.MessageCode.UNEXPECTED_ERROR;
import static com.bsdenterprise.qbits_chat_licenses.common.util.Constants.SNS_MESSAGE_STRUCTURE_JSON;
import com.bsdenterprise.qbits_chat_licenses.common.config.exception.model.ValidationException;

import com.bsdenterprise.qbits_chat_licenses.app.dto.notification.request.APNSNotificationRequest;
import com.bsdenterprise.qbits_chat_licenses.app.dto.notification.request.GCMNotificationRequest;

import com.bsdenterprise.qbits_chat_licenses.app.service.organization.UserService;

import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.notification.DeviceToken;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.organization.auth.User;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.springframework.beans.factory.annotation.Autowired;
import com.google.common.collect.ImmutableMap;
import org.springframework.stereotype.Service;
import javax.annotation.PostConstruct;

import com.amazonaws.services.sns.model.MessageAttributeValue;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sns.AmazonSNS;

import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.SneakyThrows;
import lombok.Setter;

@Service
public class NotificationService {

    @Setter(onMethod = @__(@Autowired))
    private UserService userService;

    @Setter(onMethod = @__(@Autowired))
    private AmazonSNS snsClient;

    private ObjectMapper objectMapper;

    @PostConstruct
    public void init() {
        objectMapper = new ObjectMapper();
        objectMapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
    }

    @SneakyThrows
    public void sendGcmNotification(String jabberId, GCMNotificationRequest gcmNotification) {

        User user = userService.findUserByJabberId(jabberId)
                .orElseThrow(() -> new ValidationException(UNEXPECTED_ERROR));

        DeviceToken deviceToken = user.getDeviceToken();

        PublishRequest publishRequest = new PublishRequest()
                .withMessageStructure(SNS_MESSAGE_STRUCTURE_JSON)
                .withTargetArn(deviceToken.getPlatformEndpointArn())
                .withMessage(gcmPayload(deviceToken, gcmNotification));

        ttlValue(deviceToken, publishRequest);

        snsClient.publish(publishRequest);
    }

    @SneakyThrows
    private String gcmPayload(DeviceToken deviceToken, GCMNotificationRequest gcmNotification) {

        String payload = objectMapper.writeValueAsString(gcmNotification);

        return objectMapper.writeValueAsString(
                new ImmutableMap.Builder<String, String>()
                        .put(deviceToken.getPlatformType().name(), payload)
                        .build()
        );
    }

    @SneakyThrows
    public void sendApnsNotification(String jabberId, APNSNotificationRequest apnsNotification) {

        User user = userService.findUserByJabberId(jabberId)
                .orElseThrow(() -> new ValidationException(UNEXPECTED_ERROR));

        DeviceToken deviceToken = user.getDeviceToken();

        PublishRequest publishRequest = new PublishRequest()
                .withMessageStructure(SNS_MESSAGE_STRUCTURE_JSON)
                .withTargetArn(deviceToken.getPlatformEndpointArn())
                .withMessage(apnsPayload(deviceToken, apnsNotification));

        backgroundPublish(apnsNotification, publishRequest);
        ttlValue(deviceToken, publishRequest);
        voipPublish(publishRequest);
        priority(publishRequest);

        snsClient.publish(publishRequest);
    }

    @SneakyThrows
    private String apnsPayload(DeviceToken deviceToken, APNSNotificationRequest apnsNotification) {

        String payload = objectMapper.writeValueAsString(apnsNotification);

        return objectMapper.writeValueAsString(
                new ImmutableMap.Builder<String, String>()
                        .put(deviceToken.getPlatformType().name(), payload)
                        .build()
        );
    }

    private void voipPublish(PublishRequest publishRequest) {

        MessageAttributeValue backgroungAttribute = new MessageAttributeValue();
        backgroungAttribute.setDataType("String");
        backgroungAttribute.setStringValue("voip");

        final String key = "AWS.SNS.MOBILE.APNS.PUSH_TYPE";

        publishRequest.addMessageAttributesEntry(key, backgroungAttribute);
    }

    private void ttlValue(DeviceToken deviceToken, PublishRequest publishRequest) {

        final String ttlMinutesValue = Integer.toString(24 * 5 * 60 * 60);

        MessageAttributeValue ttlAttribute = new MessageAttributeValue();
        ttlAttribute.setDataType("String");
        ttlAttribute.setStringValue(ttlMinutesValue);

        String key;

        switch (deviceToken.getPlatformType()) {
            case GCM:
                key = "AWS.SNS.MOBILE.FCM.TTL"; break;
            case APNS:
                key = "AWS.SNS.MOBILE.APNS.TTL"; break;
            case APNS_SANDBOX:
                key = "AWS.SNS.MOBILE.APNS_SANDBOX.TTL"; break;
            case APNS_VOIP:
                key = "AWS.SNS.MOBILE.APNS_VOIP.TTL"; break;
            case APNS_VOIP_SANDBOX:
                key = "AWS.SNS.MOBILE.APNS_VOIP_SANDBOX.TTL"; break;
            default:
                key = "AWS.SNS.MOBILE.APNS.TTL"; break;
        }

        publishRequest.addMessageAttributesEntry(key, ttlAttribute);
    }

    private void priority(PublishRequest publishRequest) {

        MessageAttributeValue priorityAttribute = new MessageAttributeValue();
        priorityAttribute.setDataType("String");
        priorityAttribute.setStringValue("10");

        publishRequest.addMessageAttributesEntry("AWS.SNS.MOBILE.APNS.PRIORITY", priorityAttribute);
    }

    private void backgroundPublish(APNSNotificationRequest apnsNotification, PublishRequest publishRequest) {

        if (apnsNotification.getAps().isSilent()) {

            MessageAttributeValue backgroungAttribute = new MessageAttributeValue();
            backgroungAttribute.setDataType("String");
            backgroungAttribute.setStringValue("background");

            publishRequest.addMessageAttributesEntry("AWS.SNS.MOBILE.APNS.PUSH_TYPE", backgroungAttribute);
        }

    }

}
