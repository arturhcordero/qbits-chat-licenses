package com.bsdenterprise.qbits_chat_licenses.app.service.organization;

import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.organization.Organization;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.organization.auth.Role;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.organization.auth.User;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.organization.auth.UserOrganizationRole;
import com.bsdenterprise.qbits_chat_licenses.app.type.organization.UserOrganizationStatusType;
import com.bsdenterprise.qbits_chat_licenses.app.type.organization.auth.RoleType;
import com.bsdenterprise.qbits_chat_licenses.common.service.BaseService;

import com.bsdenterprise.qbits_chat_licenses.app.persistence.repository.organization.UserOrganizationRepository;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.organization.UserOrganization;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import lombok.Setter;

import javax.persistence.criteria.*;

import static org.springframework.util.StringUtils.isEmpty;

@Slf4j
@Service
public class UserOrganizationService extends BaseService<UserOrganizationRepository, UserOrganization> {

    public Page<UserOrganization> findUsers(String search, Long organizationId, String authenticatedUser, Pageable pageable) {

        return repository.findAll((Specification<UserOrganization>) (root, query, cb) -> {

            Path<User> user = root.get("user");

            Predicate organizationPredicated = cb.equal(root.get("organizationId"), organizationId);
            Predicate emailAuthenticatedPredicate = cb.notEqual(user.get("email"), authenticatedUser);

            Predicate finalPredicated = cb.and(organizationPredicated, emailAuthenticatedPredicate);

            if (!isEmpty(search)) {

                Expression<String> concatExpression = cb.concat(
                        cb.coalesce(user.get("name"), ""),
                        cb.concat(
                                cb.coalesce(user.get("lastName"), ""),
                                cb.coalesce(user.get("secondLastName"), "")
                        )
                );

                Predicate searchPredicated = cb.like(concatExpression, "%" + search + "%");
                Predicate emailPredicated = cb.like(user.get("email"), "%" + search + "%");
                Predicate queryPredicated = cb.or(searchPredicated, emailPredicated);

                finalPredicated = cb.and(finalPredicated, queryPredicated);

            }

            return finalPredicated;

        }, pageable);

    }

    public void updateUserOrganization(UserOrganization userOrganization) throws Exception {
        save(userOrganization);
    }

    public void removeUserOrganizationLicense(UserOrganization userOrganization) throws Exception {
        userOrganization.setLicense(null);
        save(userOrganization);
    }

    public UserOrganization relateUserOrganization(User user, Organization organization, RoleType role) throws Exception {

        UserOrganization userOrganization = new UserOrganization(user, organization);
        userOrganization.setStatus(UserOrganizationStatusType.Active);

        UserOrganizationRole userOrganizationRole = new UserOrganizationRole();
        userOrganizationRole.setRole(getReference(Role.class, role.getIdentifier()));

        userOrganization.addUserOrganizationRole(userOrganizationRole);

        return super.save(userOrganization);
    }

    public void addRole(UserOrganization userOrganization, RoleType role) throws Exception {

        UserOrganizationRole userOrganizationRole = new UserOrganizationRole();
        userOrganizationRole.setRole(getReference(Role.class, role.getIdentifier()));

        userOrganization.addUserOrganizationRole(userOrganizationRole);

        super.save(userOrganization);
    }

}
