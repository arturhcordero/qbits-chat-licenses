package com.bsdenterprise.qbits_chat_licenses.app.service.payment;

import com.bsdenterprise.qbits_chat_licenses.app.config.qbits_pay.QBitspayConfig;
import com.bsdenterprise.qbits_chat_licenses.app.dto.qbits_pay.integration.info.BusinessInfo;
import com.bsdenterprise.qbits_chat_licenses.app.dto.qbits_pay.integration.sale.create.QBitsSaleRequest;
import com.bsdenterprise.qbits_chat_licenses.app.dto.qbits_pay.integration.sale.create.QBitspaySaleRequest;
import com.bsdenterprise.qbits_chat_licenses.app.dto.qbits_pay.integration.sale.create.QbitsPaymentMethodRequest;
import com.bsdenterprise.qbits_chat_licenses.app.dto.qbits_pay.integration.sale.response.PaymentAuthorizationResponse;
import com.bsdenterprise.qbits_chat_licenses.app.dto.qbits_pay.integration.sale.response.QBitspaySaleResponse;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.payment.PaymentMethod;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.order.ProductOrder;

import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.order.payment.OrderPayment;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.repository.payment.OrderPaymentRepository;
import com.bsdenterprise.qbits_chat_licenses.app.service.qbits_pay.PaymentClient;
import com.bsdenterprise.qbits_chat_licenses.app.type.order.OrderPaymentType;
import com.bsdenterprise.qbits_chat_licenses.app.type.order.ProsaCodeType;
import com.bsdenterprise.qbits_chat_licenses.common.service.BaseService;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.util.UUID;

@Slf4j
@Service
public class PaymentService extends BaseService<OrderPaymentRepository, OrderPayment> {

    @Setter(onMethod = @__(@Autowired))
    private PaymentClient paymentClient;

    @Setter(onMethod = @__(@Autowired))
    private QBitspayConfig qBitspayConfig;

    public OrderPayment payOrganizationOrder(ProductOrder productOrder, PaymentMethod paymentMethod) throws Exception {

        QbitsPaymentMethodRequest qbitsPaymentMethodRequest = new QbitsPaymentMethodRequest();
        mapperUtil().map(paymentMethod, qbitsPaymentMethodRequest);

        QBitsSaleRequest sale = new QBitsSaleRequest();
        mapperUtil().map(qBitspayConfig, sale);

        sale.setBusinessInfo(businessInfo());
        sale.setTransactionIdentifier(transactionIdentifier());
        sale.setTotal(formatOrderTotal(productOrder.getTotal()));

        if (qBitspayConfig.isEnabled())
            return createPayment(OrderPaymentType.QBitspay,
                    paymentClient.payOrganizationOrder(QBitspaySaleRequest.builder()
                            .paymentMethod(qbitsPaymentMethodRequest)
                            .sale(sale)
                            .build()),
                    productOrder);

        return createPayment(OrderPaymentType.Dummy,
                createDummyPayment(
                        sale.getTransactionIdentifier(),
                        sale.getTotal()
                ), productOrder);
    }

    public QBitspaySaleResponse createDummyPayment(String transactionIdentifier, String amount) {

        QBitspaySaleResponse qbitspaySaleResponse = new QBitspaySaleResponse();

        qbitspaySaleResponse.setMessage("Cargo exitoso");
        qbitspaySaleResponse.setProsaCode(ProsaCodeType.SUCCESS.getIdentifier());
        qbitspaySaleResponse.setResponseCode("OK");
        qbitspaySaleResponse.setTransactionIdentifier(transactionIdentifier);

        PaymentAuthorizationResponse authorizationResponse = new PaymentAuthorizationResponse();

        authorizationResponse.setAmount(amount);
        authorizationResponse.setAuthorizationCode(OrderPaymentType.Dummy.name());
        authorizationResponse.setTraceNumber(OrderPaymentType.Dummy.name());

        qbitspaySaleResponse.setAuthorization(authorizationResponse);

        return qbitspaySaleResponse;
    }

    private OrderPayment createPayment(OrderPaymentType orderPaymentType, QBitspaySaleResponse sale, ProductOrder productOrder) throws Exception {

        OrderPayment payment = new OrderPayment();

        mapperUtil().map(sale, payment);
        mapperUtil().map(sale.getAuthorization(), payment);

        payment.setType(orderPaymentType);
        payment.setProductOrder(productOrder);

        return super.save(payment);
    }

    private BusinessInfo businessInfo() {
        return mapperUtil().map(qBitspayConfig.getPayment(), BusinessInfo.class);
    }

    private String formatOrderTotal(BigDecimal total) {
        return String.format("%d", Math.round((total.doubleValue() * 100.0)));
    }

    private String transactionIdentifier() {

        int s4 = (int) Math.floor((Math.random()) * 0x10000);
        String pid = Integer.toHexString(s4);

        if (pid.length() <= 3)
            return transactionIdentifier();

        return String.format("%s%s", UUID.randomUUID(), pid);
    }

}
