package com.bsdenterprise.qbits_chat_licenses.app.service.openfire;

import static com.bsdenterprise.qbits_chat_licenses.common.config.message.MessageProperties.getMessage;

import com.bsdenterprise.qbits_chat_licenses.app.config.openfire.OpenfireConfig;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.organization.auth.User;

import com.bsdenterprise.qbits_chat_licenses.common.config.exception.model.IntegrationException;
import com.bsdenterprise.qbits_chat_licenses.common.config.exception.model.ValidationException;
import com.bsdenterprise.qbits_chat_licenses.common.util.MapperUtil;

import static javax.ws.rs.core.Response.Status.fromStatusCode;
import static javax.ws.rs.core.Response.Status.Family.*;

import javax.ws.rs.ClientErrorException;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

import org.igniterealtime.restclient.RestApiClient;
import org.igniterealtime.restclient.entity.UserEntity;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import lombok.Setter;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@Service
public class OpenfireService {

    @Setter(onMethod = @__(@Autowired))
    private OpenfireConfig openfireConfig;

    @Setter(onMethod = @__(@Autowired))
    private RestApiClient openfireClient;

    @Setter(onMethod = @__({@Autowired, @Qualifier("commonMapper")}))
    private MapperUtil mapperUtil;

    private Map<Thread, UserEntity> unresolvedUserCreation = new HashMap<>();

    public void setUserGeneration(User user) throws Exception { /*{ throws Exception*/

        UserEntity ofUser = mapperUtil.map(user, UserEntity.class);
        ofUser.setName(user.getFullName());
        ofUser.setPassword(user.getOriginalPassword());

        unresolvedUserCreation.put(currentThread(), ofUser);
    }

    public String resolveUserGeneration(boolean remove) throws IntegrationException {

        if (remove) {
            unresolvedUserCreation.remove(currentThread());
            return null;
        }

        UserEntity ofUser = unresolvedUserCreation.remove(currentThread());
        String username = ofUser.getUsername();

        try {

            openfireClient.getUser(username);

            Response response = openfireClient.updateUser(ofUser);

            log.info("Response: {}", response);

        } catch (ClientErrorException ex) {

            Response response = openfireClient.createUser(ofUser);

            if (fromStatusCode(response.getStatus()).getFamily() != SUCCESSFUL)
                throw new IntegrationException(getMessage("openfire.error.create-user"));

            response.close();
        }

        return String.format("%s@%s", username, openfireConfig.getJabberDomain());
    }

    public void changePassword(String username, String password) {

        UserEntity user = openfireClient.getUser(username);
        user.setPassword(password);

        openfireClient.updateUser(user);
    }

    public void removeUser(String username) throws IntegrationException {

        try {

            openfireClient.getUser(username);

        } catch (ClientErrorException ex) {
            throw new IntegrationException(getMessage("openfire.error.non-existent-user"));
        }

        Response response = openfireClient.deleteUser(username);

        if (fromStatusCode(response.getStatus()).getFamily() != SUCCESSFUL)
            throw new IntegrationException(getMessage("openfire.error.delete-user"));

        response.close();
    }

    private Thread currentThread() {
        return Thread.currentThread();
    }

}
