package com.bsdenterprise.qbits_chat_licenses.app.service.organization;

import com.bsdenterprise.qbits_chat_licenses.common.service.BaseService;

import com.bsdenterprise.qbits_chat_licenses.app.persistence.repository.organization.UserOrganizationRoleRepository;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.organization.auth.UserOrganizationRole;

import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class UserOrganizationRoleService extends BaseService<UserOrganizationRoleRepository, UserOrganizationRole> {

    public UserOrganizationRole findUserOrganizationRole(Long roleId) {
        return findById(roleId, UserOrganizationRole.class);
    }

}
