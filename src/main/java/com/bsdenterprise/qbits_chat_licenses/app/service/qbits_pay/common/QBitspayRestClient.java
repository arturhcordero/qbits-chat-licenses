package com.bsdenterprise.qbits_chat_licenses.app.service.qbits_pay.common;

import com.bsdenterprise.qbits_chat_licenses.common.config.exception.model.IntegrationException;

import com.bsdenterprise.qbits_chat_licenses.app.config.qbits_pay.oauth.QBitspayResourceDetails;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.*;

import java.io.IOException;
import java.net.*;

import lombok.Setter;

public abstract class QBitspayRestClient {

    @Setter(onMethod = @__({@Autowired, @Qualifier("qbitspayRestTemplate")}))
    private OAuth2RestTemplate restTemplate;

    @Setter(onMethod = @__(@Autowired))
    private QBitspayResourceDetails resourceDetails;

    protected <T> ResponseEntity<T> get(final String path, Class<T> outDTOClass) throws Exception {
        final HttpMethod method = HttpMethod.GET;
        RequestEntity requestEntity = createRequest(path, method, null);
        return send(requestEntity,outDTOClass);
    }

    protected <T> ResponseEntity<T> post(final String path, Object body, Class<T> clazz) throws Exception {
        final HttpMethod method = HttpMethod.POST;
        RequestEntity requestEntity = createRequest(path, method, body);
        return send(requestEntity,clazz);
    }
    protected <T> ResponseEntity<T> put(final String path, Object body, Class<T> outDTOClass) throws Exception {
        final HttpMethod method = HttpMethod.PUT;
        RequestEntity requestEntity = createRequest(path, method, body);
        return send(requestEntity,outDTOClass);
    }
    protected  <T> ResponseEntity<T> delete(final String path, Object body, Class<T> outDTOClass) throws Exception {
        final HttpMethod method = HttpMethod.DELETE;
        RequestEntity requestEntity = createRequest(path, method, body);
        return send(requestEntity,outDTOClass);
    }

    private RequestEntity createRequest(final String path, final HttpMethod method, Object body) throws Exception {

        try {

            final String baseUri = resourceDetails.getBaseUri();
            final UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(baseUri).path(path);
            final RequestEntity.BodyBuilder bodyBuilder = RequestEntity.method(method, builder.build().toUri());

            if (!isServiceAvailable()) {
                throw new IntegrationException(resourceDetails.getBaseUri(),
                        String.format("The url %s isn't available", baseUri));
            }

            bodyBuilder.contentType(MediaType.APPLICATION_JSON);

            bodyBuilder.header("X-API-KEY", resourceDetails.getApiKey());
            bodyBuilder.header("ClientId", resourceDetails.getClientId());

            if (method == HttpMethod.GET)
                return bodyBuilder.build();

            return bodyBuilder.body(body);

        } catch (IntegrationException ex) {
            throw ex;
        } catch (Exception ex) {
            ex.printStackTrace();

            String fullPath = String.format("%s/%s", resourceDetails.getBaseUri(), path);

            throw new IntegrationException(fullPath, "Can't create the request");
        }

    }

    private boolean isServiceAvailable() {

        final int timeout = 3000;

        final String url = String.format("%s/v1/version?api_key=%s",
                resourceDetails.getBaseUri(),
                resourceDetails.getApiKey());

        try {

            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            connection.setConnectTimeout(timeout);
            connection.setReadTimeout(timeout);
            connection.setRequestMethod("HEAD");

            int responseCode = connection.getResponseCode();
            return (200 <= responseCode && responseCode < 400);

        } catch (IOException ex) {
            ex.printStackTrace();
            return false;
        }
    }

    private <T> ResponseEntity<T> send(RequestEntity<?> requestEntity, Class<T> responseType) throws IntegrationException {

        try {

            return restTemplate.exchange(requestEntity, responseType);

        } catch (Exception ex) {
            String path = requestEntity.getUrl().toString();
            throw new IntegrationException(path, ex.getMessage());
        }

    }

}
