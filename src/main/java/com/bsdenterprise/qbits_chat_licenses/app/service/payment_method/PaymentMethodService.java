package com.bsdenterprise.qbits_chat_licenses.app.service.payment_method;

import static com.bsdenterprise.qbits_chat_licenses.common.config.message.MessageCode.*;
import static com.bsdenterprise.qbits_chat_licenses.common.config.message.MessageProperties.getMessage;
import static com.bsdenterprise.qbits_chat_licenses.common.util.Constants.*;
import static java.lang.String.format;
import static java.lang.String.valueOf;

import com.bsdenterprise.qbits_chat_licenses.app.config.qbits_pay.QBitspayConfig;
import com.bsdenterprise.qbits_chat_licenses.app.dto.payment.create.PaymentMethodRequest;
import com.bsdenterprise.qbits_chat_licenses.app.dto.payment.migration.PaymentMethodMigrationRequest;
import com.bsdenterprise.qbits_chat_licenses.app.dto.payment.read.PaymentMethodInfo;
import com.bsdenterprise.qbits_chat_licenses.app.dto.payment.update.PaymentMethodUpdateRequest;
import com.bsdenterprise.qbits_chat_licenses.app.dto.qbits_pay.integration.payment_method.update.QbitsPaymentMethodUpdate;
import com.bsdenterprise.qbits_chat_licenses.app.dto.qbits_pay.integration.payment_method.update.QbitsPaymentMethodUpdated;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.organization.Organization;
import com.bsdenterprise.qbits_chat_licenses.app.service.organization.OrganizationService;

import com.bsdenterprise.qbits_chat_licenses.app.service.qbits_pay.PaymentMethodClient;
import com.bsdenterprise.qbits_chat_licenses.app.dto.qbits_pay.integration.payment_method.create.QbitsPaymentMethodCreate;
import com.bsdenterprise.qbits_chat_licenses.app.dto.qbits_pay.integration.payment_method.create.QbitsPaymentMethodCreated;

import com.bsdenterprise.qbits_chat_licenses.app.util.IdentifierUtil;
import com.bsdenterprise.qbits_chat_licenses.common.config.exception.model.ValidationException;

import com.bsdenterprise.qbits_chat_licenses.app.persistence.repository.payment_method.PaymentMethodRepository;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.payment.PaymentMethod;

import com.bsdenterprise.qbits_chat_licenses.common.service.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import lombok.Setter;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.List;

@Slf4j
@Service
@CacheConfig(cacheNames = "paymentMethods")
public class PaymentMethodService extends BaseService<PaymentMethodRepository, PaymentMethod> {

    @Setter(onMethod = @__(@Autowired))
    private OrganizationService organizationService;

    @Setter(onMethod = @__(@Autowired))
    private PaymentMethodClient paymentMethodClient;

    @Setter(onMethod = @__(@Autowired))
    private QBitspayConfig qbitspayConfig;

    public List<PaymentMethodInfo> findPaymentMethodsInfo(Long organizationId) throws ValidationException {

        if (!organizationService.findOrganization(organizationId).getOwnerEmail()
                .equalsIgnoreCase(authentication().getName()))
            throw new ValidationException(UNEXPECTED_ERROR);

        return mapperUtil().map(repository.findPaymentMethods(organizationId), PaymentMethodInfo.class);
    }

    //@Cacheable(key = "#organizationId")
    public PaymentMethodInfo findDefaultPaymentMethodInfo(Long organizationId) throws Exception {

        if (!organizationService.findOrganization(organizationId).getOwnerEmail()
                .equalsIgnoreCase(authentication().getName()))
            throw new ValidationException(UNEXPECTED_ERROR);

        PaymentMethod paymentMethod = repository.findDefault(organizationId).orElseThrow(() ->
                new ValidationException(UNEXPECTED_ERROR));

        return mapperUtil().map(paymentMethod, PaymentMethodInfo.class);
    }

    public PaymentMethod findDefaultPaymentMethod(Long organizationId) throws ValidationException {

        return repository.findDefault(organizationId).orElseThrow(() ->
                new ValidationException(UNEXPECTED_ERROR));
    }

    public PaymentMethodInfo findPaymentMethodInfo(String identifier) throws Exception {

        PaymentMethod paymentMethod = findPaymentMethod(identifier);

        if (!organizationService.findOrganization(paymentMethod.getOrganizationId()).getOwnerEmail()
                .equalsIgnoreCase(authentication().getName()))
            throw new ValidationException(UNEXPECTED_ERROR);

        return mapperUtil().map(paymentMethod, PaymentMethodInfo.class);
    }

    public PaymentMethod findPaymentMethod(String paymentMethodIdentifier) throws ValidationException {

        return repository.findByUuid(paymentMethodIdentifier)
                .orElseThrow(() -> new ValidationException(UNEXPECTED_ERROR));
    }

    public PaymentMethod createPaymentMethod(PaymentMethodRequest paymentMethodInput, boolean validate) throws Exception {

        final Organization organization = checkOrganization(paymentMethodInput);

        if (validate && !organization.getOwnerEmail().equalsIgnoreCase(authentication().getName()))
            throw new ValidationException(UNEXPECTED_ERROR);

        validatePaymentMethodCreation(paymentMethodInput);

        QbitsPaymentMethodCreate qbitsPaymentMethod = new QbitsPaymentMethodCreate();
        mapperUtil().map(paymentMethodInput, qbitsPaymentMethod);

        qbitsPaymentMethod.setCvv(paymentMethodInput.getSecurityCode());
        qbitsPaymentMethod.setJidUuid(organization.getUuid());
        qbitsPaymentMethod.setAgreement(generateAgreement());
        qbitsPaymentMethod.setAlias(generateCreateAlias(qbitsPaymentMethod));

        QbitsPaymentMethodCreated paymentMethodCreated = new QbitsPaymentMethodCreated();
        mapperUtil().map(qbitsPaymentMethod, paymentMethodCreated);

        if (qbitspayConfig.isEnabled())
            paymentMethodCreated = paymentMethodClient.createPaymentMethod(qbitsPaymentMethod);

        PaymentMethod paymentMethod = new PaymentMethod();
        mapperUtil().map(paymentMethodCreated, paymentMethod);

        paymentMethod.setOrganization(getReference(Organization.class, organization.getId()));
        paymentMethod.setLastDigits(last4digits(paymentMethodInput.getCardNumber()));

        defaultPayment(organization.getId(), paymentMethodInput, paymentMethod);

        return save(paymentMethod);
    }

    public PaymentMethod updatePaymentMethod(String paymentMethodIdentifier, PaymentMethodUpdateRequest paymentMethodUpdate) throws Exception {

        PaymentMethod paymentMethod = findPaymentMethod(paymentMethodIdentifier);
        Organization organization = paymentMethod.getOrganization();

        if (!organization.getOwnerEmail().equalsIgnoreCase(authentication().getName()))
            throw new ValidationException(UNEXPECTED_ERROR);

        validatePaymentMethodUpdate(paymentMethodUpdate);

        QbitsPaymentMethodUpdate qbitsPaymentMethod = new QbitsPaymentMethodUpdate();

        qbitsPaymentMethod.setExpirationDate(paymentMethodUpdate.getExpirationDate());
        qbitsPaymentMethod.setCvv(paymentMethodUpdate.getSecurityCode());
        qbitsPaymentMethod.setJidUuid(organization.getUuid());
        qbitsPaymentMethod.setAgreement(paymentMethod.getAgreement());
        qbitsPaymentMethod.setAlias(paymentMethod.getAlias());

        QbitsPaymentMethodUpdated paymentMethodUpdated = new QbitsPaymentMethodUpdated();
        mapperUtil().map(qbitsPaymentMethod, paymentMethodUpdated);

        if (qbitspayConfig.isEnabled())
            paymentMethodUpdated = paymentMethodClient.updatePaymentMethod(qbitsPaymentMethod);

        paymentMethod.setExpirationDate(qbitsPaymentMethod.getExpirationDate());

        return save(paymentMethod);
    }

    public PaymentMethod migratePaymentMethod(PaymentMethodMigrationRequest paymentMethodInput) throws Exception {

        PaymentMethod paymentMethod = new PaymentMethod();
        mapperUtil().map(paymentMethodInput, paymentMethod);

        paymentMethod.setOrganization(getReference(Organization.class,
                paymentMethodInput.getOrganizationId()));

        return save(paymentMethod);
    }

    public void establishDefaultPayment(String paymentMethodIdentifier) throws Exception {

        PaymentMethod paymentMethod = findPaymentMethod(paymentMethodIdentifier);

        if (!paymentMethod.getOrganization().getOwnerEmail().equalsIgnoreCase(authentication().getName()))
            throw new ValidationException(UNEXPECTED_ERROR);

        unmarkDefaultMethod(paymentMethod.getOrganizationId());
        paymentMethod.setDefaultPayment(true);

        save(paymentMethod);
    }

    @Transactional
    public void removePaymentMethod(String paymentMethodIdentifier) throws Exception {

        PaymentMethod paymentMethod = findPaymentMethod(paymentMethodIdentifier);
        List<PaymentMethod> paymentMethods = repository.findPaymentMethods(paymentMethod.getOrganizationId());

        if (!paymentMethod.getOrganization().getOwnerEmail().equalsIgnoreCase(authentication().getName()))
            throw new ValidationException(UNEXPECTED_ERROR);

        paymentMethod.setDeleted(true);

        if (paymentMethod.isDefaultPayment()) {
            paymentMethod.setDefaultPayment(false);
            paymentMethods.stream()
                    .filter(pm -> !pm.getUuid().equals(paymentMethod.getUuid()))
                    .findFirst().ifPresent(dpm -> {
                        dpm.setDefaultPayment(true);
                        repository.save(dpm);
            });
        }

        save(paymentMethod);
    }

    private void defaultPayment(Long organizationId, PaymentMethodRequest input, PaymentMethod paymentMethod) {

        switch (input.getCurrentFlow()) {

            case CreatingOrganizationOrder:
                paymentMethod.setDefaultPayment(true); break;

            case CreatingPaymentMethod:

                if (input.isDefaultMethod()) {
                    unmarkDefaultMethod(organizationId);
                    paymentMethod.setDefaultPayment(true);
                }

                break;
        }

    }

    private void unmarkDefaultMethod(Long organizationId) {
        repository.findDefault(organizationId).ifPresent(defaultMethod ->
                defaultMethod.setDefaultPayment(false));
    }

    private String generateAgreement() {
        return IdentifierUtil.generateRandomIdentifier(DEFAULT_AGREEMENT_LENGTH);
    }

    private String generateCreateAlias(QbitsPaymentMethodCreate qbitsPaymentMethodCreate) {
        return generateAlias(qbitsPaymentMethodCreate.getCardNumber(), qbitsPaymentMethodCreate.getAlias(), false);
    }

    private String generateAlias(String cardNumber, String alias, boolean update) {

        String last4Digits = last4digits(cardNumber);

        if (update)
            return alias.substring(0, alias.length() - 4).concat(last4Digits);

        return format("%s-****-****-****-%s", alias, last4Digits);
    }

    private String last4digits(String cardNumber) {
        Assert.notNull(cardNumber, "Card number cannot be empty");
        return cardNumber.substring(cardNumber.length() - 4);
    }

    private void validatePaymentMethodCreation(PaymentMethodRequest paymentMethod) throws ValidationException {

        String expirationDate = validateExpirationDate(paymentMethod.getExpirationDate());

        if (!PAYMENT_METHOD_PATTERN.matcher(paymentMethod.getCardNumber()).matches())
            throw new ValidationException(getMessage("payment.method.invalid.card-number"));

        paymentMethod.setExpirationDate(expirationDate);
    }

    private void validatePaymentMethodUpdate(PaymentMethodUpdateRequest paymentMethod) throws ValidationException {

        String expirationDate = validateExpirationDate(paymentMethod.getExpirationDate());

        paymentMethod.setExpirationDate(expirationDate);
    }

    private String validateExpirationDate(String expirationDateInput) throws ValidationException {

        YearMonth expirationDate;
        try {
            expirationDate = YearMonth
                    .parse(expirationDateInput, DateTimeFormatter.ofPattern("MM/yy"));
        } catch (DateTimeParseException ex) {
            throw new ValidationException(getMessage("payment.method.invalid.expiration-date"));
        }

        if (expirationDate.isBefore(YearMonth.now().minusMonths(1)))
            throw new ValidationException(getMessage("payment.method.invalid.expiration-date"));

        int monthValue = expirationDate.getMonth().getValue();

        String year = PAYMENT_METHOD_YEAR_FORMATTER.format(expirationDate);
        String month = monthValue < 10 ? format("%02d" , monthValue) : valueOf(monthValue);

        return format("%s%s", year, month);
    }

    private Organization checkOrganization(PaymentMethodRequest paymentMethod) throws ValidationException {

        final Long organizationId = paymentMethod.getOrganizationId();

        if (!organizationService.existsOrganization(organizationId))
            throw new ValidationException(getMessage("payment.method.invalid.organization"));

        return organizationService.findOrganization(organizationId);
    }

    public Authentication authentication() {
        return SecurityContextHolder.getContext().getAuthentication();
    }

}
