package com.bsdenterprise.qbits_chat_licenses.app.service.mailer;

import com.bsdenterprise.qbits_chat_licenses.app.config.mailer.MailerTemplatesConfig;
import com.bsdenterprise.qbits_chat_licenses.app.dto.mailer.integration.*;
import com.bsdenterprise.qbits_chat_licenses.app.dto.mailer.config.*;
import com.bsdenterprise.qbits_chat_licenses.app.type.mailer.config.*;

import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.env.Environment;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.stereotype.Service;
import org.springframework.http.*;

import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.stream.Stream;
import java.util.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.modelmapper.ModelMapper;

import lombok.Setter;

import static org.springframework.util.StringUtils.isEmpty;

@Slf4j
@Service
public class MailerService {

    @Setter(onMethod = @__(@Autowired))
    private MailerTemplatesConfig mailerTemplatesConfig;

    @Setter(onMethod = @__({@Autowired, @Qualifier("mailerRestTemplate")}))
    private OAuth2RestTemplate mailerRestTemplate;

    @Setter(onMethod = @__(@Autowired))
    private Environment env;

    @Setter(onMethod = @__(@Autowired))
    private ModelMapper modelMapper;

    @Setter(onMethod = @__(@Autowired))
    private ObjectMapper objectMapper;

    private Map<Thread, MailQueueStatus> mailQueues = new HashMap<>();

    public List<MailStatus> sendMailQueue() throws Exception {

        MailQueueStatus mailQueue = getMailQueue(true);

        if (mailQueue == null || CollectionUtils.isEmpty(mailQueue.getMails()))
            return Collections.emptyList();

        String url = String.format("mailer/queue/mail/send/%s", mailQueue.getId());

        ParameterizedTypeReference<List<MailStatus>> typeReference =
                new ParameterizedTypeReference<List<MailStatus>>() {};

        HttpEntity request = new HttpEntity<>(null);

        return mailerRestTemplate
                .exchange(url, HttpMethod.POST, request, typeReference)
                .getBody();
    }

    public void removeMailQueue() {
        getMailQueue(true);
    }

    @SuppressWarnings("unchecked")
    public void enqueueMail(GroupType groupType, MailTemplate template, String subject,
                            MailRecipients mailRecipients, Object payload) {

        withMigration(mailRecipients);

        Map payloadBody = objectMapper.convertValue(payload, Map.class);
        MailQueueStatus mailQueue = getMailQueue(false);

        Group group = mailerTemplatesConfig.getGroups()
                .get(mailQueue.getGroupType() == null ? groupType : mailQueue.getGroupType());
        TemplateReference templateReference = group.getTemplates().get(template);

        String bucket = group.getBucket();
        Sender sender = group.getSender();
        String senderName = isEmpty(mailQueue.getSenderName()) ? sender.getName() : mailQueue.getSenderName();
        String templatePath = String.format("%s/%s", group.getPath(), templateReference.getKey());
        subject = !isEmpty(subject) ? subject : templateReference.getSubject();

        String url = String.format("mailer/queue/mail/%s/%s", bucket, mailQueue.getId());

        UriComponents builtUri = UriComponentsBuilder.fromPath(url)
                .queryParam("templateReference", templatePath).build();

        Mail mail = Mail.builder()
                .subject(subject)
                .senderEmail(sender.getEmail())
                .senderName(senderName)
                .payload(payloadBody)
                .build();

        modelMapper.map(mailRecipients, mail);

        mailQueues.put(currentThread(), enqueueMail(builtUri.toString(), mail));
    }

    public void enqueueMail(GroupType groupType, MailTemplate template,
                            MailRecipients mailRecipients, Object payload) {
        enqueueMail(groupType, template, "", mailRecipients, payload);
    }

    private void withMigration(MailRecipients mailRecipients) {
        if (Boolean.parseBoolean(env.getProperty("app.chat.migration-enabled")))
            mailRecipients.clear();
    }

    private MailQueueStatus enqueueMail(String uri, Mail mail) {

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);

        HttpEntity request = new HttpEntity<>(mail, httpHeaders);

        try {
            log.info("enqueue request: {}", objectMapper.writeValueAsString(request));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return mailerRestTemplate.postForObject(uri, request, MailQueueStatus.class);
    }

    private MailQueueStatus getMailQueue(boolean remove) {

        if (!remove && !mailQueues.containsKey(currentThread())) {

            UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromPath("mailer/queue");
            env().ifPresent(env -> uriBuilder.queryParam("env", env));

            HttpEntity request = new HttpEntity<>(null);

            MailQueueStatus mailQueue =
                    mailerRestTemplate.postForObject(uriBuilder.build().toString(), request, MailQueueStatus.class);

            mailQueues.put(Thread.currentThread(), mailQueue);

            return mailQueue;
        }

        return remove ? mailQueues.remove(currentThread()) : mailQueues.get(currentThread());
    }

    private Optional<String> env() {

        String[] activeProfiles = env.getProperty(
                "qbits-services.active-profile",
                String[].class, env.getActiveProfiles()
        );

        boolean matchEmpty = Stream.of(activeProfiles)
                .anyMatch(ap -> ap.toLowerCase().contains("empty"));

        if (matchEmpty)
            return Optional.empty();

        return Stream.of(activeProfiles).findFirst();
    }

    private Thread currentThread() {
        return Thread.currentThread();
    }

}
