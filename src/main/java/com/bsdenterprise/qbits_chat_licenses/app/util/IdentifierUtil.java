package com.bsdenterprise.qbits_chat_licenses.app.util;

import org.apache.commons.lang3.RandomStringUtils;

import java.util.Random;

public class IdentifierUtil {

    public static String generateRandomIdentifier(int length) {

        char[] chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".toCharArray();

        StringBuilder sb = new StringBuilder();
        Random random = new Random();

        for (int i = 0; i < length; i++)
            sb.append(chars[random.nextInt(chars.length)]);

        return sb.toString();
    }

    public static String generateRandomCode(int count) {
        return RandomStringUtils.randomNumeric(count).toUpperCase();
    }

    public static String generateAlphanumericCode(int count) {
        return RandomStringUtils.randomAlphanumeric(count).toUpperCase();
    }

    public static String attachmentIdentifier(int column) {
        StringBuilder columnName = new StringBuilder();
        while (column > 0) {
            int rem = column % 26;
            if (rem == 0) {
                columnName.append("Z");
                column = (column / 26) - 1;
            }
            else {
                columnName.append((char)((rem - 1) + 'A'));
                column = column / 26;
            }
        }
        return columnName.reverse().toString();
    }

}