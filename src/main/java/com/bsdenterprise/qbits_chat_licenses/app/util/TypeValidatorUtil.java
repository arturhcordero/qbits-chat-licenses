package com.bsdenterprise.qbits_chat_licenses.app.util;

import com.bsdenterprise.qbits_chat_licenses.app.type.info.CodeType;
import com.bsdenterprise.qbits_chat_licenses.app.type.license.LicenseDurationType;
import com.bsdenterprise.qbits_chat_licenses.app.type.license.LicenseProductType;
import com.bsdenterprise.qbits_chat_licenses.app.type.organization.CommunityType;
import com.bsdenterprise.qbits_chat_licenses.app.type.organization.OrganizationType;
import com.bsdenterprise.qbits_chat_licenses.app.type.organization.auth.RoleType;
import com.bsdenterprise.qbits_chat_licenses.app.type.organization.user.GenderReference;
import com.bsdenterprise.qbits_chat_licenses.common.config.exception.model.ValidationException;
import org.springframework.util.StringUtils;

import static com.bsdenterprise.qbits_chat_licenses.common.config.message.MessageProperties.getMessage;

public class TypeValidatorUtil {




    public static GenderReference validateGender(Long genderReferenceId) throws ValidationException {
        return validateGender(genderReferenceId, null, true);
    }

    public static GenderReference validateGender(Long genderReferenceId, boolean validate) throws ValidationException {
        return validateGender(genderReferenceId, null, validate);
    }

    public static GenderReference validateGender(Long genderReferenceId, String code) throws ValidationException {
        return validateGender(genderReferenceId, code, true);
    }

    public static GenderReference validateGender(Long genderReferenceId, String code, boolean validate) throws ValidationException {

        if (StringUtils.isEmpty(code))
            code = "validator.invalid.role";

        GenderReference genderReference = GenderReference.findType(genderReferenceId);

        if (validate && genderReference == null)
            throw new ValidationException(getMessage(code));

        return genderReference;
    }




    public static RoleType validateRole(Long roleId) throws ValidationException {
        return validateRole(roleId, null, true);
    }

    public static RoleType validateRole(Long roleId, boolean validate) throws ValidationException {
        return validateRole(roleId, null, validate);
    }

    public static RoleType validateRole(Long roleId, String code) throws ValidationException {
        return validateRole(roleId, code, true);
    }

    public static RoleType validateRole(Long roleId, String code, boolean validate) throws ValidationException {

        if (StringUtils.isEmpty(code))
            code = "validator.invalid.role";

        RoleType role = RoleType.findRoleType(roleId);

        if (validate && role == null)
            throw new ValidationException(getMessage(code));

        return role;
    }




    public static LicenseDurationType validateLicenseDuration(Long licenseDurationId) throws ValidationException {
        return validateLicenseDuration(licenseDurationId, null, true);
    }

    public static LicenseDurationType validateLicenseDuration(Long licenseDurationId, boolean validate) throws ValidationException {
        return validateLicenseDuration(licenseDurationId, null, validate);
    }

    public static LicenseDurationType validateLicenseDuration(Long licenseDurationId, String code) throws ValidationException {
        return validateLicenseDuration(licenseDurationId, code, true);
    }

    public static LicenseDurationType validateLicenseDuration(Long licenseDurationId, String code, boolean validate) throws ValidationException {

        if (StringUtils.isEmpty(code))
            code = "validator.invalid.license-duration";

        LicenseDurationType licenseDuration = LicenseDurationType.findType(licenseDurationId);

        if (validate && licenseDuration == null)
            throw new ValidationException(getMessage(code));

        return licenseDuration;
    }




    public static LicenseProductType validateLicenseProductType(Long licenseProductTypeId) throws ValidationException {
        return validateLicenseProductType(licenseProductTypeId, null, true);
    }

    public static LicenseProductType validateLicenseProductType(Long licenseProductTypeId, boolean validate) throws ValidationException {
        return validateLicenseProductType(licenseProductTypeId, null, validate);
    }

    public static LicenseProductType validateLicenseProductType(Long licenseProductTypeId, String code) throws ValidationException {
        return validateLicenseProductType(licenseProductTypeId, code, true);
    }

    public static LicenseProductType validateLicenseProductType(Long licenseProductTypeId, String code, boolean validate) throws ValidationException {

        if (StringUtils.isEmpty(code))
            code = "validator.invalid.license-product-type";

        LicenseProductType licenseProductType = LicenseProductType.findType(licenseProductTypeId);

        if (validate && licenseProductType == null)
            throw new ValidationException(getMessage(code));

        return licenseProductType;
    }




    public static OrganizationType validateOrganizationType(Long organizationTypeId) throws ValidationException {
        return validateOrganizationType(organizationTypeId, null, true);
    }

    public static OrganizationType validateOrganizationType(Long organizationTypeId, boolean validate) throws ValidationException {
        return validateOrganizationType(organizationTypeId, null, validate);
    }

    public static OrganizationType validateOrganizationType(Long organizationTypeId, String code) throws ValidationException {
        return validateOrganizationType(organizationTypeId, code, true);
    }

    public static OrganizationType validateOrganizationType(Long organizationTypeId, String code, boolean validate) throws ValidationException {

        if (StringUtils.isEmpty(code))
            code = "validator.invalid.license-product-type";

        OrganizationType organizationType = OrganizationType.findType(organizationTypeId);

        if (validate && organizationType == null)
            throw new ValidationException(getMessage(code));

        return organizationType;
    }




    public static CommunityType validateCommunityType(Integer communityTypeId) throws ValidationException {
        return validateCommunityType(communityTypeId, null, true);
    }

    public static CommunityType validateCommunityType(Integer communityTypeId, boolean validate) throws ValidationException {
        return validateCommunityType(communityTypeId, null, validate);
    }

    public static CommunityType validateCommunityType(Integer communityTypeId, String code) throws ValidationException {
        return validateCommunityType(communityTypeId, code, true);
    }

    public static CommunityType validateCommunityType(Integer communityTypeId, String code, boolean validate) throws ValidationException {

        if (StringUtils.isEmpty(code))
            code = "validator.invalid.license-product-type";

        CommunityType communityType = CommunityType.findType(communityTypeId);

        if (validate && communityType == null)
            throw new ValidationException(getMessage(code));

        return communityType;
    }




    public static CodeType validateCodeType(Integer codeTypeId) throws ValidationException {
        return validateCodeType(codeTypeId, null, true);
    }

    public static CodeType validateCodeType(Integer codeTypeId, boolean validate) throws ValidationException {
        return validateCodeType(codeTypeId, null, validate);
    }

    public static CodeType validateCodeType(Integer codeTypeId, String code) throws ValidationException {
        return validateCodeType(codeTypeId, code, true);
    }

    public static CodeType validateCodeType(Integer codeTypeId, String code, boolean validate) throws ValidationException {

        if (StringUtils.isEmpty(code))
            code = "validator.invalid.license-product-type";

        CodeType codeType = CodeType.findType(codeTypeId);

        if (validate && codeType == null)
            throw new ValidationException(getMessage(code));

        return codeType;
    }

}
