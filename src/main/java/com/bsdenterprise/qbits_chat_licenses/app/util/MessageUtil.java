package com.bsdenterprise.qbits_chat_licenses.app.util;

import lombok.*;

public class MessageUtil {

    @Data @AllArgsConstructor
    public static class SimpleMessage {
        private String message;
    }

    public static SimpleMessage message(String message) {
        return new SimpleMessage(message);
    }

}
