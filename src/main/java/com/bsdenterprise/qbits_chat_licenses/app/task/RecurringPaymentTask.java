package com.bsdenterprise.qbits_chat_licenses.app.task;

import static com.bsdenterprise.qbits_chat_licenses.app.type.order.ProductOrderType.LicenseRenovation;

import com.bsdenterprise.qbits_chat_licenses.app.dto.order.license.OrderDetailRequest;
import com.bsdenterprise.qbits_chat_licenses.app.dto.order.license.UserLicenseAssignmentRequest;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.license.License;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.order.payment.OrderPayment;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.organization.Organization;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.organization.UserOrganization;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.organization.auth.User;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.payment.PaymentMethod;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.repository.license.LicenseRepository;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.order.ProductOrder;
import com.bsdenterprise.qbits_chat_licenses.app.dto.order.ProductOrderRequest;

import com.bsdenterprise.qbits_chat_licenses.app.service.license.LicenseService;
import com.bsdenterprise.qbits_chat_licenses.app.service.order.ProductOrderService;
import com.bsdenterprise.qbits_chat_licenses.app.service.payment.PaymentService;
import com.bsdenterprise.qbits_chat_licenses.app.service.payment_method.PaymentMethodService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import lombok.*;

import java.util.List;

@Component
@Scope("prototype")
public class RecurringPaymentTask implements Runnable {

    @Setter(onMethod = @__(@Autowired))
    private LicenseRepository licenseRepository;

    @Setter(onMethod = @__(@Autowired))
    private ProductOrderService productOrderService;

    @Setter(onMethod = @__(@Autowired))
    private PaymentMethodService paymentMethodService;

    @Setter(onMethod = @__(@Autowired))
    private LicenseService licenseService;

    @Setter(onMethod = @__(@Autowired))
    private PaymentService paymentService;

    @Override
    public void run() {

        List<Organization> organizations = licenseRepository.findOrganizationsWithExpiredLicenses();

        for (Organization organization : organizations) {

            final Long organizationId = organization.getId();

            try {

                final PaymentMethod paymentMethod = paymentMethodService.findDefaultPaymentMethod(organizationId);

                ProductOrderRequest productOrder = new ProductOrderRequest();
                productOrder.setOrderType(LicenseRenovation);
                productOrder.setPaymentMethodId(paymentMethod.getId());

                List<License> licenses = licenseRepository.findExpiredGroupedByProduct(organizationId);

                for (License license : licenses) {

                    UserOrganization userOrganization = license.getUserOrganization();
                    User user = userOrganization.getUser();

                    UserLicenseAssignmentRequest assignment = new UserLicenseAssignmentRequest(userOrganization, user);

                    OrderDetailRequest orderDetail = new OrderDetailRequest();
                    orderDetail.setProductId(license.getLicenseProductId());
                    orderDetail.setQuantity(license.getQuantity());
                    orderDetail.setAssignment(assignment);
                    orderDetail.setOrder(productOrder);
                    orderDetail.setDurationId(license.getDuration().getIdentifier());

                    productOrder.addDetail(orderDetail);
                }

                ProductOrder createdProductOrder = productOrderService.createOrganizationOrder(organization, productOrder);

                licenseService.processOrderLicenses(createdProductOrder);

                OrderPayment orderPayment = paymentService.payOrganizationOrder(createdProductOrder, paymentMethod);

            } catch (Exception ex) {

            }

        }

    }

}

