package com.bsdenterprise.qbits_chat_licenses.app.config.access;

import org.aopalliance.intercept.MethodInvocation;
import org.springframework.security.access.*;
import org.springframework.security.core.*;

import org.springframework.security.core.userdetails.UserDetails;

import java.util.stream.Collectors;
import java.util.*;

public class PermissionVoter implements AccessDecisionVoter<MethodInvocation> {

    @Override
    public boolean supports(ConfigAttribute attribute) {
        return attribute instanceof SecurityAttribute;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return MethodInvocation.class.isAssignableFrom(clazz);
    }

    @Override
    public int vote(Authentication authentication, MethodInvocation object, Collection<ConfigAttribute> attributes) {

        Optional<SecurityAttribute> securityAttribute = attributes.stream()
                .filter(attr -> attr instanceof SecurityAttribute)
                .map(SecurityAttribute.class::cast)
                .findFirst();

        if (!securityAttribute.isPresent())
            return AccessDecisionVoter.ACCESS_ABSTAIN;

        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        PermissionAttribute permissionAttribute = securityAttribute.get().getPermission();

        List<String> roles = permissionAttribute.getAccess().stream()
                .map(PermissionType::name)
                .collect(Collectors.toList());

        List<String> authorities = authentication.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toList());
        boolean hasAccess = authorities.stream().anyMatch(element -> roles.contains(element));

        if (hasAccess)
        	return AccessDecisionVoter.ACCESS_GRANTED;

        return ACCESS_ABSTAIN;

    }
}