package com.bsdenterprise.qbits_chat_licenses.app.config.qbits_pay;

import com.bsdenterprise.qbits_chat_licenses.app.config.qbits_pay.oauth.QBitspayResourceDetails;
import com.bsdenterprise.qbits_chat_licenses.app.config.qbits_pay.oauth.QBitspayTokenProvider;
import com.bsdenterprise.qbits_chat_licenses.common.util.MapperUtil;

import com.bsdenterprise.qbits_chat_licenses.common.util.ObjectUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.*;

import org.springframework.security.oauth2.config.annotation.web.configuration.EnableOAuth2Client;
import org.springframework.security.oauth2.client.token.DefaultAccessTokenRequest;
import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.DefaultOAuth2ClientContext;

import lombok.Setter;

@Configuration
@EnableOAuth2Client
public class OAuthQBitspayConfig {

    @Setter(onMethod = @__(@Autowired))
    private QBitspayConfig qBitspayConfig;

    @Setter(onMethod = @__(@Autowired))
    private ObjectUtil objectUtil;

    @Bean
    public QBitspayResourceDetails qbitsPayResourceDetails() {

        QBitspayResourceDetails details = new QBitspayResourceDetails();
        objectUtil.merge(qBitspayConfig.getOauth(), details);
        objectUtil.merge(qBitspayConfig, details);

        return details;
    }

    @Bean
    public QBitspayTokenProvider qbitspayTokenProvider(QBitspayResourceDetails resourceDetails) {

        QBitspayTokenProvider tokenProvider = new QBitspayTokenProvider();
        tokenProvider.setApiKey(resourceDetails.getApiKey());

        return tokenProvider;
    }

    @Bean
    public OAuth2RestTemplate qbitspayRestTemplate(QBitspayResourceDetails resourceDetails, QBitspayTokenProvider tokenProvider) {

        OAuth2RestTemplate restTemplate = new OAuth2RestTemplate(resourceDetails, qbitsPayClientContext());
        restTemplate.setAccessTokenProvider(tokenProvider);

        return restTemplate;
    }

    @Bean
    @Primary
    public OAuth2ClientContext qbitsPayClientContext() {
        return new DefaultOAuth2ClientContext(new DefaultAccessTokenRequest());
    }

}
