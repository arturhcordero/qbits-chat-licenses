package com.bsdenterprise.qbits_chat_licenses.app.config.cache;

import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import static java.util.Arrays.asList;

@Configuration
@EnableCaching
@Profile({"internal"})
public class CacheConfigLocal {

    @Bean
    public CacheManager cacheManager() {
        return new ConcurrentMapCacheManager(
                "roles",
                "licenseProducts",
                "organizations",
                "features",
                "paymentMethods"
        );
    }

}
