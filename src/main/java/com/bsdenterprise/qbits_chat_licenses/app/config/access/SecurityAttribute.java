package com.bsdenterprise.qbits_chat_licenses.app.config.access;

import java.util.List;
import java.util.stream.Collectors;

import lombok.RequiredArgsConstructor;
import org.springframework.security.access.ConfigAttribute;

@RequiredArgsConstructor
public class SecurityAttribute implements ConfigAttribute {

    private final List<PermissionType> permissions;
    private final PermissionAttribute permissionAttribute;

    @Override
    public String getAttribute() {
        return permissions.stream()
                .map(Enum::name)
                .collect(Collectors.joining(","));
    }

	public PermissionAttribute getPermission() {
		return permissionAttribute;
	}

}