package com.bsdenterprise.qbits_chat_licenses.app.config.validator.password.match;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Documented
@Constraint(validatedBy = PasswordMatchValidator.class)
@Target({ PARAMETER, TYPE })
@Retention(RUNTIME)
public @interface PasswordMatch {

    String message() default "The passwords does not match";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};

}
