package com.bsdenterprise.qbits_chat_licenses.app.config.cache;

import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.cache.CacheProperties;
import org.springframework.boot.autoconfigure.data.redis.RedisProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import org.springframework.data.redis.cache.RedisCacheConfiguration;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.jedis.JedisClientConfiguration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.JdkSerializationRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import redis.clients.jedis.JedisPoolConfig;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.stream.Stream;

@Configuration
@Profile({"live"})
@EnableCaching
@EnableConfigurationProperties({RedisProperties.class, CacheProperties.class})
public class CacheConfigLive {

    @Setter(onMethod = @__(@Autowired))
    private Environment environment;

    private final RedisProperties redisProperties;
    private final CacheProperties cacheProperties;

    public CacheConfigLive(RedisProperties redisProperties, CacheProperties cacheProperties) {
        this.redisProperties = redisProperties;
        this.cacheProperties = cacheProperties;
    }

    @Bean
    public JedisConnectionFactory redisConnectionFactory() {

        String host = Stream.of(environment.getActiveProfiles())
                .filter(profile -> profile.equals("local")).findFirst()
                .map(p -> "localhost")
                .orElse(redisProperties.getHost());

        RedisStandaloneConfiguration redisStandaloneConfiguration = new RedisStandaloneConfiguration();
        redisStandaloneConfiguration.setHostName(host);
        redisStandaloneConfiguration.setPort(redisProperties.getPort());

        JedisClientConfiguration configuration = JedisClientConfiguration.builder()
                .connectTimeout(Duration.of(5, ChronoUnit.MINUTES))
                .readTimeout(Duration.of(10, ChronoUnit.MINUTES))
                .usePooling().poolConfig(poolConfig())
                .build();

        return new JedisConnectionFactory(redisStandaloneConfiguration, configuration);
    }

    @Bean("redisTemplate")
    public RedisTemplate<Object, Object> redisTemplate(RedisConnectionFactory redisConnectionFactory) {

        RedisTemplate<Object, Object> redisTemplate = new RedisTemplate<>();

        redisTemplate.setConnectionFactory(redisConnectionFactory);
        redisTemplate.setKeySerializer(new StringRedisSerializer());
        redisTemplate.setValueSerializer(new JdkSerializationRedisSerializer());

        return redisTemplate;
    }

    @Bean
    public CacheManager cacheManager(RedisConnectionFactory redisConnectionFactory) {

        return RedisCacheManager.builder(redisConnectionFactory)
                .cacheDefaults(RedisCacheConfiguration.defaultCacheConfig()
                        .entryTtl(cacheProperties.getRedis().getTimeToLive()))
                .build();
    }

    private JedisPoolConfig poolConfig() {

        RedisProperties.Pool pool = redisProperties.getJedis().getPool();

        JedisPoolConfig poolConfig = new JedisPoolConfig();

        poolConfig.setMaxTotal(pool.getMaxActive());
        poolConfig.setMaxIdle(pool.getMaxIdle());
        poolConfig.setMinIdle(pool.getMinIdle());
        poolConfig.setBlockWhenExhausted(true);
        poolConfig.setMaxWaitMillis(Duration.of(10, ChronoUnit.MINUTES).getSeconds());

        poolConfig.setTestOnBorrow(true);
        poolConfig.setTestOnReturn(true);
        poolConfig.setTestWhileIdle(false);

        return poolConfig;
    }

}