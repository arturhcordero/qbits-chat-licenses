package com.bsdenterprise.qbits_chat_licenses.app.config.validator.password.match.common;

public interface PasswordChecker {
    boolean checkPasswords();
}
