package com.bsdenterprise.qbits_chat_licenses.app.config.qbits_pay.oauth;

import lombok.Data;

import java.util.List;

import static java.util.stream.Collectors.toList;

public @Data class QBitspayOAuthConfig {

    private String accessTokenUri;
    private String clientId;
    private String clientSecret;
    private List<String> scope;
    private String grantType;
    private String username;
    private String password;

    public List<String> getScope() {
        return scope.stream().map(s -> s.concat(" ")).collect(toList());
    }
}
