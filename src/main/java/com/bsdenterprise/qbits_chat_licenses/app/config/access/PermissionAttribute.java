package com.bsdenterprise.qbits_chat_licenses.app.config.access;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class PermissionAttribute {

	private String country;
	
	private List<PermissionType> access;
}
