package com.bsdenterprise.qbits_chat_licenses.app.config.access;

import org.springframework.security.core.GrantedAuthority;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class PermissionAuthority implements GrantedAuthority {

    public String name;

    @Override
    public String getAuthority() {
        return name;
    }

}
