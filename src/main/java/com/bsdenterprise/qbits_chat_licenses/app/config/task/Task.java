package com.bsdenterprise.qbits_chat_licenses.app.config.task;

import java.util.concurrent.ScheduledFuture;
import lombok.Data;

@Data
public class Task {

    private Runnable runnable;
    private ScheduledFuture scheduledFuture;
    private String cron;

    public Task(Runnable runnable, String cron) {
        this.runnable = runnable;
        this.cron = cron;
    }
}
