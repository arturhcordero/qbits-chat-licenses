package com.bsdenterprise.qbits_chat_licenses.app.config.mailer.oauth;

import lombok.Setter;
import org.apache.http.impl.client.HttpClients;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.boot.context.properties.ConfigurationProperties;

import org.springframework.core.env.Environment;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.security.oauth2.client.DefaultOAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.resource.OAuth2ProtectedResourceDetails;
import org.springframework.security.oauth2.client.token.DefaultAccessTokenRequest;
import org.springframework.security.oauth2.client.token.grant.client.ClientCredentialsResourceDetails;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableOAuth2Client;
import org.springframework.web.util.DefaultUriBuilderFactory;

@Configuration
@EnableOAuth2Client
public class OAuthMailerConfig extends ClientCredentialsResourceDetails {

    @Setter(onMethod = @__(@Autowired))
    private Environment env;

    @Bean
    @ConfigurationProperties(prefix = "mailer.oauth")
    protected OAuth2ProtectedResourceDetails mailerResourceDetails() {
        return new ClientCredentialsResourceDetails();
    }

    @Bean
    public OAuth2RestTemplate mailerRestTemplate() {

        ClientHttpRequestFactory requestFactory = new
                HttpComponentsClientHttpRequestFactory(HttpClients.createDefault());

        OAuth2RestTemplate oAuth2RestTemplate = new OAuth2RestTemplate(
                mailerResourceDetails(), mailerClientContext());

        oAuth2RestTemplate.setRequestFactory(requestFactory);

        oAuth2RestTemplate.setUriTemplateHandler(new DefaultUriBuilderFactory(
                env.getProperty("mailer.url", "")));

        return oAuth2RestTemplate;
    }

    @Bean
    public OAuth2ClientContext mailerClientContext() {
        return new DefaultOAuth2ClientContext(new DefaultAccessTokenRequest());
    }


}
