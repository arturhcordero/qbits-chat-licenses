package com.bsdenterprise.qbits_chat_licenses.app.config.validator.password.policy.common;

public interface PasswordContainer {
    String getPassword();
}
