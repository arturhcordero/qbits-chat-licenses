package com.bsdenterprise.qbits_chat_licenses.app.config.mailer;

import com.bsdenterprise.qbits_chat_licenses.app.dto.mailer.config.Group;
import com.bsdenterprise.qbits_chat_licenses.app.type.mailer.config.GroupType;
import com.bsdenterprise.qbits_chat_licenses.common.config.format.YamlPropertySourceFactory;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import java.util.*;

import lombok.Data;

@Data
@Component
@ConfigurationProperties(prefix = "qbits")
@PropertySource(factory = YamlPropertySourceFactory.class, value = "classpath:templates.yml")
public class MailerTemplatesConfig {

    private Map<GroupType, Group> groups;

}

