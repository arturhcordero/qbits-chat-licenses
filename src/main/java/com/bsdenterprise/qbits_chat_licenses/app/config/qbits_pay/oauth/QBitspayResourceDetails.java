package com.bsdenterprise.qbits_chat_licenses.app.config.qbits_pay.oauth;

import org.springframework.security.oauth2.client.token.grant.password.ResourceOwnerPasswordResourceDetails;

import lombok.*;

@Data
@EqualsAndHashCode(callSuper = true)
public class QBitspayResourceDetails extends ResourceOwnerPasswordResourceDetails {

    private String apiKey;
    private String baseUri;

}
