package com.bsdenterprise.qbits_chat_licenses.app.config.qbits_pay.payment;

import lombok.Data;

public @Data class PaymentConfig {

    private String affiliationNumber;
    private String currencyCode;
    private String fiid;
    private String logicalNetwork;
    private String businessName;
    private String businessCity;
    private String businessStateCode;
    private String businessCountryCode;
    private String businessTypeCode;
    private String saleType;
    private String origin;
    private String placeId;
    private String testJid;

}
