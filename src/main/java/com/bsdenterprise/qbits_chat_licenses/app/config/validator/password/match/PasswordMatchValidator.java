package com.bsdenterprise.qbits_chat_licenses.app.config.validator.password.match;

import com.bsdenterprise.qbits_chat_licenses.app.config.validator.password.match.common.PasswordChecker;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Component
public class PasswordMatchValidator implements ConstraintValidator<PasswordMatch, PasswordChecker> {

    private static final String NOT_MATCH = "The passwords does not match";

    @Override
    public boolean isValid(PasswordChecker container, ConstraintValidatorContext context) {

        boolean match = container.checkPasswords();

        if (!match) {
            context.buildConstraintViolationWithTemplate(NOT_MATCH)
                    .addConstraintViolation()
                    .disableDefaultConstraintViolation();
        }

        return match;
    }
}
