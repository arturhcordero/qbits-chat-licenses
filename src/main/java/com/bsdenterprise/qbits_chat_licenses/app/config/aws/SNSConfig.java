package com.bsdenterprise.qbits_chat_licenses.app.config.aws;

import com.bsdenterprise.qbits_chat_licenses.app.type.notification.PlatformType;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.Map;
import lombok.Data;

@Data
@Component
@ConfigurationProperties("app.aws.sns")
public class SNSConfig {

    private Map<PlatformType, String> platformApplications;

}
