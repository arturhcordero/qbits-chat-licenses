package com.bsdenterprise.qbits_chat_licenses.app.config.openfire;

import lombok.Data;
import org.igniterealtime.restclient.entity.AuthenticationToken;
import org.igniterealtime.restclient.RestApiClient;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.*;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "openfire")
public class OpenfireConfig {

    private String authToken;
    private String apiUrl;
    private Integer apiPort;
    private String jabberDomain;

    @Bean
    public RestApiClient openfireClient() {

        AuthenticationToken token = new AuthenticationToken(authToken);

        return new RestApiClient(apiUrl, apiPort, token);
    }

}
