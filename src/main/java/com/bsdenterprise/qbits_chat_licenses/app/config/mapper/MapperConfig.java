package com.bsdenterprise.qbits_chat_licenses.app.config.mapper;

import com.bsdenterprise.qbits_chat_licenses.app.dto.order.info.ProductOrderInfo;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.order.ProductOrder;
import com.bsdenterprise.qbits_chat_licenses.common.util.MapperUtil;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MapperConfig {

    @Bean("commonMapper")
    public MapperUtil commonMapper(ModelMapper modelMapper) {
        MapperUtil mapperUtil = new MapperUtil();
        mapperUtil.setModelMapper(modelMapper);
        return mapperUtil;
    }

    @Bean("productOrderMapper")
    public MapperUtil productOrderMapper() {

        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setAmbiguityIgnored(true);

        modelMapper.addMappings(new PropertyMap<ProductOrder, ProductOrderInfo>() {
            protected void configure() {
                skip(destination.getOrderPayment());
                skip(destination.getOrganization());
            }
        });

        /*
        modelMapper.addMappings(new PropertyMap<LicenseProduct, LicenseProductInfo>() {
            protected void configure() {
                skip(destination.getCost());
                skip(destination.getFeatures());
            }
        });
        */

        MapperUtil mapperUtil = new MapperUtil();
        mapperUtil.setModelMapper(modelMapper);

        return mapperUtil;
    }

}
