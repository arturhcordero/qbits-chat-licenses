package com.bsdenterprise.qbits_chat_licenses.app.config.validator.cellphone.common;

public interface PhoneNumber {
    String getPhone();
    String getPhoneRegion();
}
