package com.bsdenterprise.qbits_chat_licenses.app.config.validator.cellphone;

import com.bsdenterprise.qbits_chat_licenses.app.config.validator.cellphone.common.PhoneNumber;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;

import com.google.i18n.phonenumbers.Phonenumber;
import org.springframework.util.StringUtils;
import javax.validation.*;

import static org.springframework.util.StringUtils.isEmpty;

public class PhoneNumberValidator implements ConstraintValidator<Phone, PhoneNumber> {

    @Override
    public void initialize(Phone constraintAnnotation) {

    }

    @Override
    public boolean isValid(PhoneNumber phoneNumber, ConstraintValidatorContext context) {

        if (phoneNumber == null || isEmpty(phoneNumber.getPhone()) || isEmpty(phoneNumber.getPhoneRegion()))
            return true/*false*/;

        try {

            PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();
            Phonenumber.PhoneNumber parsedNumber = phoneNumberUtil
                    .parse(phoneNumber.getPhone(), phoneNumber.getPhoneRegion());

            //return phoneNumberUtil.isValidNumber(parsedNumber);
            return true;

        } catch (NumberParseException ex) {
            return false;
        }

    }
}
