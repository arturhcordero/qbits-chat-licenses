package com.bsdenterprise.qbits_chat_licenses.app.config.validator.password.policy.common;

import java.util.List;

public interface InvalidPasswords {
    List<PasswordContainer> findAll();
}
