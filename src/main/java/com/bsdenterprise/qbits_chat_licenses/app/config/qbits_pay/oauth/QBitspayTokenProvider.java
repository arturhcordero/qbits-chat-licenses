package com.bsdenterprise.qbits_chat_licenses.app.config.qbits_pay.oauth;

import org.springframework.http.HttpHeaders;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.oauth2.client.resource.*;
import org.springframework.security.oauth2.client.token.AccessTokenRequest;
import org.springframework.security.oauth2.client.token.grant.password.*;
import org.springframework.security.oauth2.common.*;

import org.springframework.util.*;

import java.util.List;
import lombok.Setter;

public class QBitspayTokenProvider extends ResourceOwnerPasswordAccessTokenProvider {

    @Setter
    private String apiKey;

    @Override
    public OAuth2AccessToken obtainAccessToken(OAuth2ProtectedResourceDetails details, AccessTokenRequest request)
            throws UserRedirectRequiredException, AccessDeniedException, OAuth2AccessDeniedException {

        ResourceOwnerPasswordResourceDetails resource = (ResourceOwnerPasswordResourceDetails)details;
        HttpHeaders headers = new HttpHeaders();

        headers.add("X-API-KEY", apiKey);

        return retrieveToken(request, resource, getParametersForTokenRequest(resource, request), headers);
    }

    @Override
    public OAuth2AccessToken refreshAccessToken(OAuth2ProtectedResourceDetails resource,
                                                OAuth2RefreshToken refreshToken,
                                                AccessTokenRequest request)
            throws UserRedirectRequiredException, OAuth2AccessDeniedException {

        MultiValueMap<String, String> form = new LinkedMultiValueMap<>();
        form.add("grant_type", "refresh_token");
        form.add("refresh_token", refreshToken.getValue());

        HttpHeaders headers = new HttpHeaders();
        headers.add("X-API-KEY",apiKey);

        return retrieveToken(request, resource, form, headers);
    }

    private MultiValueMap<String, String> getParametersForTokenRequest(ResourceOwnerPasswordResourceDetails resource,
                                                                       AccessTokenRequest request) {
        MultiValueMap<String, String> form = new LinkedMultiValueMap<>();

        form.set("grant_type", "password");
        form.set("username", resource.getUsername());
        form.set("password", resource.getPassword());
        form.putAll(request);

        if (resource.isScoped()) {

            StringBuilder builder = new StringBuilder();
            List<String> scope = resource.getScope();

            if (!CollectionUtils.isEmpty(scope))
                scope.forEach(builder::append);

            form.set("scope", builder.toString());
        }

        return form;
    }
}
