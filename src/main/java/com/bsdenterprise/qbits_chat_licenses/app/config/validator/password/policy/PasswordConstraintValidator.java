package com.bsdenterprise.qbits_chat_licenses.app.config.validator.password.policy;

import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.validation.InvalidPassword;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.repository.validation.InvalidPasswordRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import org.passay.*;
import org.passay.dictionary.WordListDictionary;
import org.passay.dictionary.WordLists;
import org.passay.dictionary.sort.ArraysSort;

import java.util.stream.Collectors;

import javax.validation.*;
import java.util.*;
import java.io.*;

import lombok.Setter;

@Component
public class PasswordConstraintValidator implements ConstraintValidator<ValidPassword, String> {

    @Setter(onMethod = @__(@Autowired))
    private InvalidPasswordRepository invalidPasswordRepository;

    private DictionaryRule dictionaryRule;

    @Override
    public void initialize(ValidPassword constraintAnnotation) {
        dictionaryRule = dictionaryRule();
    }

    private DictionaryRule dictionaryRule() {

        DictionaryRule dictionaryRule = null;

        try {

            String words = invalidPasswordRepository.findAll().stream()
                    .map(InvalidPassword::getPassword)
                    .collect(Collectors.joining("\n"));

            dictionaryRule = new DictionaryRule(
                    new WordListDictionary(WordLists.createFromReader(
                            new BufferedReader[] {
                                    new BufferedReader(new StringReader(words))
                            },
                            false,
                            new ArraysSort()
                    ))
            );

        } catch (IOException ignored) { }

        return dictionaryRule;
    }

    private List<Rule> rules() {

        return Arrays.asList(

                // at least 8 characters
                new LengthRule(8, 20),

                // at least one lower-case character
                new CharacterRule(EnglishCharacterData.LowerCase, 1),

                // at least one digit character
                new CharacterRule(EnglishCharacterData.Digit, 1),

                // at least one symbol (special character)
                new CharacterRule(EnglishCharacterData.Special, 1),

                // no whitespace
                new WhitespaceRule(),

                dictionaryRule
        );

    }

    @Override
    public boolean isValid(String password, ConstraintValidatorContext context) {

        List<Rule> rules = rules();

        PasswordValidator passwordValidator = new PasswordValidator(rules);

        RuleResult result = passwordValidator.validate(new PasswordData(password));

        if (result.isValid())
            return true;

        List<String> messages = passwordValidator.getMessages(result);
        String messageTemplate = String.join(",", messages);

        context.buildConstraintViolationWithTemplate(messageTemplate)
                .addConstraintViolation()
                .disableDefaultConstraintViolation();

        return false;
    }
}
