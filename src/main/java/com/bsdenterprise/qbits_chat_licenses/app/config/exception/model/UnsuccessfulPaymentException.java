package com.bsdenterprise.qbits_chat_licenses.app.config.exception.model;

import com.bsdenterprise.qbits_chat_licenses.common.config.exception.model.ValidationException;

public class UnsuccessfulPaymentException extends ValidationException {

    public UnsuccessfulPaymentException(String message, Object... args) {
        super(message, args);
    }

}
