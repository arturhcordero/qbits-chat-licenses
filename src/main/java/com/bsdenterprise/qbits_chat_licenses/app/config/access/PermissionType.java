package com.bsdenterprise.qbits_chat_licenses.app.config.access;

public enum PermissionType {

    ManageUsersLicenses,
    UpdateLicense,
    ManagePaymentMethods,
    ReadBillingInformation,
    ChangePassword,
    CancellMembership,
    SendSaleInvitation

}