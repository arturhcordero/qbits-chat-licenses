package com.bsdenterprise.qbits_chat_licenses.app.config.qbits_pay;

import com.bsdenterprise.qbits_chat_licenses.app.config.qbits_pay.oauth.QBitspayOAuthConfig;
import com.bsdenterprise.qbits_chat_licenses.app.config.qbits_pay.payment.PaymentConfig;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;

@Component
@ConfigurationProperties(prefix = "qbitspay")
public @Data class QBitspayConfig {

    private PaymentConfig payment;
    private QBitspayOAuthConfig oauth;

    private String apiKey;
    private String baseUri;
    private boolean enabled;

}
