package com.bsdenterprise.qbits_chat_licenses.app.persistence.model.code;

import com.bsdenterprise.qbits_chat_licenses.common.persistence.model.BaseEntity;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.organization.auth.User;
import com.bsdenterprise.qbits_chat_licenses.app.type.info.CodeStatus;

import javax.persistence.*;
import lombok.*;

@Entity
@EqualsAndHashCode(callSuper = true)
public @Data class QRCode extends BaseEntity {

    @Column(name = "email", length = 100)
    private String email;

    @Column(name = "content", length = 20, nullable = false, unique = true)
    private String code;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private CodeStatus status;

    @Column(name = "configuration")
    private Integer configuration;

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    private User user;

    @Column(name = "user_id", insertable = false, updatable = false)
    private Long userId;

    @ManyToOne
    @JoinColumn(name = "to_user_id", referencedColumnName = "id")
    private User toUser;

    @Column(name = "to_user_id", insertable = false, updatable = false)
    private Long toUserId;

}
