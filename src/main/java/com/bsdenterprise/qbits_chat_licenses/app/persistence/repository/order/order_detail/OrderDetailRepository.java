package com.bsdenterprise.qbits_chat_licenses.app.persistence.repository.order.order_detail;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.bsdenterprise.qbits_chat_licenses.common.persistence.repository.SoftDeleteRepository;

import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.order.OrderDetail;

import java.util.Optional;

@Repository
public interface OrderDetailRepository extends SoftDeleteRepository<OrderDetail, Long>, OrderDetailRepositoryCustom {

    @Modifying
    @Query("update OrderDetail od set od.licenseId = :licenseId where od.id = :orderDetailId")
    void setOrderDetailLicense(@Param("licenseId") Long licenseId,
                               @Param("orderDetailId") Long orderDetailId);

}
