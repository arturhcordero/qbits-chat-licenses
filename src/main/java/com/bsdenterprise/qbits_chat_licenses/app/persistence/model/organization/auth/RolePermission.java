package com.bsdenterprise.qbits_chat_licenses.app.persistence.model.organization.auth;

import com.bsdenterprise.qbits_chat_licenses.common.persistence.model.BaseEntity;

import javax.persistence.*;
import lombok.*;

@Entity
@EqualsAndHashCode(callSuper = true)
public @Data class RolePermission extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "role_id", referencedColumnName = "id", nullable = false)
    private Role role;

    @Column(name = "role_id", nullable = false, insertable = false, updatable = false)
    private Long roleId;

    @ManyToOne
    @JoinColumn(name = "permission_id", referencedColumnName = "id", nullable = false)
    private Permission permission;

    @Column(name = "permission_id", nullable = false, insertable = false, updatable = false)
    private Long permissionId;

    public RolePermission(Role role, Permission permission) {
        this.role = role;
        this.permission = permission;
    }

}
