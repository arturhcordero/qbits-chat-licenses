package com.bsdenterprise.qbits_chat_licenses.app.persistence.repository.product;

import org.springframework.stereotype.Repository;
import com.bsdenterprise.qbits_chat_licenses.common.persistence.repository.SoftDeleteRepository;

import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.license.product.Tax;

import java.util.Optional;

@Repository
public interface TaxRepository extends SoftDeleteRepository<Tax, Long> {

    Optional<Tax> findByCode(String code);

}
