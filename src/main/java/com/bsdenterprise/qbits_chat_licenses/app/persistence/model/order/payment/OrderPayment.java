package com.bsdenterprise.qbits_chat_licenses.app.persistence.model.order.payment;

import com.bsdenterprise.qbits_chat_licenses.app.type.order.OrderPaymentType;
import com.bsdenterprise.qbits_chat_licenses.common.persistence.model.BaseEntity;

import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.order.ProductOrder;

import javax.persistence.*;
import lombok.*;
import org.springframework.util.StringUtils;

import static com.bsdenterprise.qbits_chat_licenses.common.util.Constants.SUCESSFUL_PAYMENT_CODE;

@Entity
@EqualsAndHashCode(callSuper = true)
public @Data class OrderPayment extends BaseEntity {

    @Column(name = "alias", length = 50)
    private String alias;

    @Column(name = "transaction_identifier", length = 200)
    private String transactionIdentifier;

    @Column(name = "transaction_date", length = 10)
    private String transactionDate;

    @Column(name = "affiliation_number", length = 40)
    private String affiliationNumber;

    @Column(name = "authorization_code", length = 10)
    private String authorizationCode;

    @Column(name = "response_code", length = 30)
    private String responseCode;

    @Column(name = "prosa_code", length = 6)
    private String prosaCode;

    @Column(name = "trace_number", length = 30)
    private String traceNumber;

    @Column(name = "message")
    private String message;

    @Column(name = "amount", length = 100)
    private String amount;

    @Column(name = "bin", length = 20)
    private String bin;

    @Enumerated(EnumType.STRING)
    @Column(name = "type", length = 30)
    private OrderPaymentType type;

    @OneToOne(mappedBy="orderPayment", fetch = FetchType.LAZY)
    private ProductOrder productOrder;

    public boolean isApproved() {
        if (StringUtils.isEmpty(prosaCode))
            return false;
        return prosaCode.equals(SUCESSFUL_PAYMENT_CODE);
    }

}
