package com.bsdenterprise.qbits_chat_licenses.app.persistence.repository.license;

import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.license.UserLicenseAssignment;
import com.bsdenterprise.qbits_chat_licenses.common.persistence.repository.SoftDeleteRepository;

import org.springframework.stereotype.Repository;

@Repository
public interface UserLicenseAssignmentRepository extends SoftDeleteRepository<UserLicenseAssignment, Long> {

}
