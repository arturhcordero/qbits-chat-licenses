package com.bsdenterprise.qbits_chat_licenses.app.persistence.repository.validation;

import org.springframework.stereotype.Repository;
import com.bsdenterprise.qbits_chat_licenses.common.persistence.repository.SoftDeleteRepository;

import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.validation.InvalidPassword;

@Repository
public interface InvalidPasswordRepository extends SoftDeleteRepository<InvalidPassword, Long> {



}
