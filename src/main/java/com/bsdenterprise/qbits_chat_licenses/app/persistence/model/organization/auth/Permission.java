package com.bsdenterprise.qbits_chat_licenses.app.persistence.model.organization.auth;

import javax.persistence.*;

import com.bsdenterprise.qbits_chat_licenses.common.persistence.model.BaseEntity;
import lombok.*;

import java.util.Collection;

@Entity
@NoArgsConstructor
@RequiredArgsConstructor
@EqualsAndHashCode(callSuper = true)
public @Data class Permission extends BaseEntity {

    @Column(name = "name", nullable = false, length = 100)
    private @NonNull String name;

    @OneToMany(mappedBy = "permission")
    private Collection<RolePermission> rolePermissions;

}
