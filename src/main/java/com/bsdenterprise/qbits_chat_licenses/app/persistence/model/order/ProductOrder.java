package com.bsdenterprise.qbits_chat_licenses.app.persistence.model.order;

import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.organization.Organization;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.organization.auth.User;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.order.payment.OrderPayment;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.payment.PaymentMethod;
import com.bsdenterprise.qbits_chat_licenses.app.type.order.ProductOrderStatus;
import com.bsdenterprise.qbits_chat_licenses.app.type.order.ProductOrderType;
import com.bsdenterprise.qbits_chat_licenses.common.persistence.model.BaseEntity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;
import lombok.*;
import org.springframework.util.CollectionUtils;

import static java.math.BigDecimal.ZERO;

@Entity
@EqualsAndHashCode(callSuper = true)
public @Data class ProductOrder extends BaseEntity {

    @Column(name = "total", nullable = false)
    private BigDecimal total = ZERO;

    @Enumerated(EnumType.STRING)
    @Column(name = "status", nullable = false, length = 50)
    private ProductOrderStatus status;

    @Enumerated(EnumType.STRING)
    @Column(name = "type", nullable = false, length = 50)
    private ProductOrderType type;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="order_payment_id")
    private OrderPayment orderPayment;

    @Column(name = "order_payment_id", insertable = false, updatable = false)
    private Long paymentId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "salesman_id", referencedColumnName = "id")
    private User salesmanUser;

    @Column(name = "salesman_id", insertable = false, updatable = false)
    private Long salesmanId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    private User user;

    @Column(name = "user_id", insertable = false, updatable = false)
    private Long userId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "organization_id", referencedColumnName = "id")
    private Organization organization;

    @Column(name = "organization_id", insertable = false, updatable = false)
    private Long organizationId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "payment_method_id", referencedColumnName = "id")
    private PaymentMethod paymentMethod;

    @Column(name = "payment_method_id", insertable = false, updatable = false)
    private Long paymentMethodId;

    @OneToMany(mappedBy = "productOrder", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<OrderDetail> orderDetails = new ArrayList<>();

    public boolean isEmpty() {
        return CollectionUtils.isEmpty(orderDetails);
    }

    public void addDetail(OrderDetail detail) {
        orderDetails.add(detail);
        detail.setProductOrder(this);
        calculateTotal();
    }

    public BigDecimal calculateTotal() {
        total = orderDetails
                .stream()
                .map(OrderDetail::getSubtotal)
                .reduce(ZERO, BigDecimal::add);
        return total;
    }

}
