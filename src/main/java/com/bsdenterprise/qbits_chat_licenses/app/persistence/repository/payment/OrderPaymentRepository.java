package com.bsdenterprise.qbits_chat_licenses.app.persistence.repository.payment;

import org.springframework.stereotype.Repository;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.order.payment.OrderPayment;

import com.bsdenterprise.qbits_chat_licenses.common.persistence.repository.SoftDeleteRepository;

@Repository
public interface OrderPaymentRepository extends SoftDeleteRepository<OrderPayment, Long> {



}
