package com.bsdenterprise.qbits_chat_licenses.app.persistence.model.organization;

import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.organization.auth.Role;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.organization.auth.UserOrganizationRole;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.organization.auth.User;
import com.bsdenterprise.qbits_chat_licenses.app.type.organization.UserOrganizationStatusType;
import com.bsdenterprise.qbits_chat_licenses.common.persistence.model.BaseEntity;

import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.license.License;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.springframework.util.CollectionUtils;

import static org.springframework.util.CollectionUtils.isEmpty;

@Entity
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString(exclude = {"userOrganizationRoles","license"})
public @Data class UserOrganization extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id", nullable = false)
    private User user;

    /*
    @Column(name = "user_id", nullable = false, insertable = false, updatable = false)
    private Long userId;
    */

    @ManyToOne
    @JoinColumn(name = "organization_id", referencedColumnName = "id", nullable = false)
    private Organization organization;

    @Column(name = "organization_id", nullable = false, insertable = false, updatable = false)
    private Long organizationId;

    @Enumerated(EnumType.STRING)
    @Column(name = "status", nullable = false, length = 45)
    private UserOrganizationStatusType status;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "license_id")
    private License license;

    @Column(name = "license_id", insertable = false, updatable = false)
    private Long licenseId;

    @OneToMany(mappedBy = "userOrganization", cascade = CascadeType.ALL)
    private List<UserOrganizationRole> userOrganizationRoles = new ArrayList<>();

    @Transient
    @JsonIgnore
    private Role mainRole;

    public UserOrganization(User user, Organization organization) {
        this.user = user;
        this.organization = organization;
    }

    public void addUserOrganizationRole(UserOrganizationRole userOrganizationRole) {
        userOrganizationRole.setUserOrganization(this);
        userOrganizationRoles.add(userOrganizationRole);
    }

    public boolean hasLicense() {
        return license != null || licenseId != null;
    }

    public void establishMainRole(Role role) {

        List<UserOrganizationRole> userOrganizationRoles = getUserOrganizationRoles();

        if (!isEmpty(userOrganizationRoles)) {
            userOrganizationRoles.stream().findFirst()
                    .ifPresent(uor -> uor.setRole(role));
        }

    }

    public Role getMainRole() {

        if (mainRole != null) return mainRole;

        mainRole = getUserOrganizationRoles().stream()
                .map(UserOrganizationRole::getRole)
                .findFirst().orElse(null);

        return mainRole;
    }

}
