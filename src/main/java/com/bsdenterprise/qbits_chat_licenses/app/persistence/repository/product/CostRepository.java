package com.bsdenterprise.qbits_chat_licenses.app.persistence.repository.product;

import org.springframework.stereotype.Repository;
import com.bsdenterprise.qbits_chat_licenses.common.persistence.repository.SoftDeleteRepository;

import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.license.product.Cost;

@Repository
public interface CostRepository extends SoftDeleteRepository<Cost, Long> {

}
