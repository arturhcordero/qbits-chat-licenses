package com.bsdenterprise.qbits_chat_licenses.app.persistence.model.organization;

import com.bsdenterprise.qbits_chat_licenses.common.persistence.model.BaseEntity;

import javax.persistence.*;
import lombok.*;

@Entity
@EqualsAndHashCode(callSuper = true)
public @Data class Address extends BaseEntity {

    @Column(name = "address")
    private String address;

    @Column(name = "state")
    private String state;

    @Column(name = "city")
    private String city;

    @Column(name = "zip_code")
    private String zipCode;

}
