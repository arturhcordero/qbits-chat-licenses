package com.bsdenterprise.qbits_chat_licenses.app.persistence.repository.invitation;

import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.invitation.SaleInvitation;
import com.bsdenterprise.qbits_chat_licenses.common.persistence.repository.SoftDeleteRepository;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Page;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface SaleInvitationRepository extends SoftDeleteRepository<SaleInvitation, Long> {

    @Query("select si from SaleInvitation si " +
            "where si.email = :email " +
            "and si.userId = :userId " +
            "and si.deleted = false")
    Optional<SaleInvitation> findInvitation(@Param("email") String email,
                                            @Param("userId") Long userId);

    @Modifying(clearAutomatically = true)
    @Query("update SaleInvitation s " +
            "set s.modifiedAt = current_timestamp," +
                "s.status = 'Forwarded' " +
            "where s.id = :saleInvitationId")
    void forwardSaleInvitation(@Param("saleInvitationId") Long saleInvitationId);

    @Modifying(clearAutomatically = true)
    @Query("update SaleInvitation s " +
            "set s.modifiedAt = current_timestamp," +
            "s.status = 'Cancelled' " +
            "where s.id = :saleInvitationId")
    void cancelSaleInvitation(@Param("saleInvitationId") Long saleInvitationId);

    @Modifying(clearAutomatically = true)
    @Query("update SaleInvitation s " +
            "set s.modifiedAt = current_timestamp," +
            "s.status = 'Open' " +
            "where s.id = :saleInvitationId")
    void openSaleInvitation(@Param("saleInvitationId") Long saleInvitationId);

}
