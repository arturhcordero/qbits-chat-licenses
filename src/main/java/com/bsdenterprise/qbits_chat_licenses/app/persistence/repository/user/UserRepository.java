package com.bsdenterprise.qbits_chat_licenses.app.persistence.repository.user;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.bsdenterprise.qbits_chat_licenses.common.persistence.repository.SoftDeleteRepository;

import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.organization.auth.User;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Repository
public interface UserRepository extends SoftDeleteRepository<User, Long> {

    @Query("select u.email from User u " +
            "where u.confirmationToken = :token and u.enabled = false")
    Optional<String> findEmailByToken(@Param("token") String token);

    Optional<User> findByConfirmationTokenAndEnabledFalse(String token);

    boolean existsByForgotPasswordToken(String token);

    @Query("select new User(u.email, u.jid, u.name, u.lastName, u.secondLastName) " +
            "from User u where u.forgotPasswordToken = :token")
    User findSimpleUser(@Param("token") String token);

    Optional<User> findByJid(String jid);

    Optional<User> findByEmail(String email);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("update User u set u.jid = :jid where u.id = :userId")
    void assignJabberId(@Param("jid") String jid, @Param("userId") Long userId);

    @Modifying(clearAutomatically = true)
    @Query("update User u set u.forgotPasswordToken = :token where u.email = :email")
    void assignPasswordConfirmationToken(@Param("token") String token, @Param("email") String email);

    boolean existsByEmail(String email);

    @Modifying(clearAutomatically = true)
    @Query("update User u set u.password = :password where u.id = :userId")
    void changePassword(@Param("password") String password, @Param("userId") Long userId);

    @Modifying(clearAutomatically = true)
    @Query("update User u set u.password = :password where u.forgotPasswordToken = :token")
    void changePassword(@Param("password") String password, @Param("token") String token);

}
