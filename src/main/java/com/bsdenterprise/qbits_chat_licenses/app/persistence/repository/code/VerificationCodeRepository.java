package com.bsdenterprise.qbits_chat_licenses.app.persistence.repository.code;

import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.code.VerificationCode;

import com.bsdenterprise.qbits_chat_licenses.common.persistence.repository.SoftDeleteRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface VerificationCodeRepository extends SoftDeleteRepository<VerificationCode, Long> {

    default boolean exists(String code, String email) {
        return existsByCodeAndEmail(code, email);
    }

    boolean existsByCodeAndEmail(String code, String email);

    @Query("select case when count(vc) > 0 then true else false end " +
            "from VerificationCode vc where vc.email = :email and vc.code = :code and vc.status = 'Created'")
    boolean canVerifyCode(@Param("email") String email, @Param("code") String code);

    @Modifying(clearAutomatically = true)
    @Query("update VerificationCode vc set vc.status = 'Used' where vc.code = :code")
    void markCodeAsUsed(@Param("code") String code);

}
