package com.bsdenterprise.qbits_chat_licenses.app.persistence.model.notification;

import com.bsdenterprise.qbits_chat_licenses.common.persistence.model.BaseEntity;

import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.organization.auth.User;
import com.bsdenterprise.qbits_chat_licenses.app.type.notification.*;

import javax.persistence.*;
import lombok.*;

@Data
@Entity
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class DeviceToken extends BaseEntity {

    @Column(name = "token")
    private String token;

    @Column(name = "platform_endpoint_arn")
    private String platformEndpointArn;

    @Enumerated(EnumType.STRING)
    private DeviceType type;

    @Enumerated(EnumType.STRING)
    private PlatformType platformType;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    private User user;

    @Column(name = "user_id", insertable = false, updatable = false)
    private Long userId;

    public DeviceToken(String token, DeviceType type, PlatformType platformType) {
        this.token = token;
        this.type = type;
        this.platformType = platformType;
    }

}
