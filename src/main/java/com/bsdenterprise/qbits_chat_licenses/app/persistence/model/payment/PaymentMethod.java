package com.bsdenterprise.qbits_chat_licenses.app.persistence.model.payment;

import com.bsdenterprise.qbits_chat_licenses.common.persistence.model.BaseEntity;

import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.organization.Organization;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.order.ProductOrder;

import java.util.List;

import javax.persistence.*;

import lombok.*;
import org.springframework.util.StringUtils;

import static com.bsdenterprise.qbits_chat_licenses.common.util.Constants.UNDEFINED;

@Entity
@EqualsAndHashCode(callSuper = true)
public @Data class PaymentMethod extends BaseEntity {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "organization_id", referencedColumnName = "id", nullable = false)
    private Organization organization;

    @Column(name = "organization_id", nullable = false, insertable = false, updatable = false)
    private Long organizationId;

    @Column(name = "jid_uuid", nullable = false)
    private String jidUuid;

    @Column(name = "alias", nullable = false, length = 60)
    private String alias;

    @Column(name = "agreement", nullable = false, length = 100)
    private String agreement;

    @Column(name = "customer_name", nullable = false, length = 60)
    private String customerName;

    @Column(name = "last_digits", length = 4, nullable = false)
    private String lastDigits;

    @Column(name = "expiration_date", length = 6)
    private String expirationDate;

    @Column(name = "bank", length = 40)
    private String bank;

    @Column(name = "brand", length = 40)
    private String brand;

    @Column(name = "type", length = 20)
    private Integer type;

    @Column(name = "default_payment", nullable = false)
    private boolean defaultPayment;

    @OneToMany(mappedBy = "orderPayment")
    private List<ProductOrder> productOrders;

    public void setBank(String bank) {
        this.bank = StringUtils.isEmpty(bank) ? UNDEFINED : bank;
    }

    public void setBrand(String brand) {
        this.brand = StringUtils.isEmpty(brand) ? UNDEFINED : brand;
    }

    public void setType(Integer type) {
        this.type = type == null ? 1 : type;
    }

}
