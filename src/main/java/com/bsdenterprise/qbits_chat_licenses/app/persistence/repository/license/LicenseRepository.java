package com.bsdenterprise.qbits_chat_licenses.app.persistence.repository.license;

import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.organization.Organization;
import com.bsdenterprise.qbits_chat_licenses.common.persistence.repository.SoftDeleteRepository;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.license.License;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface LicenseRepository extends SoftDeleteRepository<License, Long> {

    Page<License> findByOrganizationIdAndDeletedFalse(@Param("organizationId") Long organizationId, Pageable pageable);

    @Query("select " +
                "new License(count(l.id), l) from License l " +
            "where l.organizationId = :organizationId " +
                "and l.userOrganizationId is null " +
                "and l.deleted = false " +
            "group by l.licenseProductId")
    List<License> findGroupedByProduct(@Param("organizationId") Long organizationId);

    @Query("select " +
                "new License(count(l.id), l) from License l " +
            "where l.organizationId = :organizationId " +
                "and l.userOrganizationId is null " +
                "and l.deleted = false " +
                "and l.expiration >= current_timestamp " +
            "group by l.licenseProductId")
    List<License> findExpiredGroupedByProduct(@Param("organizationId") Long organizationId);

    @Query("select " +
                "new License(count(l.id), l) from License l " +
            "where l.deleted = false " +
                "and l.expiration >= current_timestamp " +
            "group by l.organizationId ")
    List<Organization> findOrganizationsWithExpiredLicenses();

    @Modifying(clearAutomatically = true)
    @Query("update License l set l.expiration = :expiration where l.id = :licenseId")
    void updateExpiration(@Param("expiration") LocalDateTime expiration,
                        @Param("licenseId") Long licenseId);

}
