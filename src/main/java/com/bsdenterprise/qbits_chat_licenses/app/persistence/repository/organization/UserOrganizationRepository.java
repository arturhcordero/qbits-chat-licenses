package com.bsdenterprise.qbits_chat_licenses.app.persistence.repository.organization;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Repository;
import com.bsdenterprise.qbits_chat_licenses.common.persistence.repository.SoftDeleteRepository;

import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.organization.UserOrganization;

@Repository
public interface UserOrganizationRepository extends SoftDeleteRepository<UserOrganization, Long> {


}
