package com.bsdenterprise.qbits_chat_licenses.app.persistence.model.organization.auth;

import com.bsdenterprise.qbits_chat_licenses.common.persistence.model.BaseEntity;

import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.organization.UserOrganization;

import javax.persistence.*;
import lombok.*;

@Entity
@EqualsAndHashCode(callSuper = true)
@ToString(exclude = {"userOrganization"})
public @Data class UserOrganizationRole extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "organization_user_id", referencedColumnName = "id", nullable = false)
    private UserOrganization userOrganization;

    @Column(name = "organization_user_id", nullable = false, insertable = false, updatable = false)
    private Long organizationUserId;

    @ManyToOne
    @JoinColumn(name = "role_id", referencedColumnName = "id", nullable = false)
    private Role role;

    @Column(name = "role_id", nullable = false, insertable = false, updatable = false)
    private Long roleId;

}
