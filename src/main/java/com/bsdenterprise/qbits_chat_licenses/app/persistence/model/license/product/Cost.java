package com.bsdenterprise.qbits_chat_licenses.app.persistence.model.license.product;

import static com.bsdenterprise.qbits_chat_licenses.app.type.license.LicenseDurationType.*;
import com.bsdenterprise.qbits_chat_licenses.app.type.license.LicenseDurationType;
import com.bsdenterprise.qbits_chat_licenses.common.persistence.model.BaseEntity;

import java.math.BigDecimal;

import static java.math.BigDecimal.ZERO;
import static java.math.RoundingMode.*;

import javax.persistence.*;
import lombok.*;

@Entity
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public @Data class Cost extends BaseEntity {

    @Column(name = "value", nullable = false, precision = 19, scale = 12)
    private BigDecimal value;

    @ManyToOne
    @JoinColumn(name = "tax_id")
    private Tax tax;

    public Cost(BigDecimal value, Tax tax) {
        this.value = value;
        this.tax = tax;
    }

    public BigDecimal getPrice(LicenseDurationType duration) {
        return getPrice(duration, 0);
    }

    public BigDecimal getPrice(LicenseDurationType duration, long daysDifference) {

        BigDecimal daysDifferenceCost = value.multiply(BigDecimal.valueOf(daysDifference));

        if (daysDifferenceCost.compareTo(ZERO) > 0)
            return daysDifferenceCost;

        BigDecimal yearlyPrice = value.multiply(BigDecimal.valueOf(YearDays));

        if (duration == Monthly) {
            return yearlyPrice
                    .divide(BigDecimal.valueOf(YearMonths), 2, HALF_UP);
        }

        return yearlyPrice
                .subtract(daysDifferenceCost);
    }

    public BigDecimal getTax(LicenseDurationType duration) {
        return getTax(duration, 0);
    }

    public BigDecimal getTax(LicenseDurationType duration, long daysDifference) {

        BigDecimal taxPercent = tax.getValue()
                .divide(new BigDecimal(100), 2, HALF_UP);

        return getPrice(duration, daysDifference).multiply(taxPercent);
    }

    public BigDecimal getTotalCost(LicenseDurationType duration) {
        return getPrice(duration).add(getTax(duration));
    }

}
