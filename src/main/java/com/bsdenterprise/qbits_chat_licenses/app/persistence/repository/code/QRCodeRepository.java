package com.bsdenterprise.qbits_chat_licenses.app.persistence.repository.code;

import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.code.QRCode;
import com.bsdenterprise.qbits_chat_licenses.common.persistence.repository.SoftDeleteRepository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface QRCodeRepository extends SoftDeleteRepository<QRCode, Long> {

    default boolean exists(String code, String email) {
        return existsByCodeAndEmail(code, email);
    }

    boolean existsByCodeAndEmail(String code, String email);

    @Query("select qc from QRCode qc where qc.userId = :userId and qc.email = coalesce(:email, qc.email) ")
    Optional<QRCode> findQRCode(@Param("userId") Long userId, @Param("email") String email);

    @Query("select qc from QRCode qc where qc.userId = :userId")
    Optional<QRCode> findQRCode(@Param("userId") Long userId);

    @Query("select qc from QRCode qc where qc.code = :code")
    Optional<QRCode> findQRCode(@Param("code") String code);

    @Query("select case when count(qc) > 0 then true else false end " +
            "from QRCode qc where qc.email = :email and qc.code = :code and qc.status = 'Created'")
    boolean canVerifyCode(@Param("email") String email, @Param("code") String code);

    @Modifying(clearAutomatically = true)
    @Query("update QRCode qc set qc.status = 'Used' where qc.id = :qrCodeId")
    void markCodeAsUsed(@Param("qrCodeId") Long qrCodeId);

}
