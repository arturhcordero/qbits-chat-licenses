package com.bsdenterprise.qbits_chat_licenses.app.persistence.model.organization;

import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.organization.auth.User;
import com.bsdenterprise.qbits_chat_licenses.app.type.organization.CommunityType;
import com.bsdenterprise.qbits_chat_licenses.app.type.organization.auth.RoleType;
import com.bsdenterprise.qbits_chat_licenses.common.persistence.model.BaseEntity;

import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.payment.PaymentMethod;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.order.ProductOrder;
import com.bsdenterprise.qbits_chat_licenses.app.type.organization.OrganizationType;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import static com.bsdenterprise.qbits_chat_licenses.app.type.organization.auth.RoleType.findRoleType;

@Entity
@EqualsAndHashCode(callSuper = true)
@ToString(of = {"name", "description"})
public @Data class Organization extends BaseEntity {

    @Column(name = "name", nullable = false, length = 120)
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "rfc", length = 20)
    private String rfc;

    @OneToMany(cascade = CascadeType.ALL)
    private List<Address> addresses;

    @Column(name = "organization_type", nullable = false, length = 25)
    @Enumerated(EnumType.STRING)
    private OrganizationType organizationType;

    @Column(name = "community_type", nullable = false, length = 25)
    @Enumerated(EnumType.STRING)
    private CommunityType communityType;

    @OneToMany(mappedBy = "organization", fetch = FetchType.LAZY)
    private List<UserOrganization> userOrganizations = new ArrayList<>();

    @OneToMany(mappedBy = "organization", fetch = FetchType.LAZY)
    private List<PaymentMethod> paymentMethods;

    @OneToMany(mappedBy = "organization", fetch = FetchType.LAZY)
    private List<ProductOrder> productOrders;

    @Transient
    @JsonIgnore
    private User owner;

    public User getOwner() {

        if (owner != null) return owner;

        owner = userOrganizations.stream()
                .filter(uo -> uo.getUserOrganizationRoles().stream()
                        .anyMatch(uor -> findRoleType(uor.getRole().getId()) == RoleType.Owner))
                .map(UserOrganization::getUser)
                .findFirst().orElse(null);

        return owner;
    }

    public String getOwnerEmail() {
        return getOwner().getEmail();
    }

}
