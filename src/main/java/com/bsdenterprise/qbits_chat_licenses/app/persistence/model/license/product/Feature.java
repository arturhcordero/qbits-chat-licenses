package com.bsdenterprise.qbits_chat_licenses.app.persistence.model.license.product;

import com.bsdenterprise.qbits_chat_licenses.common.persistence.model.BaseEntity;

import javax.persistence.*;
import lombok.*;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public @Data class Feature extends BaseEntity {

    @Column(name = "code", length = 30, unique = true)
    private String code;

    @Column(name = "description", length = 100)
    private String description;

}
