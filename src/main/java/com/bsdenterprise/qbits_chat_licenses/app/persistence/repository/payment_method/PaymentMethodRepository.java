package com.bsdenterprise.qbits_chat_licenses.app.persistence.repository.payment_method;

import com.bsdenterprise.qbits_chat_licenses.common.persistence.repository.SoftDeleteRepository;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.payment.PaymentMethod;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public interface PaymentMethodRepository extends SoftDeleteRepository<PaymentMethod, Long> {

    @Query("select p from PaymentMethod p " +
                "where p.organizationId = :organizationId " +
                "and p.defaultPayment = true " +
                "and p.deleted = false")
    Optional<PaymentMethod> findDefault(@Param("organizationId") Long organizationId);

    @Query("select p from PaymentMethod p " +
            "where p.uuid = :identifier " +
            "and p.deleted = false")
    Optional<PaymentMethod> findPaymentMethod(@Param("identifier") String identifier);

    @Query("select pm from PaymentMethod pm where pm.organizationId = :organizationId and pm.deleted = false")
    List<PaymentMethod> findPaymentMethods(@Param("organizationId") Long organizationId);

}
