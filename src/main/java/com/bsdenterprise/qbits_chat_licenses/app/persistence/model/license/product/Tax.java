package com.bsdenterprise.qbits_chat_licenses.app.persistence.model.license.product;

import com.bsdenterprise.qbits_chat_licenses.common.persistence.model.BaseEntity;

import java.math.BigDecimal;

import javax.persistence.*;
import lombok.*;

@Entity
@EqualsAndHashCode(callSuper = true)
public @Data class Tax extends BaseEntity {

    @Column(name = "code", nullable = false, length = 30)
    private String code;

    @Column(name = "description", nullable = false, length = 150)
    private String description;

    @Column(name = "value", nullable = false)
    private BigDecimal value;

}
