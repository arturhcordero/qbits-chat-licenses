package com.bsdenterprise.qbits_chat_licenses.app.persistence.repository.notification;

import com.bsdenterprise.qbits_chat_licenses.common.persistence.repository.SoftDeleteRepository;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.notification.DeviceToken;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface DeviceTokenRepository extends SoftDeleteRepository<DeviceToken, Long> {

    Optional<DeviceToken> findByUserId(Long userId);

    @Modifying(clearAutomatically = true)
    @Query("update DeviceToken dt set dt.platformEndpointArn = :platformEndpointArn where dt.id = :deviceTokenId")
    void updatePlatformEndpoint(@Param("platformEndpointArn") String platformEndpointArn,
                                @Param("deviceTokenId") Long deviceTokenId);

}
