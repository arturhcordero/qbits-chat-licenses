package com.bsdenterprise.qbits_chat_licenses.app.persistence.model.organization.auth;

import com.bsdenterprise.qbits_chat_licenses.common.persistence.model.BaseEntity;

import java.util.List;

import javax.persistence.*;
import lombok.*;

@Entity
@NoArgsConstructor
@RequiredArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString(exclude = {"rolePermissions"})
public @Data class Role extends BaseEntity {

    @Column(name = "code", nullable = false, length = 50)
    private @NonNull String code;

    @Column(name = "name", nullable = false, length = 100)
    private @NonNull String name;

    @Column(name = "description", nullable = false, length = 150)
    private String description;

    @OneToMany(mappedBy = "role", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<RolePermission> rolePermissions;

}
