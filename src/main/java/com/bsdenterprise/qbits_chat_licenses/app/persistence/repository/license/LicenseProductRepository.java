package com.bsdenterprise.qbits_chat_licenses.app.persistence.repository.license;

import com.bsdenterprise.qbits_chat_licenses.common.persistence.repository.SoftDeleteRepository;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.license.product.LicenseProduct;
import com.bsdenterprise.qbits_chat_licenses.app.type.license.LicenseProductType;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LicenseProductRepository extends SoftDeleteRepository<LicenseProduct, Long> {

    @Query("select lp from LicenseProduct lp " +
            "where lp.code = coalesce(:code, lp.code) " +
            "and   lp.type = coalesce(:type, lp.type)" +
            "and   lp.deleted = false")
    List<LicenseProduct> findLicenseProducts(@Param("code") String code,
                                             @Param("type") String type);

    boolean existsByCode(String code);

}
