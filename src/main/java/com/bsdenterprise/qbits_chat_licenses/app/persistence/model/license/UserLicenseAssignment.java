package com.bsdenterprise.qbits_chat_licenses.app.persistence.model.license;

import com.bsdenterprise.qbits_chat_licenses.app.type.organization.auth.RoleType;
import com.bsdenterprise.qbits_chat_licenses.common.persistence.model.BaseEntity;

import javax.persistence.*;
import lombok.*;

@Entity
@EqualsAndHashCode(callSuper = true)
public @Data class UserLicenseAssignment extends BaseEntity {

    @Column(name = "email", nullable = false, length = 100)
    private String email;

    @Enumerated(EnumType.STRING)
    @Column(name = "role", nullable = false, length = 30)
    private RoleType role;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "license_id", referencedColumnName = "id", insertable = false, updatable = false)
    private License license;

    @Column(name = "license_id")
    private Long licenseId;

}
