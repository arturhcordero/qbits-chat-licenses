package com.bsdenterprise.qbits_chat_licenses.app.persistence.model.license;

import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.organization.Organization;
import com.bsdenterprise.qbits_chat_licenses.app.type.license.LicenseDurationType;
import com.bsdenterprise.qbits_chat_licenses.app.type.license.LicenseStatusType;
import com.bsdenterprise.qbits_chat_licenses.common.persistence.model.BaseEntity;

import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.license.product.LicenseProduct;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.organization.UserOrganization;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

import javax.persistence.*;
import lombok.*;

@Entity
@EqualsAndHashCode(callSuper = true)
public @Data class License extends BaseEntity {

    @Transient
    private Integer quantity;

    @Column(name = "expiration", length = 45)
    private LocalDateTime expiration;

    @Enumerated(EnumType.STRING)
    @Column(name = "status", length = 45)
    private LicenseStatusType status;

    @Enumerated(EnumType.STRING)
    @Column(name = "duration", nullable = false, length = 45)
    private LicenseDurationType duration;

    @ManyToOne
    @JoinColumn(name = "organization_id", referencedColumnName = "id", nullable = false)
    private Organization organization;

    @Column(name = "organization_id", nullable = false, insertable = false, updatable = false)
    private Long organizationId;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_organization_id")
    private UserOrganization userOrganization;

    @Column(name = "user_organization_id", insertable = false, updatable = false)
    private Long userOrganizationId;

    @ManyToOne
    @JoinColumn(name = "license_product_id", referencedColumnName = "id")
    private LicenseProduct licenseProduct;

    @Column(name = "license_product_id", insertable = false, updatable = false)
    private Long licenseProductId;

    public boolean isExpired() {
        return LocalDate.now().isAfter(expiration.toLocalDate());
    }

    public long expirationDaysDifference() {
        return LocalDate.now().until(expiration.toLocalDate(), ChronoUnit.DAYS);
    }

    public License() {

    }

    public License(Long quantity, License license) {
        super.setId(license.getId());
        this.quantity = quantity.intValue();
        this.expiration = license.getExpiration();
        this.status = license.getStatus();
        this.duration = license.getDuration();
        this.organization = license.getOrganization();
        this.organizationId = license.getOrganizationId();
        this.userOrganization = license.getUserOrganization();
        this.userOrganizationId = license.getUserOrganizationId();
        this.licenseProduct = license.getLicenseProduct();
        this.licenseProductId = license.getLicenseProductId();
    }

}
