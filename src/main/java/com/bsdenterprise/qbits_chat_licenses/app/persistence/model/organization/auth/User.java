package com.bsdenterprise.qbits_chat_licenses.app.persistence.model.organization.auth;

import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.notification.DeviceToken;
import com.bsdenterprise.qbits_chat_licenses.common.persistence.model.BaseEntity;

import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.organization.UserOrganization;

import com.bsdenterprise.qbits_chat_licenses.app.type.organization.auth.UserActivationType;
import com.bsdenterprise.qbits_chat_licenses.app.type.organization.user.GenderReference;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;


import static org.apache.commons.lang3.ObjectUtils.firstNonNull;
import static org.springframework.util.StringUtils.isEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.util.CollectionUtils;

import javax.persistence.*;
import lombok.*;

@Entity
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString(exclude = {"userOrganizations"})
public @Data class User extends BaseEntity {

    @Column(name = "email", nullable = false, unique = true, length = 100)
    private String email;

    @Column(name = "jid")
    private String jid;

    @Column(name = "password")
    private String password;

    @JsonIgnore
    @Transient
    private String originalPassword;

    @Column(name = "enabled", nullable = false)
    private boolean enabled;

    @Column(name = "name", length = 100)
    private String name;

    @Column(name = "last_name", length = 100)
    private String lastName;

    @Column(name = "second_last_name", length = 100)
    private String secondLastName;

    @Column(name = "birthday")
    private LocalDate birthday;

    @Enumerated(EnumType.STRING)
    @Column(name = "gender_reference", length = 20)
    private GenderReference genderReference;

    @Column(name = "nickname", length = 30)
    private String nickname;

    @Column(name = "cell_phone", length = 20)
    private String phone;

    @Column(name = "cell_phone_region", length = 5)
    private String phoneRegion;

    @Enumerated(EnumType.STRING)
    @Column(name = "activation_type", length = 20)
    private UserActivationType activationType;

    @Column(name = "confirmation_token", length = 36)
    private String confirmationToken;

    @Column(name = "forgot_password_token", length = 36)
    private String forgotPasswordToken;

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "user")
    private DeviceToken deviceToken;

    @OneToMany(mappedBy = "user")
    private List<UserOrganization> userOrganizations = new ArrayList<>();

    public User(String email, String jid, String name, String lastName, String secondLastName) {
        this.email = email;
        this.jid = jid;
        this.name = name;
        this.lastName = lastName;
        this.secondLastName = secondLastName;
    }

    public String getFullName() {

        String fullName = String.join(" ",
                firstNonNull(name, ""),
                firstNonNull(lastName, ""),
                firstNonNull(secondLastName, ""))
                .trim();

        if (isEmpty(fullName))
            return null;
        return fullName;
    }

    public String getUsername() {
        return email.replace("@", ".");
    }

    public boolean hasLicense() {

        if (CollectionUtils.isEmpty(userOrganizations))
            return false;

        return getUserOrganization().hasLicense();
    }

    public UserOrganization getUserOrganization() {
        return userOrganizations.iterator().next();
    }

}
