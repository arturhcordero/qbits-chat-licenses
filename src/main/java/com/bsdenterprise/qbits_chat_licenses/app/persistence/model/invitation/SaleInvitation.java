package com.bsdenterprise.qbits_chat_licenses.app.persistence.model.invitation;

import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.license.product.LicenseProduct;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.order.ProductOrder;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.organization.auth.User;
import com.bsdenterprise.qbits_chat_licenses.app.type.invitation.SaleInvitationStatus;
import com.bsdenterprise.qbits_chat_licenses.common.persistence.model.BaseEntity;

import javax.persistence.*;

import lombok.*;

@Entity
@EqualsAndHashCode(callSuper = true)
public @Data class SaleInvitation extends BaseEntity {

    @Column(name = "email")
    private String email;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    private User user;

    @Column(name = "user_id", insertable = false, updatable = false)
    private Long userId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "license_product_id", referencedColumnName = "id", nullable = false)
    private LicenseProduct licenseProduct;

    @Column(name = "license_product_id", insertable = false, updatable = false)
    private Long licenseProductId;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private SaleInvitationStatus status;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "product_order_id")
    private ProductOrder productOrder;

    @Column(name = "product_order_id", insertable = false, updatable = false)
    private Long productOrderId;

}
