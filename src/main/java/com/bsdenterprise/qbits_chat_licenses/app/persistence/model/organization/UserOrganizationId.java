package com.bsdenterprise.qbits_chat_licenses.app.persistence.model.organization;

/*
import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.organization.auth.User;

import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import java.io.Serializable;

import lombok.Data;

@Data
@Embeddable
@Transactional
public class UserOrganizationId implements Serializable {

    @ManyToOne(optional = false)
    @JoinColumn(name = "organizationId", updatable = false, insertable = false)
    private Organization organization;

    @Column
    private Long organizationId;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "userId", insertable = false, updatable = false)
    private User user;
    private Long userId;

}
*/