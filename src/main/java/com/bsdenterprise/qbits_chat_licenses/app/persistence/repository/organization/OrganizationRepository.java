package com.bsdenterprise.qbits_chat_licenses.app.persistence.repository.organization;

import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.organization.Organization;
import com.bsdenterprise.qbits_chat_licenses.common.persistence.repository.SoftDeleteRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrganizationRepository extends SoftDeleteRepository<Organization, Long> {



}
