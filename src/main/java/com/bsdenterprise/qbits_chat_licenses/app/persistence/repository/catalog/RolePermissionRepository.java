package com.bsdenterprise.qbits_chat_licenses.app.persistence.repository.catalog;

import org.springframework.stereotype.Repository;
import com.bsdenterprise.qbits_chat_licenses.common.persistence.repository.SoftDeleteRepository;

import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.organization.auth.RolePermission;

@Repository
public interface RolePermissionRepository extends SoftDeleteRepository<RolePermission, Long> {

}
