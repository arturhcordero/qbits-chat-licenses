package com.bsdenterprise.qbits_chat_licenses.app.persistence.model.order;

import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.license.License;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.license.UserLicenseAssignment;
import com.bsdenterprise.qbits_chat_licenses.app.type.license.LicenseDurationType;
import com.bsdenterprise.qbits_chat_licenses.common.persistence.model.BaseEntity;

import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.license.product.LicenseProduct;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;

import javax.persistence.*;
import lombok.*;

@Entity
@EqualsAndHashCode(callSuper = true)
public @Data class OrderDetail extends BaseEntity {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "product_order_id", referencedColumnName = "id", nullable = false)
    private ProductOrder productOrder;

    @Column(name = "product_order_id", nullable = false, insertable = false, updatable = false)
    private Long productOrderId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "license_product_id", referencedColumnName = "id", nullable = false)
    private LicenseProduct licenseProduct;

    @Column(name = "license_product_id", nullable = false, insertable = false, updatable = false)
    private Long licenseProductId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "license_id", referencedColumnName = "id", nullable = false)
    private License license;

    @Column(name = "license_id", insertable = false, updatable = false)
    private Long licenseId;

    @Enumerated(EnumType.STRING)
    @Column(name = "duration", nullable = false, length = 45)
    private LicenseDurationType duration;

    @Column(name = "quantity", nullable = false)
    private Integer quantity;

    @Column(name = "price", nullable = false)
    private BigDecimal price;

    @Column(name = "tax", nullable = false)
    private BigDecimal tax;

    @Column(name = "discount")
    private BigDecimal discount = BigDecimal.ZERO;

    @Column(name = "period_start_date")
    private LocalDate periodStartDate;

    @Column(name = "period_end_date")
    private LocalDate periodEndDate;

    @OneToOne(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
    @JoinColumn(name="user_license_assignment_id")
    private UserLicenseAssignment assignment;

    public BigDecimal getSubtotal() {
        return price
                .subtract(discount)
                .add(tax)
                //.multiply(BigDecimal.valueOf(this.quantity))
                .setScale(2, RoundingMode.HALF_UP);
    }

    public void setPrice(BigDecimal price) {
        this.price = BigDecimal.valueOf(quantity).multiply(price);
    }

    public void setTax(BigDecimal tax) {
        this.tax = BigDecimal.valueOf(quantity).multiply(tax);
    }

    public boolean isLicenseEstablished() {
        return license != null;
    }

}
