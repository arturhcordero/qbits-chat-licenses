package com.bsdenterprise.qbits_chat_licenses.app.persistence.model.code;

import com.bsdenterprise.qbits_chat_licenses.common.persistence.model.BaseEntity;
import com.bsdenterprise.qbits_chat_licenses.app.type.info.CodeStatus;

import javax.persistence.*;
import lombok.*;

@Entity
@EqualsAndHashCode(callSuper = true)
public @Data class VerificationCode extends BaseEntity {

    @Column(name = "email", length = 100, nullable = false)
    private String email;

    @Column(name = "code", length = 20, nullable = false, unique = true)
    private String code;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private CodeStatus status;

}
