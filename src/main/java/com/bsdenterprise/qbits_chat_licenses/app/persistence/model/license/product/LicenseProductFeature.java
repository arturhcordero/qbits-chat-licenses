package com.bsdenterprise.qbits_chat_licenses.app.persistence.model.license.product;

import com.bsdenterprise.qbits_chat_licenses.common.persistence.model.BaseEntity;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@JsonIgnoreProperties({"licenseProduct"})
public @Data class LicenseProductFeature extends BaseEntity {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "license_product_id")
    private LicenseProduct licenseProduct;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "feature_id")
    private Feature feature;



}
