package com.bsdenterprise.qbits_chat_licenses.app.persistence.model.license.product;

import com.bsdenterprise.qbits_chat_licenses.common.persistence.model.BaseEntity;

import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.license.License;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.order.OrderDetail;

import com.bsdenterprise.qbits_chat_licenses.app.type.license.LicenseProductType;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

@Entity
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor @RequiredArgsConstructor
@JsonIgnoreProperties({"licenses", "orderDetails", "productFeatures"})
public @Data class LicenseProduct extends BaseEntity implements Serializable {

    @Column(name = "code", nullable = false, length = 20)
    private @NonNull String code;

    @Column(name = "name", nullable = false, length = 60)
    private @NonNull String name;

    @Column(name = "description", length = 200)
    private @NonNull String description;

    @Column(name = "short_description", length = 50)
    private @NonNull String shortDescription;

    @Column(name = "abbreviation", length = 8)
    private @NonNull String abbreviation;

    @Column(name = "recording_url")
    private @NonNull String recordingUrl;

    @Column(name = "icon_url")
    private @NonNull String iconUrl;

    @Column(name = "color", length = 20)
    private @NonNull String color;

    @Column(name = "default_product", nullable = false)
    private @NonNull boolean defaultProduct;

    @Column(name = "hierarchy", nullable = false)
    private @NonNull Integer hierarchy;

    @Enumerated(EnumType.STRING)
    @Column(name = "type", nullable = false, length = 30)
    private LicenseProductType type = LicenseProductType.SingleLicense;

    @ManyToOne
    @JoinColumn(name = "cost_id", referencedColumnName = "id", nullable = false)
    private Cost cost;

    @Column(name = "cost_id", nullable = false, insertable = false, updatable = false)
    private Long costId;

    @OneToMany(mappedBy = "licenseProduct")
    private List<License> licenses;

    @OneToMany(mappedBy = "licenseProduct")
    private List<OrderDetail> orderDetails;

    @OneToMany(mappedBy = "licenseProduct", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<LicenseProductFeature> productFeatures = new LinkedList<>();

    public List<Feature> getFeatures() {
        return productFeatures.stream()
                .map(LicenseProductFeature::getFeature)
                .collect(Collectors.toList());
    }

    public boolean isGreatherThan(LicenseProduct licenseProduct) {
        return hierarchy.compareTo(licenseProduct.getHierarchy()) > 0;
    }

}
