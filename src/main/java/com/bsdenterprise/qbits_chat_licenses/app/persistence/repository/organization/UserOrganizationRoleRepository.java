package com.bsdenterprise.qbits_chat_licenses.app.persistence.repository.organization;

import org.springframework.stereotype.Repository;
import com.bsdenterprise.qbits_chat_licenses.common.persistence.repository.SoftDeleteRepository;

import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.organization.auth.UserOrganizationRole;

@Repository
public interface UserOrganizationRoleRepository extends SoftDeleteRepository<UserOrganizationRole, Long> {



}
