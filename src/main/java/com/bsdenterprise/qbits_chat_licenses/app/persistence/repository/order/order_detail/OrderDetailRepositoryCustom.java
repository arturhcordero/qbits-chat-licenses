package com.bsdenterprise.qbits_chat_licenses.app.persistence.repository.order.order_detail;

import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.order.OrderDetail;

public interface OrderDetailRepositoryCustom {

    OrderDetail findLastOrderDetail(Long licenseId);

}
