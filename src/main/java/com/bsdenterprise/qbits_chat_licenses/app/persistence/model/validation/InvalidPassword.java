package com.bsdenterprise.qbits_chat_licenses.app.persistence.model.validation;

import javax.persistence.*;

import com.bsdenterprise.qbits_chat_licenses.common.persistence.model.BaseEntity;
import lombok.*;

@Entity
@EqualsAndHashCode(callSuper = true)
public @Data class InvalidPassword extends BaseEntity {

    @Column(unique = true)
    private String password;

}
