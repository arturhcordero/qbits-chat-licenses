package com.bsdenterprise.qbits_chat_licenses.app.persistence.repository.order.order_detail.impl;

import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.order.OrderDetail;
import com.bsdenterprise.qbits_chat_licenses.app.persistence.repository.order.order_detail.OrderDetailRepositoryCustom;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository
@Transactional(readOnly = true)
public class OrderDetailRepositoryImpl implements OrderDetailRepositoryCustom {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public OrderDetail findLastOrderDetail(Long licenseId) {

        final String query =
                "select od from OrderDetail od " +
                    "where od.licenseId = :licenseId " +
                "order by od.id desc";

        return entityManager.createQuery(query, OrderDetail.class)
                .setParameter("licenseId", licenseId)
                .setMaxResults(1).getSingleResult();
    }

}
