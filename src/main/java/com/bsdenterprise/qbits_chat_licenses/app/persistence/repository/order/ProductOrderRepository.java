package com.bsdenterprise.qbits_chat_licenses.app.persistence.repository.order;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.bsdenterprise.qbits_chat_licenses.common.persistence.repository.SoftDeleteRepository;

import com.bsdenterprise.qbits_chat_licenses.app.persistence.model.order.ProductOrder;

import java.util.List;

@Repository
public interface ProductOrderRepository extends SoftDeleteRepository<ProductOrder, Long> {

    @Query("select po from ProductOrder po " +
            "where po.organizationId = :organizationId " +
            "and po.deleted = false")
    List<ProductOrder> findOrganizationOrders(@Param("organizationId") Long organizationId);


}
