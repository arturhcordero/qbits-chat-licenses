package com.bsdenterprise.qbits_chat_licenses.app.type.license;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

@Getter
@AllArgsConstructor
public enum LicenseActionFlow {

    LicenseAssignment                   (1L, "License assignment"),
    LicenseAssignmentNonExistentUser    (2L, "License assignment for existent user"),
    LicenseUnassignment                 (3L, "License unassignment"),
    LicenseRenovation                   (4L, "License renovation");

    private Long identifier;
    private String description;

    public static LicenseActionFlow findActionFlow(Long identifier) {
        return Arrays.stream(values())
                .filter(type -> type.getIdentifier().equals(identifier))
                .findFirst().orElse(null);
    }

}
