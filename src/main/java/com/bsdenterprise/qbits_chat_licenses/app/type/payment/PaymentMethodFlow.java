package com.bsdenterprise.qbits_chat_licenses.app.type.payment;

import java.util.Arrays;
import lombok.*;

@Getter
@AllArgsConstructor
public enum PaymentMethodFlow {

    CreatingOrganizationOrder   (1L, "Creating organization"),
    CreatingPaymentMethod       (2L, "Creating a simple payment method");

    private Long identifier;
    private String description;

    public static PaymentMethodFlow findPaymentMethodFlow(Long identifier) {
        return Arrays.stream(values())
                .filter(type -> type.getIdentifier().equals(identifier))
                .findFirst().orElse(null);
    }

}
