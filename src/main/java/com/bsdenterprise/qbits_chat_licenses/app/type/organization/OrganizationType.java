package com.bsdenterprise.qbits_chat_licenses.app.type.organization;

import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

@Getter
@AllArgsConstructor
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum OrganizationType {

    Physical  (1L, "Persona física", true),
    Moral     (2L, "Persona moral", true),
    NoData    (3L, "Sin datos fiscales", false);

    private Long identifier;
    private String description;
    private boolean visible;

    public static OrganizationType findType(Long id) {
        return Arrays.stream(values())
                .filter(type -> type.getIdentifier().equals(id))
                .findFirst().orElse(null);
    }

}
