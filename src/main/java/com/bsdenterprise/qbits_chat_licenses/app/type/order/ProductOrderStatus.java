package com.bsdenterprise.qbits_chat_licenses.app.type.order;

import lombok.*;

@Getter
@AllArgsConstructor
public enum ProductOrderStatus {

    Created         ("Order created"),
    Ordered         ("Order in status ordered"),
    Processed       ("Order processed"),
    Trying          ("Trying to pay order"),
    Paid            ("The order was paid successfully"),
    AlreadyPaid     ("The order is already paid"),
    PaymentDeclined ("Order with payment declined");

    private String description;

}
