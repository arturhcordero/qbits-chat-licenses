package com.bsdenterprise.qbits_chat_licenses.app.type.organization;

import java.util.Arrays;
import lombok.*;

@Getter
@AllArgsConstructor
public enum UserOrganizationStatusType {

    Inactive    (1L, "Usuario inactivo"),
    Active      (2L, "Usuario activo");

    private Long identifier;
    private String description;

    public static UserOrganizationStatusType findType(Long id) {
        return Arrays.stream(values())
                .filter(type -> type.getIdentifier().equals(id))
                .findFirst().orElse(null);
    }

}

