package com.bsdenterprise.qbits_chat_licenses.app.type.organization.user;

import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

@Getter
@AllArgsConstructor
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum GenderReference {

    Male        (1L, "Masculino"),
    Female      (2L, "Femenino"),
    ToDefine    (3L, "Otro");

    private Long identifier;
    private String description;

    public static GenderReference findType(Long id) {
        return Arrays.stream(values())
                .filter(type -> type.getIdentifier().equals(id))
                .findFirst().orElse(null);
    }

}
