package com.bsdenterprise.qbits_chat_licenses.app.type.invitation;

import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

@Getter
@AllArgsConstructor
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum SaleInvitationStatus {

    Sent        (1L, "Invitación enviada", "fas fa-send text-primary"),
    Forwarded   (2L, "Invitación reenviada", "fas fa-send text-primary"),
    Open        (3L, "Invitación abierta", "fas fa-envelope-open text-primary"),
    Cancelled   (4L, "Invitación cancelada", "fas fa-ban text-danger"),
    Used        (5L, "Compra realizada", "fas fa-shopping-cart text-dark");

    private Long identifier;
    private String description;
    private String icon;

    public String getName(){
        return name();
    }

    public static SaleInvitationStatus findStatus(Long id) {
        return Arrays.stream(values())
                .filter(type -> type.getIdentifier().equals(id))
                .findFirst().orElse(null);
    }

}
