package com.bsdenterprise.qbits_chat_licenses.app.type.organization;

import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

@Getter
@AllArgsConstructor
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum CommunityType {

    Closed          (1, "Comunidad cerrada"),
    Open            (2, "Comunidad abierta"),
    PartiallyClosed (3, "Comunidad parcialmente cerrada");

    private Integer identifier;
    private String description;

    public static CommunityType findType(Integer identifier) {
        if (identifier == null) return null;
        return Arrays.stream(values())
                .filter(type -> type.getIdentifier().equals(identifier))
                .findFirst().orElse(null);
    }

}
