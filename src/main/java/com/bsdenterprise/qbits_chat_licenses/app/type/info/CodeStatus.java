package com.bsdenterprise.qbits_chat_licenses.app.type.info;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

@Getter
@AllArgsConstructor
public enum CodeStatus {

    Created    (1, "Código creado"),
    Used       (2, "Código utilizado");

    private Integer identifier;
    private String description;

    public static CodeStatus findType(Integer id) {
        return Arrays.stream(values())
                .filter(type -> type.getIdentifier().equals(id))
                .findFirst().orElse(null);
    }

}
