package com.bsdenterprise.qbits_chat_licenses.app.type.organization.auth;

import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

@Getter
@AllArgsConstructor
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum RoleType {

    Owner       (1L, "OWNER", "Owner", "Propietario"),
    Admin       (2L, "ADMIN", "Admin", "Administrador"),
    User        (3L, "USER", "User", "Usuario ordinario"),
    Salesman    (4L, "SALESMAN", "Salesman", "Vendedor"),
    Validator   (5L, "VALIDATOR", "Validator",  "Validador de la tienda"),
    Maintainer  (5L, "MAINTAINER", "Maintainer",  "Mantenedor");

    private Long identifier;
    private String code;
    private String name;
    private String description;

    public static RoleType findRoleType(Long type) {
        return Arrays.stream(values())
                .filter(roleType -> roleType.getIdentifier().equals(type))
                .findFirst().orElse(null);
    }

    public static RoleType findRoleType(String name) {
        return Arrays.stream(values())
                .filter(roleType -> roleType.name().equals(name))
                .findFirst().orElse(null);
    }

}
