package com.bsdenterprise.qbits_chat_licenses.app.type.info;

import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

@Getter
@AllArgsConstructor
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum CodeType {

    UserVerification    (1, "Verificación de usuario"),
    QRToken             (2, "Generación de QR");

    private Integer identifier;
    private String description;

    public static CodeType findType(Integer id) {
        return Arrays.stream(values())
                .filter(type -> type.getIdentifier().equals(id))
                .findFirst().orElse(null);
    }

}
