package com.bsdenterprise.qbits_chat_licenses.app.type.license;

import java.util.Arrays;
import lombok.*;

@Getter
@AllArgsConstructor
public enum LicenseProductType {

    SingleLicense   (1L, "Single license"),
    LicensePacket   (2L, "License packet");

    private Long identifier;
    private String description;

    public static LicenseProductType findType(Long id) {
        return Arrays.stream(values())
                .filter(type -> type.getIdentifier().equals(id))
                .findFirst().orElse(null);
    }

}
