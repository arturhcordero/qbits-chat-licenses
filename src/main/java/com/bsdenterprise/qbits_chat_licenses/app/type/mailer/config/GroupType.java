package com.bsdenterprise.qbits_chat_licenses.app.type.mailer.config;

import java.util.Arrays;
import lombok.*;

@Getter
@AllArgsConstructor
public enum GroupType {

    QBITS_CHAT(1),
    QBITS_CHAT_CONTRACTS(2);

    private Integer type;

    public static GroupType findGroupType(Integer type) {
        return Arrays.stream(values())
                .filter(platform -> platform.getType().equals(type))
                .findFirst().orElse(null);
    }

}
