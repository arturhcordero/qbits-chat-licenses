package com.bsdenterprise.qbits_chat_licenses.app.type.order;

import java.util.*;
import lombok.*;

@Getter
@AllArgsConstructor
public enum ProsaCodeType {

    SUCCESS("00"),
    NOT_SUCCESSFUL("XX");

    private String identifier;

    public static Optional<ProsaCodeType> findType(String id) {
        return Arrays.stream(values())
                .filter(type -> type.getIdentifier().equals(id))
                .findFirst();
    }

}
