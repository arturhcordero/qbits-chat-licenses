package com.bsdenterprise.qbits_chat_licenses.app.type.payment;

import lombok.*;

import java.util.Arrays;

@Getter
@AllArgsConstructor
public enum PaymentMethodBrand {

    Visa        (1L, "VISA", "payment-visa"),
    Mastercard  (2L, "MASTERCARD", "payment-mastercard");

    private Long identifier;
    private String name;
    private String icon;

    public static PaymentMethodBrand findPaymentMethodBrand(String name) {
        return Arrays.stream(values())
                .filter(type -> type.getName().equals(name))
                .findFirst().orElse(null);
    }

}
