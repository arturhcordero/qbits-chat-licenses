package com.bsdenterprise.qbits_chat_licenses.app.type.license;

import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

@Getter
@AllArgsConstructor
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum LicenseStatusType {

    Inactive    (1L, "Licencia inactiva"),
    Active      (2L, "Licencia activa");

    private Long identifier;
    private String description;

    public static LicenseStatusType findType(Long id) {
        return Arrays.stream(values())
                .filter(type -> type.getIdentifier().equals(id))
                .findFirst().orElse(null);
    }

}
