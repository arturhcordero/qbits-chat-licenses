package com.bsdenterprise.qbits_chat_licenses.app.type.notification;

import java.util.*;
import lombok.*;

@Getter
@AllArgsConstructor
public enum DeviceType {

    Android(1),
    iOS(2);

    private Integer type;

    public static Optional<DeviceType> findDeviceType(Integer type) {
        return Arrays.stream(values())
                .filter(deviceType -> deviceType.getType().equals(type))
                .findFirst();
    }

}
