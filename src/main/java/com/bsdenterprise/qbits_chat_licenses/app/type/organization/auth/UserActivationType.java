package com.bsdenterprise.qbits_chat_licenses.app.type.organization.auth;

import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

@Getter
@AllArgsConstructor
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum UserActivationType {

    Automatic  (1L, "Activación automática"),
    Default    (2L, "Activación por solicitud");

    private Long identifier;
    private String description;

    public static UserActivationType findType(Long identifier) {
        return Arrays.stream(values())
                .filter(type -> type.getIdentifier().equals(identifier))
                .findFirst().orElse(null);
    }

}
