package com.bsdenterprise.qbits_chat_licenses.app.type.notification;

import java.util.*;
import lombok.*;

@Getter
@AllArgsConstructor
public enum PlatformType {

    GCM(1),
    APNS(2),
    APNS_VOIP(3),
    APNS_SANDBOX(4),
    APNS_VOIP_SANDBOX(5);

    private Integer type;

    public static Optional<PlatformType> findPlatformType(Integer type) {
        return Arrays.stream(values())
                .filter(platform -> platform.getType().equals(type))
                .findFirst();
    }

}
