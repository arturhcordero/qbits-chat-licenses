package com.bsdenterprise.qbits_chat_licenses.app.type.license;

import static java.time.temporal.ChronoUnit.*;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.*;

@Getter
@AllArgsConstructor
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum LicenseDurationType {

    Monthly (1L, "Mensual", MONTHS),
    Yearly  (2L, "Anual", YEARS);

    private Long identifier;
    private String description;
    private ChronoUnit unit;

    public static final Integer YearMonths = 12;
    public static final Integer YearDays = 365;

    public static LicenseDurationType findType(Long id) {
        return Arrays.stream(values())
                .filter(type -> type.getIdentifier().equals(id))
                .findFirst().orElse(null);
    }

}
