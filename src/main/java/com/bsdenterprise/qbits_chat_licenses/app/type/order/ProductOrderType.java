package com.bsdenterprise.qbits_chat_licenses.app.type.order;

import java.util.Arrays;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.google.common.collect.ImmutableMap;
import lombok.*;

@Getter
@AllArgsConstructor
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum ProductOrderType {

    CreateOrganization(1L,
            new ImmutableMap.Builder<String, String>()
                    .put("singular", "Compra de licencia")
                    .put("plural", "Compra de licencia")
                    .build()
    ),
    AddingLicenses(2L,
            new ImmutableMap.Builder<String, String>()
                    .put("singular", "Compra de licencia adicional")
                    .put("plural", "Compra de licencias adicionales")
                    .build()
    ),
    UpdateLicense(3L,
            new ImmutableMap.Builder<String, String>()
                    .put("singular", "Actualización de licencia")
                    .put("plural", "Actualización de licencia")
                    .build()
    ),
    LicenseRenovation(4L,
            new ImmutableMap.Builder<String, String>()
                    .put("singular", "Renovación de licencia")
                    .put("plural", "Renovación de licencias")
                    .build()
    );

    private Long identifier;
    private Map<String, String> description;

    public static ProductOrderType findType(Long id) {
        return Arrays.stream(values())
                .filter(type -> type.getIdentifier().equals(id))
                .findFirst().orElse(null);
    }

    public String getName() {
        return name();
    }

}
