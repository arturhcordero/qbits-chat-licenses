package com.bsdenterprise.qbits_chat_licenses.common.config.swagger;

import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.oauth2.resource.ResourceServerProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;

import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.swagger.web.SecurityConfiguration;
import springfox.documentation.swagger.web.SecurityConfigurationBuilder;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.service.*;
import springfox.documentation.builders.*;

import lombok.Data;

import java.util.*;

import static org.springframework.http.MediaType.*;
import static org.springframework.web.bind.annotation.RequestMethod.*;

@Data
@Component
@EnableSwagger2
@ConfigurationProperties(prefix = "app.swagger")
public class SwaggerConfig {

    private String name;
    private String version;
    private String description;

    private String maintainerName;
    private String maintainerEmail;
    private String maintainerUrl;

    @Setter(onMethod = @__(@Autowired))
    private ResourceServerProperties resourceServerProperties;

    @Bean
    public Docket qbitsLicensesDocket() {
        return docket("Q-Bits Chat Licenses", "/.*");
    }

    private Docket docket(String title, String path) {

        List<ResponseMessage> globalResponses = globalResponses();

        return new Docket(DocumentationType.SWAGGER_2)
                .groupName(title)

                .select()
                .apis(RequestHandlerSelectors.basePackage(
                        "com.bsdenterprise.qbits_chat_licenses"
                ))
                .paths(PathSelectors.regex(path))
                .build()

                .apiInfo(info())
                .consumes(new HashSet<>(Arrays.asList(
                        APPLICATION_JSON_VALUE
                ))).produces(new HashSet<>(Arrays.asList(
                        APPLICATION_JSON_VALUE
                )))
                .useDefaultResponseMessages(false)
                .globalResponseMessage(GET, globalResponses)
                .globalResponseMessage(POST, globalResponses)
                .globalResponseMessage(PUT, globalResponses)
                .globalResponseMessage(PATCH, globalResponses)
                .globalResponseMessage(DELETE, globalResponses)

                //.globalOperationParameters(dockParameters())
                .securityContexts(Collections.singletonList(securityContext()))
                .securitySchemes(Collections.singletonList(securityScheme()));
    }

    private List<ResponseMessage> globalResponses() {

        return Arrays.asList(
                new ResponseMessageBuilder()
                        .code(200)
                        .message("OK")
                        .build(),
                new ResponseMessageBuilder()
                        .code(400)
                        .message("Bad Request")
                        .build(),
                new ResponseMessageBuilder()
                        .code(500)
                        .message("Internal Error")
                        .build());
    }

    private ApiInfo info() {

        return new ApiInfoBuilder()
                .title(name)
                .description(description)
                .version(version)
                .contact(new Contact(
                        maintainerName,
                        maintainerUrl,
                        maintainerEmail
                )).build();
    }

    @Bean
    public SecurityConfiguration security() {
        return SecurityConfigurationBuilder.builder().build();
    }

    private List<Parameter> dockParameters() {

        List<Parameter> parameters = new ArrayList<>();

        Parameter authorizationParameter = new ParameterBuilder()
                .name(HttpHeaders.AUTHORIZATION)
                .description("Authentication header")
                .modelRef(new ModelRef("string"))
                .parameterType("header")
                .required(false)
                .build();

        parameters.add(authorizationParameter);

        return parameters;
    }

    private SecurityScheme securityScheme() {

        String tokenInfoUri = resourceServerProperties.getTokenInfoUri()
                .replace("check_token", "token");

        GrantType grantType = new ResourceOwnerPasswordCredentialsGrant(tokenInfoUri);

        return new OAuthBuilder().name("spring_oauth")
                .grantTypes(Collections.singletonList(grantType))
                .scopes(Arrays.asList(scopes()))
                .build();
    }

    private SecurityContext securityContext() {
        return SecurityContext.builder().securityReferences(defaultAuth()).forPaths(PathSelectors.ant("/**"))
                .build();
    }

    private List<SecurityReference> defaultAuth() {
        return Collections.singletonList(new SecurityReference("spring_oauth", scopes()));
    }

    private AuthorizationScope[] scopes() {
        return new AuthorizationScope[]{
                new AuthorizationScope("read", "for read operations"),
                new AuthorizationScope("write", "for write operations"),
                new AuthorizationScope("foo", "Access foo API") };
    }

}
