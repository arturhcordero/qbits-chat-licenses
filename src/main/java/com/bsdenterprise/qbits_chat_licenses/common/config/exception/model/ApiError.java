package com.bsdenterprise.qbits_chat_licenses.common.config.exception.model;


import com.bsdenterprise.qbits_chat_licenses.common.config.message.MessageCode;
import lombok.Data;
import org.springframework.http.HttpStatus;
import org.springframework.util.StringUtils;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.Calendar;

@Data
public class ApiError {

    private long timestamp;
    private int status;
    private String error;
    private Object message;
    private Integer messageCode;
    private String path;

    private ApiError() {
        this.timestamp = Instant.now().toEpochMilli();
    }

    public ApiError(MessageCode messageCode, Throwable ex, HttpStatus status, WebRequest request) {
        this();
        this.status = status.value();
        this.error  = status.getReasonPhrase();
        this.message = ex.getMessage();
        this.messageCode = messageCode.getCode();
        path  = ((ServletWebRequest) request).getRequest().getServletPath();
    }

}

