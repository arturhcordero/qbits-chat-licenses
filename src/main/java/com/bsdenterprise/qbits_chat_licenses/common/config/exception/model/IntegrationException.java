package com.bsdenterprise.qbits_chat_licenses.common.config.exception.model;

import lombok.Getter;

public class IntegrationException extends Exception {

    @Getter
    private String path;

    public IntegrationException(String message) {
        super(message);
    }

    public IntegrationException(String path, String message) {
        super(message);
        this.path = path;
    }

}
