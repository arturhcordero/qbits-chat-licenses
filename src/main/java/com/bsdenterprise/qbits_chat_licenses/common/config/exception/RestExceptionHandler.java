package com.bsdenterprise.qbits_chat_licenses.common.config.exception;

import com.bsdenterprise.qbits_chat_licenses.common.config.exception.model.ApiError;
import com.bsdenterprise.qbits_chat_licenses.common.config.exception.model.*;

import com.bsdenterprise.qbits_chat_licenses.common.config.message.MessageCode;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.core.Ordered;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.*;

import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.context.request.WebRequest;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import lombok.RequiredArgsConstructor;

import java.sql.SQLException;

import static com.bsdenterprise.qbits_chat_licenses.common.config.message.MessageCode.UNEXPECTED_ERROR;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
@Slf4j
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    private final ObjectMapper objectMapper;

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
            MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {

        // todo: create a response entity for method argument invalid exceptions
        ex.printStackTrace();

        return super.handleMethodArgumentNotValid(ex, headers, status, request);
    }

    @ExceptionHandler(RestClientException.class)
    protected ResponseEntity handleRestClientException(RestClientException ex, WebRequest request) {
        ex.printStackTrace();
        HttpHeaders headers = new HttpHeaders();
        return handleRestClientException(ex, headers, request);
    }

    @ExceptionHandler(ValidationException.class)
    protected ResponseEntity handleValidationException(ValidationException ex, WebRequest request) {
        ex.printStackTrace();
        HttpHeaders headers = new HttpHeaders();
        return handleValidationException(ex, headers, request);
    }

    @ExceptionHandler(Throwable.class)
    protected ResponseEntity handleThrowable(Throwable ex, WebRequest request) {
        ex.printStackTrace();
        HttpHeaders headers = new HttpHeaders();
        return handleUnexpectedException(ex, headers, HttpStatus.INTERNAL_SERVER_ERROR, request);
    }

    @ExceptionHandler(DataIntegrityViolationException.class)
    protected ResponseEntity handleDataIntegrityViolationException(DataIntegrityViolationException ex, WebRequest request) {
        ex.printStackTrace();
        HttpHeaders headers = new HttpHeaders();
        SQLException sqlException = ((ConstraintViolationException) ex.getCause()).getSQLException();
        return handleUnexpectedException(sqlException, headers, HttpStatus.INTERNAL_SERVER_ERROR, request);
    }

    private ResponseEntity handleUnexpectedException(Throwable ex, HttpHeaders headers, HttpStatus status, WebRequest request) {

        ApiError apiError = new ApiError(UNEXPECTED_ERROR, ex, status, request);

        return new ResponseEntity<Object>(apiError, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    private ResponseEntity handleRestClientException(RestClientException ex, HttpHeaders headers, WebRequest request) {

        if (ex instanceof HttpStatusCodeException) {

            HttpStatusCodeException httpClientError = ((HttpStatusCodeException) ex);
            String body = httpClientError.getResponseBodyAsString();

            httpClientError.printStackTrace();

            try {

                log.error(body);

                ApiError apiError = objectMapper.readValue(body, ApiError.class);
                HttpStatus status = HttpStatus.valueOf(apiError.getStatus());

                return handleExceptionInternal(ex, apiError, headers, status, request);

            } catch (Exception e) {

                HttpStatus status = httpClientError.getStatusCode();
                ApiError apiError = new ApiError(MessageCode.USER_ALREADY_EXISTS, ex, status, request);

                return handleExceptionInternal(ex, apiError, headers, status, request);
            }

        } else {

            ex.printStackTrace();

            HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;

            ApiError apiError = new ApiError(MessageCode.INTEGRATION_ERROR, ex, status, request);

            return handleExceptionInternal(ex, apiError, headers, status, request);

        }
    }

    private ResponseEntity handleValidationException(ValidationException ex, HttpHeaders headers, WebRequest request) {

        if (log.isErrorEnabled())
            log.error(ex.getMessage(), ex);

        HttpStatus status = HttpStatus.UNPROCESSABLE_ENTITY;

        ApiError apiError = new ApiError(ex.getMessageCode(), ex, status, request);

        return handleExceptionInternal(ex, apiError, headers, status, request);
    }

}

