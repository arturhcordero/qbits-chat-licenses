package com.bsdenterprise.qbits_chat_licenses.common.config.message;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

@Component
public class MessageProperties {

    private static MessageSource messageSource;

    @Autowired
    public void setMessageSource(@Qualifier("messageSource") MessageSource ms) {
        messageSource = ms;
    }

    public static String getMessage(String code) {
        return messageSource.getMessage(code, null, LocaleContextHolder.getLocale());
    }

}
