package com.bsdenterprise.qbits_chat_licenses.common.config.exception.model;

import com.bsdenterprise.qbits_chat_licenses.common.config.message.MessageCode;
import com.bsdenterprise.qbits_chat_licenses.common.config.message.MessageProperties;
import lombok.*;

import static com.bsdenterprise.qbits_chat_licenses.common.config.message.MessageProperties.getMessage;

@Data
@EqualsAndHashCode(callSuper = true)
public class ValidationException extends Exception {

    private MessageCode messageCode;

    public ValidationException(String message, Object... args) {
        super(String.format(message, args));
    }

    public ValidationException(MessageCode messageCode, Object... args) {
        super(String.format(MessageProperties.getMessage(messageCode.getKey()), args));
        this.messageCode = messageCode;
    }

}

