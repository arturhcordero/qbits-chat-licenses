package com.bsdenterprise.qbits_chat_licenses.common.config.message;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter @AllArgsConstructor
public enum MessageCode {

    UNEXPECTED_ERROR("unexpected.error", 820),
    USER_ALREADY_EXISTS("user.create.existent-user", 1500),
    USER_INVALID_EMAIL("user.create.existent-user", 1500),
    PAYMENT_UNSUCCESSFUL("user.create.existent-user", 1500),
    USER_INVALID_BIRTHDAY("user.create.invalid.birthday", 1500),
    USER_ACCESS_DENIED("user.access.denied", 1400),
    INTEGRATION_ERROR("integration.error", 1501);

    private String key;
    private Integer code;

}
