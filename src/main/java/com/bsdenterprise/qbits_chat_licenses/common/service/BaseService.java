package com.bsdenterprise.qbits_chat_licenses.common.service;

import com.bsdenterprise.qbits_chat_licenses.common.persistence.model.BaseEntity;
import com.bsdenterprise.qbits_chat_licenses.common.persistence.repository.SoftDeleteRepository;
import com.bsdenterprise.qbits_chat_licenses.common.util.MapperUtil;
import com.bsdenterprise.qbits_chat_licenses.common.util.ObjectUtil;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import java.lang.reflect.ParameterizedType;

import static com.bsdenterprise.qbits_chat_licenses.app.util.BeanUtil.getBean;
import static com.google.common.base.Preconditions.checkArgument;
import static java.util.Objects.requireNonNull;

import java.util.*;

import lombok.Setter;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.util.StringUtils;

public abstract class BaseService<R extends SoftDeleteRepository, E extends BaseEntity> {

    @Getter
    @PersistenceContext
    protected EntityManager entityManager;

    @Setter(onMethod = @__({@Autowired}))
    protected R repository;

    @Setter(onMethod = @__({@Autowired, @Qualifier("commonMapper")}))
    protected MapperUtil mapperUtil;

    @Setter(onMethod=@__({@Autowired}))
    protected ObjectUtil objectUtil;

    private Class<E> clazz;

    private final String entityNotFoundByIdTemplate = "%s with id: %d doest not exists";

    @SuppressWarnings("unchecked")
    public Class<E> clazz() {

        if (this.clazz == null) {

            Class<?> actualClass = this.getClass();

            ParameterizedType pt = (ParameterizedType) actualClass.getGenericSuperclass();

            this.clazz = (Class<E>) pt.getActualTypeArguments()[1];
        }

        return this.clazz;
    }

    @SuppressWarnings("unchecked")
    protected boolean existsById(Long id) {
        return repository.existsById(id);
    }

    @SuppressWarnings("unchecked")
    protected <T> T findInactiveById(Long id, Class<T> clazz) throws Exception {

        checkArgument(id != 0, "The given id must not be null!");

        Optional<E> optionalEntity = repository.findInactiveById(id);

        optionalEntity.orElseThrow(() -> new EntityNotFoundException("Entity with id: " + id + "does not exists"));

        E entity = optionalEntity.get();

        return mapperUtil().map(entity, clazz);
    }

    @SuppressWarnings({"unchecked", "Duplicates"})
    protected <T> T findById(Long id, Class<T> clazz) throws EntityNotFoundException {

        checkArgument(id != 0, "The given id must not be null!");

        Optional<E> optionalEntity = repository.findById(id);

        optionalEntity.orElseThrow(() ->
                new EntityNotFoundException(String.format(entityNotFoundByIdTemplate, clazz().getSimpleName(), id
                )));

        E entity = optionalEntity.get();

        return mapperUtil().map(entity, clazz);
    }

    @SuppressWarnings({"unchecked", "Duplicates"})
    protected <T> E findById(Long id) throws EntityNotFoundException {

        checkArgument(id != 0, "The given id must not be null!");

        Optional<E> optionalEntity = repository.findById(id);

        optionalEntity.orElseThrow(() ->
                new EntityNotFoundException(String.format(entityNotFoundByIdTemplate, clazz().getSimpleName(), id
                )));

        return optionalEntity.get();
    }

    @SuppressWarnings("unchecked")
    protected <T> List<T> findAllInactive(Class<T> clazz) {
        return mapperUtil().map(repository.findAllInactive(), clazz);
    }

    @SuppressWarnings("unchecked")
    protected <T> List<T> findAll(Class<T> clazz) {
        return mapperUtil().map(repository.findAll(), clazz);
    }

    @SuppressWarnings("unchecked")
    protected <E> List<E> findAll() {
        return repository.findAll();
    }

    protected <T, K> K save(T input, Class<K> clazz) throws Exception {

        requireNonNull(input);

        E entity = mapperUtil.map(input, clazz());

        return mapperUtil.map(repository.save(entity), clazz);
    }

    @SuppressWarnings("unchecked")
    protected <T> E save(T input) throws Exception {

        requireNonNull(input);

        if (input.getClass().getSimpleName()
                .equalsIgnoreCase(clazz().getName()))
            return (E) repository.save(input);

        E entity = mapperUtil.map(input, clazz());

        return mapperUtil.map(repository.save(entity), clazz());
    }

    @SuppressWarnings("unchecked")
    protected <T, K> T update(Long id, K updateInput, Class<T> clazz, boolean partialUpdate) throws Exception {

        checkArgument(id != 0);
        requireNonNull(updateInput);

        Optional<E> optionalEntity = repository.findById(id);

        optionalEntity.orElseThrow(() -> new EntityNotFoundException("Entity with id: " + id + "does not exists"));

        E entity = optionalEntity.get();

        if (partialUpdate) {
            objectUtil.merge(updateInput, entity);
        } else {
            mapperUtil.map(updateInput, entity);
        }

        repository.saveAndFlush(entity);

        return mapperUtil.map(entity, clazz);
    }

    @SuppressWarnings({"unchecked", "Duplicates"})
    protected <T> T delete(Long id, Class<T> clazz, boolean hardDelete) throws Exception {

        checkArgument(id != 0, "The given id must not be null!");

        Optional<E> optionalEntity = repository.findById(id);

        optionalEntity.orElseThrow(() -> new EntityNotFoundException("Entity with id: " + id + "does not exists"));

        E entity = optionalEntity.get();

        if (hardDelete) {
            repository.delete(entity);
        } else {
            repository.softDelete(entity);
        }

        return mapperUtil.map(entity, clazz);
    }

    @SuppressWarnings({"unchecked", "Duplicates"})
    protected E delete(Long id, boolean hardDelete) throws Exception {

        checkArgument(id != 0, "The given id must not be null!");

        Optional<E> optionalEntity = repository.findById(id);

        optionalEntity.orElseThrow(() -> new EntityNotFoundException("Entity with id: " + id + "does not exists"));

        E entity = optionalEntity.get();

        if (hardDelete) {
            repository.delete(entity);
        } else {
            repository.softDelete(entity);
        }

        return entity;
    }

    protected void deleteAll() throws Exception {
        repository.deleteAll();
    }

    public MapperUtil mapperUtil() {
        return mapperUtil;
    }

    public MapperUtil mapperUtil(String mapperName) {

        if (StringUtils.isEmpty(mapperName))
            return mapperUtil;

        try {
            return getBean(mapperName, MapperUtil.class);
        } catch (Exception ex) {
            return mapperUtil;
        }
    }

    public <T> T getReference(Class<T> clazz, Object primaryKey) {
        return entityManager.getReference(clazz, primaryKey);
    }

}