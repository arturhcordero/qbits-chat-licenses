package com.bsdenterprise.qbits_chat_licenses.common.util;

import java.time.format.DateTimeFormatter;
import java.util.regex.Pattern;

public class Constants {

    // Regex
    public static final Pattern PAYMENT_METHOD_PATTERN = Pattern.compile("^((4\\d{3})|(5[1-5]\\d{2})|(6011))-?\\d{4}-?\\d{4}-?\\d{4}|3[4,7]\\d{13}$");

    public static final String DEFAULT_PIN_PAYMENT_METHOD = "1234";
    public static final Integer DEFAULT_AGREEMENT_LENGTH = 19;
    public static final String UNDEFINED = "Undefined";

    public static final DateTimeFormatter DEFAULT_DATE_FORMATTER = DateTimeFormatter.ofPattern("dd/MM/yyyy");
    public static final DateTimeFormatter BIRTHDAY_FORMATTER = DEFAULT_DATE_FORMATTER;
    public static final DateTimeFormatter PAYMENT_METHOD_YEAR_FORMATTER = DateTimeFormatter.ofPattern("yy");

    public static final int EMAIL_VERIFICATION_CODE_LENGTH = 6;
    public static final int QR_CODE_LENGTH = 12;

    public static final String SUCESSFUL_PAYMENT_CODE = "00";
    public static final String SNS_MESSAGE_STRUCTURE_JSON = "json";

}
