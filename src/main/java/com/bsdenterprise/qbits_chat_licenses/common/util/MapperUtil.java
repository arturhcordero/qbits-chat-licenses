package com.bsdenterprise.qbits_chat_licenses.common.util;

import org.modelmapper.ModelMapper;

import lombok.*;
import java.util.*;

public class MapperUtil {

    @Getter @Setter
    private ModelMapper modelMapper;

    public <T, K> List<T> map(Iterable<K> rawData, Class<T> convertToClass) {
        List<T> convertedData = new ArrayList<>();

        if (rawData == null)
            return convertedData;

        for (K raw : rawData) {
            T convertedElement = modelMapper.map(raw, convertToClass);
            convertedData.add(convertedElement);
        }
        return convertedData;
    }

    public <T, K> T map(K rawData, Class<T> convertToClass) {
        if (rawData == null)
            return null;

        return modelMapper.map(rawData, convertToClass);
    }

    public <T, K> void map(K source, T destination) {
        modelMapper.map(source, destination);
    }

}
